#include <iostream>
#include <time.h>
using namespace std;

#define TURN_MAX 5			//ターン数
#define INITIAL_CHIP 300	//初期手持ち金額

struct Hand
{
	//役
	int role;
	//その時の一番でかい数字
	int number;
};

//カード分配
void Distribute(Hand* hHand, Hand* nHand, bool* cTime);
//情報開示
void ShowInfo(int turn, int pGold, int nGold);
//手札の役開示
void ShowRole(Hand hInfo);
//手札の中の最強の数字開示
void ShowNum(Hand hInfo);
//賭ける
void Bet(int* hChip, int* hBet, int* nChip, int* nBet);
//結果（表示も兼ねている）
void Judge(Hand* hHand, Hand* nHand, int* hChip, int* nChip, int* allHBet, int* allNBet);

//役一覧
enum
{
	HIGH_CARD,				//ノーペア
	A_PEIR,					//ワンペア
	TWO_PEIR,				//ツーペア
	THREE_CARD,				//スリーカード
	STRAIGHT,				//ストレート
	FLUSH,					//フラッシュ
	FULLHOUSE,				//フルハウス
	FOUR_CARD,				//フォーカード
	STRAIGHT_FLASH,			//ストレートフラッシュ
	ROYAL_STRAIGHT_FLASH	//ロイヤルストレートフラッシュ
};

int main(void)
{
	int HumanChip = INITIAL_CHIP;	//ユーザーの所持金（チップ）
	int NPCChip = INITIAL_CHIP;		//　NPC　の所持金（チップ）
	int HumanBet = 0;				//ユーザーがベットする金額
	int NPCBet = 0;					//　NPC　がベットする金額
	int AllHumanBet = 0;			//ユーザーのベットした全金額
	int AllNPCBet = 0;				//　NPC　がベットした全金額
	int turn = 1;					//ターン数

	//true == ベット一回目、false == ベット二回目
	bool ChangeTime = true;

	//ユーザーの結果
	Hand HumanHand;
	//　NPC　の結果
	Hand NPCHand;

	for (turn; turn <= TURN_MAX; turn++)
	{
		srand(time(0));

		for (int i = 0; i < 2; i++)
		{
			//カードを配る
			Distribute(&HumanHand, &NPCHand, &ChangeTime);

			//情報を確認
			ShowInfo(turn, HumanChip, NPCChip);

			//役を確認
			cout << "あなたの現在の役：";
			ShowRole(HumanHand);

			//手札の中の最強の数字を確認
			cout << "あなたの現在の手札の最大数：";
			ShowNum(HumanHand);
			cout << "\n";

			//賭ける
			Bet(&HumanChip, &HumanBet, &NPCChip, &NPCBet);

			//賭け金全てを入れておく
			AllHumanBet += HumanBet;
			AllNPCBet += NPCBet;

			//とりあえず両者の手持ちを、ベットした分だけ減らす
			HumanChip = HumanChip - HumanBet;
			NPCChip = NPCChip - NPCBet;
		}

		//結果表示
		Judge(&HumanHand, &NPCHand, &HumanChip, &NPCChip, &AllHumanBet, &AllNPCBet);

		system("pause");
		system("cls");

		//
		/*if (ChangeTime)
		{
			ChangeTime = false;
		}
		else
		{
			ChangeTime = true;
		}*/
	}

	cout << "--------------------------------" << "\n";

	system("pause");

	return 0;
}

//カードを配る
void Distribute(Hand* hHand, Hand* nHand, bool* cTime)
{
	if (!(*cTime))
	{

	}
	//役と、最大の数字を決める
	(*hHand).role = rand() % 10;
	(*hHand).number = rand() % 12;

	(*nHand).role = rand() % 10;
	(*nHand).number = rand() % 12;
}

//情報開示
void ShowInfo(int turn, int hGold, int nGold)
{
	cout << "--------------------------------" << "\n";
	cout << turn << "ターン目" << "\n";
	cout << "あなたの手持ち金額：" << hGold << "G" << "\n";
	cout << "NPCの手持ち金額：" << nGold << "G" << "\n\n";
}

//役を開示
void ShowRole(Hand info)
{
	//ユーザーの役を見て判断
	switch (info.role)
	{
	case HIGH_CARD:
		cout << "ノーペア" << "\n";
		break;

	case A_PEIR:
		cout << "ワンペア" << "\n";
		break;

	case TWO_PEIR:
		cout << "ツーペア" << "\n";
		break;

	case THREE_CARD:
		cout << "スリーカード" << "\n";
		break;

	case STRAIGHT:
		cout << "ストレート" << "\n";
		break;

	case FLUSH:
		cout << "フラッシュ" << "\n";
		break;

	case FULLHOUSE:
		cout << "フルハウス" << "\n";
		break;

	case FOUR_CARD:
		cout << "フォーカード" << "\n";
		break;

	case STRAIGHT_FLASH:
		cout << "ストレートフラッシュ" << "\n";
		break;

	case ROYAL_STRAIGHT_FLASH:
		cout << "ロイヤルストレートフラッシュ" << "\n";
		break;
	}
}

//手札の最強の数字を開示
void ShowNum(Hand info)
{
	//手札の中の最強の数字を見て判断
	switch (info.number)
	{
	case 0:
		cout << "A";
		break;

	case 11:
		cout << "J";
		break;

	case 12:
		cout << "Q";
		break;

	case 13:
		cout << "K";
		break;

	default:
		cout << info.number;
		break;
	}
}

//賭ける
void Bet(int* hChip, int* hBet, int* nChip, int* nBet)
{
	if (hChip != 0)
	{
		cout << "ベットする金額を入力：";
		cin >> (*hBet);
		cout << "\nあなたは" << (*hBet) << "Gをベットしました。" << "\n";
	}
	else
	{
		cout << "あなたはチップを持っていません。" << "\n";
	}

	if (nChip != 0)
	{
		(*nBet) = ((rand() % 10) * 10);
		cout << "NPCは" << (*nBet) << "Gをベットしました。" << "\n";
	}
	else
	{
		cout << "NPCはチップを持っていません。" << "\n";
	}

	cout << "\n";
}

//結果表示
void Judge(Hand* hHand, Hand* nHand, int* hChip, int* nChip, int* allHBet, int* allNBet)
{
	//両者の結果表示
	cout << "あなたの役：";
	ShowNum((*hHand));
	cout << "の";
	ShowRole((*hHand));

	cout << "NPCの役：";
	ShowNum((*nHand));
	cout << "の";
	ShowRole((*nHand));

	//ノーペア
	if ((*hHand).role == HIGH_CARD && (*nHand).role == HIGH_CARD)
	{
		cout << "引き分け" << "\n\n";

		(*hChip) += (*allHBet);
		(*nChip) += (*allNBet);
	}

	//ユーザーの役の方が強い
	if ((*hHand).role > (*nHand).role)
	{
		cout << "あなたの勝利" << "\n\n";

		(*hChip) = ((*hChip) + (*allHBet) + (*allNBet));
	}

	//　NPC　の役の方が強い
	else if ((*hHand).role < (*nHand).role)
	{
		cout << "NPCの勝利" << "\n\n";

		(*nChip) = ((*nChip) + (*allNBet) + (*allHBet));
	}

	//結果が両方同じ
	else if ((*hHand).role == (*nHand).role)
	{
		//ユーザーの最大数が大きい
		if ((*hHand).number > (*nHand).number)
		{
			cout << "あなたの勝利" << "\n\n";

			(*hChip) = ((*hChip) + (*allHBet) + (*allNBet));
		}

		//　NPC　の最大数が大きい
		else if ((*hHand).number < (*nHand).number)
		{
			cout << "NPCの勝利" << "\n\n";

			(*nChip) = ((*nChip) + (*allNBet) + (*allHBet));
		}

		//ユーザー、NPC　ともに最大数が同じ
		else if ((*hHand).number == (*nHand).number)
		{
			cout << "引き分け" << "\n\n";

			(*hChip) += (*allHBet);
			(*nChip) += (*allNBet);
		}
	}
}