#pragma once
#include "Player.h"

const float CARD_POSITION_Z_HUMAN = -0.9f;

enum
{
	FIRST_CARD,
	SECOND_CARD,
	THIRD_CARD,
	FOURTH_CARD,
	FIFTH_CARD
};

class Human :public Player
{
private:

	//Update処理前の状態を記憶しておくための変数
	std::vector<Hand> preHandState_;

	//交換終了か
	bool changeEndFlg_;

	//声出すよ
	bool voiceFlg_;

	Npc *pNpc_;				//相手の情報
	IGameObject *pParent_;	//メンバとして親の情報を入れておく

	//NPCクラスでイテレータいじるから前の情報保存用(手札の情報)
	std::vector<Hand> currentHumanCard_;

	//NPCクラスでイテレータいじるから前の情報保存用(どの手札見てるのか)
	std::vector<Hand>::iterator currentHumanitr_;

	//ヒューマンの手札
	std::vector<Hand> HumanCard_;

	//ヒューマンの手札の何枚目を選んでいるか
	std::vector<Hand>::iterator Humanitr_;

public:
	Human(IGameObject* parent);
	~Human();

	//ゲッター
	std::vector<Hand> GetHand();

	//初期化
	void Initialize() override;
	
	//ゲーム関係の初期化はこちらで行う
	void InitializeGame() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;

	//ゲームが終了しているかどうか
	void StateCheckEndGame();

	//入力を受付ける
	void InputBet();

	////////////////////////////////////////////////////

	//カードの状態変更

	//交換したいカードの選択
	void ChangeCardSelect();

	//カード交換、状態変化
	void ChangeCard();

	//交換したいカード決定、状態変化
	void SelectCard();

	//手札の状態を初期化(STOPにする)
	void HumanCardStateReset();

	///////////////////////////////////////////////////////////////////

	//カードのポジション設定

	//交換したいカードのポジション設定
	void CardSetPosition();

	//カードが配られる時のポジション
	void MoveIn(std::vector<Hand>::iterator &Roopitr);

	//カード交換(画面外へ移動)時のポジション
	void MoveOut(std::vector<Hand>::iterator &Roopitr, int hCard);

	//交換したいカード選択中のポジション
	void MoveCheck(std::vector<Hand>::iterator &Roopitr);

	//カード未選択のポジション
	void MoveUncheck(std::vector<Hand>::iterator &Roopitr);

	////////////////////////////////////////////////////////////////////

	//描画時に使用する行列を生成する

	//描画時に使用する各カードの行列をセットする
	void SetCardMatrix(int &hMat, D3DXMATRIX  cardMat[HAND_MAX]);

	//停止状態の行列
	void CreateMatrixStop(D3DXMATRIX &tMat, int & hMat, D3DXMATRIX &rXMat, D3DXMATRIX * cardMat);

	//水平方向へ移動する行列(左右)
	void CreateMatrixHorizontal(D3DXMATRIX &tMat, std::vector<Hand>::iterator &Roopitr, int & hMat, D3DXMATRIX &rXMat, D3DXMATRIX * cardMat);

	//垂直方向へ移動する行列(上下)
	void CreateMatrixVertical(D3DXMATRIX &tMat, std::vector<Hand>::iterator &Roopitr, int & hMat, D3DXMATRIX &rXMat, D3DXMATRIX * cardMat);

	//カードを回転させる行列
	void TurnCard(D3DXMATRIX & tMat, int & hMat, D3DXMATRIX & rXMat, D3DXMATRIX * cardMat, float posx);

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	
};