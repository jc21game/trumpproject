#include "PlayScene.h"
#include <iostream>
#include <vector>
#include <time.h>
//テス
#include "Engine/Model.h"
#include "Image.h"

#pragma once

//役
#define THREECARD 3
#define TWOPAIR 2
#define ONEPAIR 1

//手札配列の最大値
#define HAND_MAX 5
#define CARD_MARK_MAX 4
#define CARD_NUMBER_MAX 13
#define CARD_SUM 52

//モデル番号の最大値
//const int MODEL_MAX = 6;

////構造体カードの型
//struct Hand
//{
//	int num = -1;			//カードのナンバー
//	int mark = -1;			//カードのマーク
//	std::string modelname;	//モデルの名前
//	int status;
//	float yPosition = 1.2f;
//	float xPosition = -0.6f;
//	float rotateY = 180.0f;
//};

//テス
//enum STATUS
//{
//	STOP,		//基本状態
//	UNCHECK,	//カードのy座標を下げる状態
//	CHECK,		//カードのy座標を上げる状態
//	MOVE_OUT,	//カードのx座標を右に移動する状態
//	MOVE_IN,	//カードのx座標を左に移動する状態
//	hairu,		//UNCHECKと同じことをしているのでいらない？
//	LOCK,		//リタイア画像出してる時にカードは動かさない
//};



//struct vhand
//{
//	int status;
//	float yPosition = 1.2f;
//	float xPosition = -0.6f;
//	float rotateY = 180.0f;
//};

class AI : public IGameObject
{
private:

protected:

	bool isDuplication_;

	//テス
	//手札のモデル番号登録用
	//int hModel_[MODEL_MAX];

	//状態管理用仮想手札
	//vhand virtualhand_[HAND_MAX];

	//何番目の手札を指しているのかを保持する
	int handnum_ = 0;

	//どっちのターンか
	//static int turn_;
	//全てのカードがSTOPか
	bool allStopFlg_;

	//継承先でプレイシーンの値をコピーして使う
	std::vector<Hand> cardSum_;
	std::vector<Hand>::iterator shuffleitr_;

public:
	AI(IGameObject * parent);
	~AI();
	//初期化
	virtual void Initialize() override;

	//更新
	virtual void Update() override;

	//描画
	virtual void Draw() override;

	//開放
	virtual void Release();// override;

	//いくらベットするか、何を交換するかの純粋仮想関数
	//ベットと交換が同一関数はおかしい
	//ここでは交換の関数として扱う
	//戻り値としてナニを返すの？
	//bool型配列[5]とか考えてたけど違う？
	//忘れてたけど手札を渡してあげないと判定できないじゃん
	virtual void Thinking(std::vector<Hand> hand, bool (&changeFlg)[5]) = 0;



	//テス

	bool CheckPair(std::vector<Hand> hand);
	//第二引数: 何枚以上多く持っていればいいか
	bool CheckMark(std::vector<Hand> hand, int remaining);
	bool CheckStraight(std::vector<Hand> hand, int remaining);

	void ChangeRand(std::vector<Hand> hand, bool(&changeFlg)[5]);

	void ChangePair(std::vector<Hand> hand, bool (&changeFlg)[5]);

	void ChangeFlash(std::vector<Hand> hand, bool(&changeFlg)[5]);

	void ChangeStraight(std::vector<Hand> hand, bool(&changeFlg)[5], int remaining);

	void ChangeHigh(std::vector<Hand> hand, bool(&changeFlg)[5], int leavenum);

	void ChangePairBluff(std::vector<Hand> hand, bool(&changeFlg)[5]);

	//ブラフするかどうか
	virtual bool IsDoBluf(int role, int numMax) = 0;

	//ベットいくら賭けるか
	virtual int Howmuchbet(int role, bool bluf, int maxbet, int chip, int totalbet) = 0;

	int Detailbet(int betdata, int maxbet, int chip, int totalbet);
};