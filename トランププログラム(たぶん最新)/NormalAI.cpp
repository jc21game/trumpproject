#include "NormalAI.h"
#include "Engine/Model.h"


//int NormalAI::turn_ = HUMAN_TURN;

NormalAI::NormalAI(IGameObject* parent) :
	AI(parent),
	isDuplication_(false), allStopFlg_(true)
{
}

NormalAI::~NormalAI()
{
}

void NormalAI::Initialize()
{
}

void NormalAI::Update()
{
}

void NormalAI::Draw()
{
}

void NormalAI::Release()
{
}

void NormalAI::Thinking(std::vector<Hand> hand, bool(&changeFlg)[5])
{

	//////////////////　　堅実？？な考え方　　////////////////////
	//先にペアの有無を確認する
	//↑bool型でペア発見したらtrueを返す関数を作って使う
	//ペアがある場合にはペアを残した考え方を実行
	//↑上の関数でtrueの時

	//ペアがない場合には各マークの枚数を確認する
	//↑手書きしてたやつの考え方を使えば同一カードがどれくらいあるかは分かる(どのマークかまでは分からないが)
	//関数にするかな

	//最多マークの枚数が3枚以上あるなら
	//フラッシュを狙う考え方を実行
	//↑の関数?のスコアを見て

	//最多枚数のマークが2枚以下(2枚)なら
	//ペアを残した考え方(全交換)、あるいはランダムで交換を実行

	srand((unsigned int)time(NULL));

	int bluff = rand() % 5;

	if (bluff)
	{
		if (CheckPair(hand))
		{
			ChangePair(hand, changeFlg);
		}
		else
		{
			for (int remaining = 5; remaining >= 3; remaining--)
			{
				if (CheckStraight(hand, remaining))
				{
					ChangeStraight(hand, changeFlg, remaining);
					return;
				}
				else if (CheckMark(hand, remaining))
				{
					ChangeFlash(hand, changeFlg);
					return;
				}
			}
			ChangeHigh(hand, changeFlg, 3);
		}
	}
	else
	{
		if (CheckPair(hand))
		{
			ChangePairBluff(hand, changeFlg);
		}
		else
		{
			for (int remaining = 5; remaining >= 4; remaining--)
			{
				if (CheckStraight(hand, remaining))
				{
					ChangeStraight(hand, changeFlg, remaining);
					return;
				}
				else if (CheckMark(hand, remaining))
				{
					ChangeFlash(hand, changeFlg);
					return;
				}
			}
			ChangeHigh(hand, changeFlg, 4);
		}
	}
}

bool NormalAI::IsDoBluf(int role, int numMax)
{
	//セミブラフ(2ペアか3カードの時に発動)
	if (role == TWOPAIR || role == THREECARD)
	{
		return true;
	}
	return false;
}

int NormalAI::Howmuchbet(int role, bool bluf, int maxbet, int chip, int totalbet)
{
	int roleData = role;
	//ノーペアの時はワンペアの時と同様のかけ方を行う
	if (roleData == 0)
	{
		roleData = 1;
	}
	//所持金が0の時
	if (chip <= 0)
	{
		return 0;
	}
	//個別に設定が必要な時はcase内で
	//下記の計算 : (roleに応じての最低値) + 0 or 5 or 10
	srand((unsigned int)time(NULL));
	int betdata;
	if (bluf)
	{
		betdata = ((roleData + 3) * 5) + ((rand() % 3) * 5);
	}
	else
	{
		betdata = ((roleData + 1) * 5) + ((rand() % 3) * 5);
	}
	return Detailbet(betdata, maxbet, chip, totalbet);
}
