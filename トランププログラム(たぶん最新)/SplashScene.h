#pragma once

#include "Engine/Global.h"

//テストシーンを管理するクラス

const int nameMax = 3;

class SplashScene : public IGameObject
{
	int hPict_[nameMax];
	float alpha_;
	bool turnFlg_;
	bool changeFlg_;

public:

	//コンストラクタ
	//引数：parent  親オブジェクト（SceneManager）
	SplashScene(IGameObject* parent);

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//完全にロゴが消えたら画面遷移
	void SceneChanege();

	//ロゴのフェードイン、フェードアウト
	void LogoFade();

	//描画
	void Draw() override;

	//開放
	void Release() override;
};