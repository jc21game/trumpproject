#include <vector>
#include "Image.h"

//3D画像を管理する
namespace Image
{
	//ロード済みの画像データ一覧
	std::vector<ImageData*>	dataList_;

	//画像をロード
	int Load(std::string fileName)
	{
		ImageData* pData = new ImageData;
		assert(pData != nullptr);

		pData->fileName = fileName;

		//開いたファイル一覧から同じファイル名のものが無いか探す
		bool isExist = false;
		for (unsigned int i = 0; i < dataList_.size(); i++)
		{
			//すでに開いている場合
			if (dataList_[i] != nullptr && dataList_[i]->fileName == fileName)
			{
				pData->pSprite = dataList_[i]->pSprite;
				isExist = true;
				break;
			}
		}

		//新たにファイルを開く
		if (isExist == false)
		{
			pData->pSprite = new Sprite;
			assert(pData->pSprite != nullptr);

			pData->pSprite->Load(fileName.c_str());

		}

		//新たに追加
		dataList_.push_back(pData);

		return dataList_.size() - 1;
	}



	//描画
	void Draw(int handle)
	{
		dataList_[handle]->pSprite->Draw(dataList_[handle]->matrix);
	}

	//透明度調整用描画
	void Draw(int handle, const D3DXVECTOR3 position, float alpha)
	{
		dataList_[handle]->pSprite->Draw(dataList_[handle]->matrix, position, alpha);
	}

	//任意の画像を開放
	void Release(int handle)
	{

		//同じモデルを他でも使っていないか
		bool isExist = false;
		for (unsigned int i = 0; i < dataList_.size(); i++)
		{
			//すでに開いている場合
			if (dataList_[i] != nullptr && i != handle && dataList_[i]->pSprite == dataList_[handle]->pSprite)
			{
				isExist = true;
				break;
			}
		}

		//使ってなければモデル解放
		if (isExist == false)
		{
			SAFE_DELETE(dataList_[handle]->pSprite);
		}

		SAFE_DELETE(dataList_[handle]);
	}



	//全ての画像を解放
	void AllRelease()
	{
		for (unsigned int i = 0; i < dataList_.size(); i++)
		{
			if( dataList_[i] != nullptr )
			{
				Release(i);
			}
		}
		dataList_.clear();
	}

	//ワールド行列を設定
	void SetMatrix(int handle, D3DXMATRIX matrix)
	{
		dataList_[handle]->matrix = matrix;
	}
}

