#pragma once
#include "Engine/global.h"

//#define PIC_MAX 8
//#define PIC_MATRIX_MAX 8

//難易度選択画面シーンを管理するクラス
class SelectDifficult : public IGameObject
{
	enum
	{
		BEGINNER,		//初心者
		BEGINNER_SELECT,
		NORMAL,			//中級者
		NORMAL_SELECT,
		GAMBLER,		//上級者
		GAMBLER_SELECT,
		BACK_GROUND,	//背景
		SELECT_DIFF,	//「難易度選択」の文字
		PICT_MAX,
	};

	int hPict_[PICT_MAX];	//画像番号;
	int Select_;			//どの難易度を選択しているか

	D3DXMATRIX mat[PICT_MAX];

public:
	//コンストラクタ
	//引数：parent  親オブジェクト（SceneManager）
	SelectDifficult(IGameObject* parent);

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;
};