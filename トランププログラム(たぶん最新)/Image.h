#pragma once
#include <string>
//#include <string>
//#include "Engine/Global.h"
#include "Engine/Sprite.h"

//2D画像を管理する
namespace Image
{
	//画像情報（本当はクラスにするべきか？）
	struct ImageData
	{
		//ファイル名
		std::string fileName;

		//ロードした画像データのアドレス
		Sprite*		pSprite;

		//行列
		D3DXMATRIX	matrix;

		//コンストラクタ
		ImageData() : fileName(""), pSprite(nullptr)
		{
			D3DXMatrixIdentity(&matrix);
		}
	};

	//画像をロード
	//すでに同じ名前のファイルをロード済みの場合は、既存のデータの番号を返す
	//引数：fileName　ファイル名
	//戻値：その画像データに割り当てられた番号
	int Load(std::string fileName);

	//描画
	//引数：handle	描画したい画像の番号
	//引数：matrix	ワールド行列
	void Draw(int handle);
	void Draw(int handle, D3DXVECTOR3 position, float alpha);

	//任意の画像を開放
	//引数：handle	開放したいモデルの番号
	void Release(int handle);

	//全ての画像を解放
	//（シーンが切り替わるときは必ず実行）
	void AllRelease();

	//ワールド行列を設定
	//引数：handle	設定したい画像の番号
	//引数：matrix	ワールド行列
	void SetMatrix(int handle, D3DXMATRIX matrix);
}