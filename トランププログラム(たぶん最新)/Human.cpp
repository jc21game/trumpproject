#include "Human.h"
#include "Npc.h"
#include "PlayScene.h"
#include "Engine/Audio.h"

Human::Human(IGameObject* parent) :Player(parent), HumanCard_(HAND_MAX), preHandState_(HAND_MAX), Humanitr_(HumanCard_.begin()), voiceFlg_(true)
{
	pParent_ = parent;
}


Human::~Human()
{
}

//これをターンの頭に呼ぶようにする
void Human::Initialize()
{
}

void Human::Update()
{
	if (PlayScene::gamestatus_ == PLAY_GAME)
	{
		//最初にこれをしとかないとのちのち相手の手札を呼べなくなる
		if ( pNpc_ == nullptr )
		{
			pNpc_ = ((PlayScene*)pParent_)->GetNpc();
		}
		//ゲームが終了しているかどうか
		StateCheckEndGame();

		if (Humanitr_->status != LOCK)
		{
			if (!turn_)
			{
				if (!changeEndFlg_)
				{
					//交換したいカードの選択
					ChangeCardSelect();
				}

				//交換したいカードのポジション設定
				CardSetPosition();
			}

			//手札の状態を初期化(STOPにする)
			HumanCardStateReset();

			//NPCにターンを渡す
			if (changeEndFlg_ && allStopFlg_)
			{
				//NPCのボイス鳴らすフラグの初期化
				voiceFlg_ = true;
				turn_ = NPC_TURN;
			}
		}
	}
}

void Human::HumanCardStateReset()
{
	//手札の状態を初期化(STOPにする)
	for (auto Roopitr = HumanCard_.begin(); Roopitr != HumanCard_.end(); ++Roopitr)
	{
		if (Roopitr->status != STOP)
		{
			allStopFlg_ = false;
			break;
		}
		allStopFlg_ = true;
	}
}

void Human::CardSetPosition()
{
	auto PreRoopitr = preHandState_.begin();
	int hCard = 0;	//カードモデルのハンドル(番号)

	//各カードのポジションの設定
	for (auto Roopitr = HumanCard_.begin(); Roopitr != HumanCard_.end(); ++Roopitr)
	{
		switch (Roopitr->status)
		{
		case UNCHECK:
		case hairu:
			MoveUncheck(Roopitr);
			break;
		case CHECK:
			MoveCheck(Roopitr);
			break;
		case MOVE_OUT:
			MoveOut(Roopitr, hCard);
			break;
		case MOVE_IN:
			MoveIn(Roopitr);
			break;
		}

		hCard++;
		//更新前最後のカードの状態を記憶
		if (PreRoopitr != preHandState_.end())
		{
			PreRoopitr->status = Roopitr->status;
			++PreRoopitr;
		}
	}
}

//カードが配られる時のポジション設定
void Human::MoveIn(std::vector<Hand>::iterator &Roopitr)
{
	if (Roopitr->xPosition >= -CARD_POSITION_X)
	{
		Roopitr->xPosition -= MOVE_CARD;
		if (Roopitr->xPosition <= -CARD_POSITION_X)
		{
			Roopitr->status = hairu;
		}
	}
}

//カード交換(画面外へ移動)時のポジション設定
void Human::MoveOut(std::vector<Hand>::iterator &Roopitr, int hCard)
{
	if (Roopitr->xPosition < REVERSE_CARD)
	{
		Roopitr->xPosition += MOVE_CARD;
		if (Roopitr->xPosition >= REVERSE_CARD)
		{
			//ここでカードを交換する

			//カードのモデルを呼びなおす
			Roopitr = Player::ChangeCard(Roopitr);
			Roopitr->status = MOVE_IN;
		}
	}
}

//交換したいカード選択中のポジション設定
void Human::MoveCheck(std::vector<Hand>::iterator &Roopitr)
{
	Roopitr->yPosition += MOVE_CARD;
	if (Roopitr->yPosition >= UNSELECT_CARD)
	{
		Roopitr->yPosition = UNSELECT_CARD;
	}
}

//カード未選択のポジション設定
void Human::MoveUncheck(std::vector<Hand>::iterator &Roopitr)
{
	if (Roopitr->yPosition > SELECT_CARD)
	{
		Roopitr->yPosition -= MOVE_CARD;
		if (Roopitr->yPosition <= SELECT_CARD)
		{
			Roopitr->status = STOP;
		}
	}
}

//交換したいカードの選択
void Human::ChangeCardSelect()
{
	if (voiceFlg_)
	{
		//ボイス入れる(ランダム)
		std::string voiceName[3] =
		{
			"line-girl1_line-girl1-ちょっとまずいかも… 3",
			"line-girl1_line-girl1-おっ！ 3",
			"line-girl1_line-girl1-ん〜？1"
		};

		int hVoice = rand() % 3;
		Audio::Play((char*)voiceName[hVoice].c_str());
		voiceFlg_ = false;
	}

	//選択カード移動
	if (Input::IsKeyDown(DIK_RIGHT) && (handnum_ < FIFTH_CARD) && (Humanitr_ != HumanCard_.end()))
	{
		handnum_++;
		++Humanitr_;
	}

	if (Input::IsKeyDown(DIK_LEFT) && (handnum_ > FIRST_CARD) && (Humanitr_ != HumanCard_.begin()))
	{
		handnum_--;
		--Humanitr_;
	}

	//カード選択
	if (Input::IsKeyDown(DIK_SPACE))
	{
		//交換したいカード決定、状態変化
		SelectCard();
	}

	if (Input::IsKeyDown(DIK_RETURN))
	{
		//カード交換、状態変化
		ChangeCard();
	}

}

//カード交換、状態変化
void Human::ChangeCard()
{
	for (auto Roopitr = HumanCard_.begin(); Roopitr != HumanCard_.end(); ++Roopitr)
	{
		if (Roopitr->status == CHECK)
		{
			Roopitr->status = MOVE_OUT;
		}

		//カード交換なしで勝負したい時は状態変化なし
		else if(Roopitr->status == STOP)
		{
		}

		//交換の有無にかかわらずフラグはtrueにして終了
		changeEndFlg_ = true;
	}
}

//交換したいカード決定、状態変化
void Human::SelectCard()
{
	if (Humanitr_->status == CHECK)
	{
		Humanitr_->status = UNCHECK;
	}

	else
	{
		//これでカードが選ばれた状態になる
		Humanitr_->status = CHECK;
	}
}

//ゲームが終了しているかどうか
void Human::StateCheckEndGame()
{
	//プレイシーンで「リザルト画面を表示しているかどうか」の情報取得
	if (PlayScene::gamestatus_ == END_GAME)
	{
		//表示中なら
		for (auto Roopitr = HumanCard_.begin(); Roopitr != HumanCard_.end(); ++Roopitr)
		{
			Roopitr->status = LOCK;
		}
	}

	else
	{
		auto preRoopitr = preHandState_.begin();
		for (auto Roopitr = HumanCard_.begin(); Roopitr != HumanCard_.end(); ++Roopitr)
		{
			Roopitr->status = preRoopitr->status;
			++preRoopitr;
		}
	}
}

void Human::Draw()
{

	D3DXMATRIX cardMat[HAND_MAX];
	int hMat = 0;	//各カードにセットする行列のハンドル(番号)

	//描画時に使用する各カードの行列を生成する
	SetCardMatrix(hMat, cardMat);

	for (int i = 0; i < HAND_MAX; i++)
	{
		//カードにまだテクスチャが入っていないときはスキップする
		if ( HumanCard_[i].modelname != "" )
		{
			//手札のモデルを描画するときにテクスチャを差し替え
			Model::SetTexture(HumanCard_[i].modelname, hModel_[i]);
			Model::SetMatrix(hModel_[i], cardMat[i]);
			Model::Draw(hModel_[i]);
		}
	}
}

//描画時に使用する各カードの行列を生成する
void Human::SetCardMatrix(int &hMat, D3DXMATRIX  cardMat[HAND_MAX])
{
	for (auto Roopitr = HumanCard_.begin(); Roopitr != HumanCard_.end(); ++Roopitr)
	{
		D3DXMATRIX tMat, rXMat, rYMat;
		switch (Roopitr->status)
		{
			//縦移動
		case CHECK:
		case UNCHECK:
		case hairu:
			CreateMatrixVertical(tMat, Roopitr, hMat, rXMat, cardMat);
			break;

			//横移動
		case MOVE_OUT:
		case MOVE_IN:
			CreateMatrixHorizontal(tMat, Roopitr, hMat, rXMat, cardMat);
			break;
			//停止状態
		default:
			CreateMatrixStop(tMat, hMat, rXMat, cardMat);
			break;
		}
		hMat++;
	}
}

//停止状態の行列
void Human::CreateMatrixStop(D3DXMATRIX &tMat, int & hMat, D3DXMATRIX &rXMat, D3DXMATRIX * cardMat)
{
	if (changeEndFlg_)
	{
		TurnCard(tMat, hMat, rXMat, cardMat, CARD_POSITION_Y);
	}

	else if (handnum_ == hMat)
	{
		TurnCard(tMat, hMat, rXMat, cardMat, CARD_POSITION_Y + 0.05f);
	}
	else
	{
		TurnCard(tMat, hMat, rXMat, cardMat, CARD_POSITION_Y);
	}
}

void Human::TurnCard(D3DXMATRIX & tMat, int & hMat, D3DXMATRIX & rXMat, D3DXMATRIX * cardMat, float posx)
{
	D3DXMatrixTranslation(&tMat, (-0.6f + (hMat * CARD_INTERVAL)), posx, CARD_POSITION_Z_HUMAN);
	D3DXMatrixRotationX(&rXMat, D3DXToRadian(ROTATION_ANGLE));
	cardMat[hMat] = rXMat * tMat;
}

//水平方向へ移動する行列(左右)
void Human::CreateMatrixHorizontal(D3DXMATRIX &tMat, std::vector<Hand>::iterator &Roopitr, int & hMat, D3DXMATRIX &rXMat, D3DXMATRIX * cardMat)
{
	D3DXMatrixTranslation(&tMat, (Roopitr->xPosition + (hMat * CARD_INTERVAL)), Roopitr->yPosition + 0.05f, CARD_POSITION_Z_HUMAN);
	D3DXMatrixRotationX(&rXMat, D3DXToRadian(ROTATION_ANGLE));
	cardMat[hMat] = rXMat * tMat;
}

//垂直方向へ移動する行列(上下)
void Human::CreateMatrixVertical(D3DXMATRIX &tMat, std::vector<Hand>::iterator &Roopitr, int & hMat, D3DXMATRIX &rXMat, D3DXMATRIX * cardMat)
{
	D3DXMatrixTranslation(&tMat, (Roopitr->xPosition + (hMat * CARD_INTERVAL)), Roopitr->yPosition + 0.05f, CARD_POSITION_Z_HUMAN);
	D3DXMatrixRotationX(&rXMat, D3DXToRadian(ROTATION_ANGLE));
	cardMat[hMat] = rXMat * tMat;
}

void Human::Release()
{
}



void Human::InitializeGame()
{
	//１回１回のベット金額を初期化
	bet_ = 0;

	//合計ベット金額を初期化
	totalBet_ = 0;

	//ベット金額の上限を初期化
	//maxBet_ = 100;

	int i = 0;
	changeEndFlg_ = false;
	handnum_ = 0;

	//「降りない」状態にする
	isFold_ = false;

	//プレイシーンの山札にアクセスして山札の情報を書き込む
	shuffleCardSum_ = *((PlayScene*)pParent_)->GetShuffleCardSum_();
	shuffleitr_ = shuffleCardSum_.begin();

	Humanitr_ = HumanCard_.begin();

	for (int i = 0; i < HAND_MAX; i++)
	{
		Humanitr_->mark = shuffleitr_->mark;
		Humanitr_->num = shuffleitr_->num;
		Humanitr_->modelname =
			("Data/CardModels/CardTexture" + name[Humanitr_->mark] + number[Humanitr_->num] + ".jpg");
		++shuffleitr_;
		++Humanitr_;
	}

	//シャッフル後の山札の一枚目をイテレータに登録
	shuffleitr_ = shuffleCardSum_.begin();

	//モデル配列にカードの絵柄を入れる
	for (auto Roopitr = HumanCard_.begin(); Roopitr != HumanCard_.end(); ++Roopitr)
	{
		hModel_[i] = Model::Load("Data/CardModels/Card.fbx");
		i++;

		//ついでに構造体を初期化しておく
		Roopitr->yPosition = ININ_POSITION_Y;
		Roopitr->xPosition = INIT_POSITION_X;
		Roopitr->rotateY = INIT_ROTATE_ANGLE;
	}

	((PlayScene*)pParent_)->RemoveCardToInit();
	Humanitr_ = HumanCard_.begin();
}

void Human::InputBet()
{
	//入力に対応した金額をベットする
	if (Input::IsKeyDown(DIK_1))
	{
		bet_ = FIVE_CHIP;

		//賭けられるかチェック
		CanBet(bet_);
	}
	else if (Input::IsKeyDown(DIK_2))
	{
		bet_ = TWENTY_FIVE_CHIP;

		//賭けられるかチェック
		CanBet(bet_);
	}
	else if (Input::IsKeyDown(DIK_3))
	{
		bet_ = FIFTY_CHIP;

		//賭けられるかチェック
		CanBet(bet_);
	}
	else if (Input::IsKeyDown(DIK_4))
	{
		bet_ = ONE_HUNDRED_CHIP;

		//賭けられるかチェック
		CanBet(bet_);
	}

	//賭け終了
	else if (Input::IsKeyDown(DIK_UP))
	{
		//現在、一回目のベットタイムなら
		if (((PlayScene*)pParent_)->GetBetTime() == false)
		{
			//賭けずにEキーを押したとしても
			if (bet_ == 0)
			{
				//必ず参加費として5Gは賭けさせる
				Bet(FIVE_CHIP);
			}
		}

		//「賭けた状態」にする
		isBet_ = true;
	}
}

std::vector<Hand> Human::GetHand()
{
	return HumanCard_;
}