#include "SceneBetweenPassData.h"


namespace SceneBetweenPassData
{

	AI_LEVEL aiLevel_;
	//int meter_ = 0;

	//プレイシーン側でセットする用
	void SetLevelData(AI_LEVEL aiLevel)
	{
		aiLevel_ = aiLevel;
	}

	//難易度選択画面で選択した難易度を持ってくる用
	AI_LEVEL GetLevelData()
	{
		return	(AI_LEVEL)(aiLevel_);
	}

	//void SetMeterStatus(int meter)
	//{
	//	meter_ = meter;
	//}

	//int GetMeterStatus()
	//{
	//	return	meter_;
	//}
}

