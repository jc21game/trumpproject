#pragma once
#include "Player.h"
#include "AI.h"
#include "BeginnerAI.h"
#include "NormalAI.h"
#include "GamblerAI.h"
#include "SceneBetweenPassData.h"
//ベット関連の定数
const int BET_FIVE = 5;
const int BET_TWENTY_FIVE = 11;
const int BET_FIFTY = 14;
const float CARD_POSITION_Z_NPC = -0.2f;

enum
{
	PlayerDefault,
	PlayerSad,
	PlayerSmile,
	NPC_MAX
};

//NPCの表情のパターンの列挙
enum
{

};

class Npc :public Player
{
private:
	//メーターの情報をNPCが保持しておきたい
	int meter;

	//今の手札の役
	int role_;

	//仮置き交換手札選択フラグ
	bool tradeFlg_;

	//カードなどの座標やモデル番号
	D3DXMATRIX tMat_, rXMat_, rYMat_;
	D3DXMATRIX cardMat_[HAND_MAX];

	Human *pHuman_;			//Human情報のポインタ
	IGameObject *pParent_;	//メンバとして親の情報を入れておく

	AI *pAI_;	//AI情報のポインタ

	//Playerの表情を含めたモデルデータ配列( 今は1個なので配列ではない )
	int hModelNPC_[3];

	//メーター。NPCのモデルはこれで左右されるため作っておく、
	//わざわざ変数に入れず、戻り値を直接使ってもいいが...。仮なので作っておく
	int meter_;

	//NPCの手札
	std::vector<Hand> NPCCard_;

	//NPCの手札の何枚目を選んでいるか
	std::vector<Hand>::iterator NPCitr_;

	//ブラフするかどうか
	bool bluf_;
	//手札の最大値
	int numMax_;

	//ゲーム開始時のボイスを一度だけ鳴らすために
	bool startFlg_;

public:
	Npc(IGameObject * parent);
	~Npc();

	//ゲッター
	std::vector<Hand> GetHand();

	//初期化
	void Initialize() override;

	//ゲーム関係の初期化はこちらで行う
	void InitializeGame() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;

	//賭ける
	void SelectChip();

	//NPCのカードをひっくり返す
	void TurnNpcCard();

	////////////////////////////////////////////////////

	//カードの状態変更

	//交換したいカードの選択
	void ChangeCardSelect();

	//カード交換、状態変化
	void ChangeCard();

	//手札の状態を初期化(STOPにする)
	void NPCCardStateReset();

	///////////////////////////////////////////////////////////////////

	//カードのポジション設定

	//交換したいカードのポジション設定
	void CardSetPosition();

	//カードが配られる時のポジション
	void MoveIn(std::vector<Hand>::iterator &Roopitr);

	//カード交換(画面外へ移動)時のポジション
	void MoveOut(std::vector<Hand>::iterator &Roopitr, int hCard);

	//交換したいカード選択中のポジション
	void MoveCheck(std::vector<Hand>::iterator &Roopitr);

	//カード未選択のポジション
	void MoveUncheck(std::vector<Hand>::iterator &Roopitr);

	////////////////////////////////////////////////////////////////////

	//描画時に使用する行列を生成する

	//描画時に使用する各カードの行列をセットする
	void SetCardMatrix(int &hMat);

	//停止状態の行列
	void CreateMatrixStop(int i, std::vector<Hand>::iterator &Roopitr);

	//水平方向へ移動する行列(左右)
	void CreateMatrixHorizontal(std::vector<Hand>::iterator &Roopitr, int i);

	//垂直方向へ移動する行列(上下)
	void CreateMatrixVertical(std::vector<Hand>::iterator &Roopitr, int i);

	////////////////////////////////////////////////////////////////////


	
};

