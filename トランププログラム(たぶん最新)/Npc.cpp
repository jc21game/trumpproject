#include "Npc.h"
#include "Human.h"
#include "Judge.h"
#include "PlayScene.h"
#include "Engine/Model.h"
#include "Engine/Audio.h"

#define rotateSpeed 3.0f

Npc::Npc(IGameObject* parent) :Player(parent), NPCCard_(HAND_MAX), NPCitr_(NPCCard_.begin()), startFlg_(true)
{

	pParent_ = parent;
	
	switch (SceneBetweenPassData::GetLevelData())
	{
	case LEVEL_EASY:
		pAI_ = new BeginnerAI(this);
		break;
	case LEVEL_NORMAL:
		pAI_ = new NormalAI(this);
		break;
	case LEVEL_HARD:
		pAI_ = new GamblerAI(this);
		break;
	}
}


Npc::~Npc()
{
}

void Npc::Initialize()
{
	//モデルデータの名前
	std::string NPCname[NPC_MAX] =
	{
		"PlayerDefault2",
		//"Character",
		//"BackGround",
		"PlayerSad",
		"PlayerSmile"
	};

	//モデルデータを読み込み
	for (int i = 0; i < NPC_MAX; i++)
	{
		hModelNPC_[i] = Model::Load("data/" + NPCname[i] + ".fbx");
		assert(hModelNPC_[i] >= 0);
	}

	scale_ /= 4;
	position_.y = 0.2;
	position_.z = 0.6;
	//Model::SetTexture("Data/img_casino_slide_03.jpg", hModelNPC_[0]);
}

void Npc::InitializeGame()
{
	int i = 0;
	tradeFlg_ = true;

	//１回１回のベット金額を初期化
	bet_ = 0;

	//合計ベット金額を初期化
	totalBet_ = 0;

	//ベット金額の上限を初期化
	maxBet_ = 100;

	//プレイシーン経由でシャッフル後の山札とそのイテレータをコピー
	shuffleitr_ = ((PlayScene*)pParent_)->GetShuffleCardSum_()->begin();

	for (NPCitr_ = NPCCard_.begin(); NPCitr_ != NPCCard_.end(); NPCitr_++)
	{
		NPCitr_->mark = shuffleitr_->mark;
		NPCitr_->num = shuffleitr_->num;
		NPCitr_->modelname =
			("Data/CardModels/CardTexture" + name[NPCitr_->mark] + number[NPCitr_->num] + ".jpg");
		++shuffleitr_;
	}

	//初手の手札の状態
	role_ = (int)((Judge*)((PlayScene*)pParent_)->GetJudge()->handCheck(NPCCard_, 0));
	numMax_ = (int)((Judge*)((PlayScene*)pParent_)->GetJudge()->GetNpcMaxNum());
	//AIにブラフをするかを問う
	bluf_ = pAI_->IsDoBluf(role_, numMax_);

	//モデル配列にカードの絵柄を入れる
	for (auto Roopitr = NPCCard_.begin(); Roopitr != NPCCard_.end(); ++Roopitr)
	{
		//hModel_[i] = Model::Load(Roopitr->modelname);
		hModel_[i] = Model::Load("Data/CardModels/Card.fbx");

		D3DXMatrixTranslation(&tMat_, (-0.6f + (i * CARD_INTERVAL)), CARD_POSITION_Y, CARD_POSITION_Z_NPC);
		D3DXMatrixRotationX(&rXMat_, D3DXToRadian(-90));
		D3DXMatrixRotationY(&rYMat_, D3DXToRadian(Roopitr->rotateY));
		cardMat_[i] = rXMat_ * rYMat_ * tMat_;
		i++;
	}

	if (allStopFlg_) i++;

	for (auto Roopitr = NPCCard_.begin(); Roopitr != NPCCard_.end(); ++Roopitr)
	{
		Roopitr->yPosition = UNSELECT_CARD;
		Roopitr->xPosition = -CARD_POSITION_X;
		Roopitr->rotateY = BACK_CARD;
	}

	((PlayScene*)pParent_)->RemoveCardToInit();
	NPCitr_ = NPCCard_.begin();
}

void Npc::Update()
{

	if (startFlg_)
	{
		Audio::Play("info-girl1_info-girl1-頑張りましょう 4");
		startFlg_ = false;
	}

	//ゲーム中の行動
	if (PlayScene::gamestatus_ == PLAY_GAME)
	{
		if (turn_)
		{
			//どのカード交換するか
			if (tradeFlg_)
			{
				//交換するカードを抽出
				ChangeCardSelect();
			}

			//カード交換開始
			ChangeCard();

			//カードにポジションを与える
			CardSetPosition();

			//NPCのカード状態をリセットする
			NPCCardStateReset();

			if (allStopFlg_)
			{
				//ここで交換後のカードの状態を見る
				role_ = (int)((Judge*)((PlayScene*)pParent_)->GetJudge()->handCheck(NPCCard_, 0));

				//ここでベットタイム
				PlayScene::gamestatus_ = BET_TIME;
			}
		}
	}

	//NPCのカードをひっくり返す
	else if (PlayScene::gamestatus_ == TURN_CARD)
	{
		TurnNpcCard();
	}
}

void Npc::Draw()
{

	//ループ用
	int hMat = 0;

	SetCardMatrix(hMat);

	//カードの座標を登録
	for (int i = 0; i < HAND_MAX; i++)
	{
		if (NPCCard_[i].modelname != "")
		{
			Model::SetTexture(NPCCard_[i].modelname, hModel_[i]);
			Model::SetMatrix(hModel_[i], cardMat_[i]);
			Model::Draw(hModel_[i]);
		}
	}

	//モデルは常に同じ
	Model::SetMatrix(hModelNPC_[PlayerDefault], worldMatrix_);
	Model::Draw(hModelNPC_[PlayerDefault]);

	//手札に応じた表情の変化
	switch ( role_ )
	{
	case NO_PAIR:
		break;

	case ROYAL_STRAIGHT_FLASH:
		break;
	}
}

void Npc::SetCardMatrix(int &hMat)
{
	for (auto Roopitr = NPCCard_.begin(); Roopitr != NPCCard_.end(); ++Roopitr)
	{

		switch (Roopitr->status)
		{
			//縦移動
		case CHECK:
		case UNCHECK:
		case hairu:
			CreateMatrixVertical(Roopitr, hMat);
			break;
			//横移動
		case MOVE_OUT:
		case MOVE_IN:
			CreateMatrixHorizontal(Roopitr, hMat);
			break;
			//停止状態
		default:
			CreateMatrixStop(hMat, Roopitr);
			break;
		}
		hMat++;
	}
}

void Npc::CreateMatrixStop(int hMat, std::vector<Hand>::iterator &Roopitr)
{
	D3DXMatrixTranslation(&tMat_, (-0.6f + (hMat * CARD_INTERVAL)), CARD_POSITION_Y, CARD_POSITION_Z_NPC);
	D3DXMatrixRotationX(&rXMat_, D3DXToRadian(-90));
	D3DXMatrixRotationY(&rYMat_, D3DXToRadian(Roopitr->rotateY));
	cardMat_[hMat] = rXMat_ * rYMat_ * tMat_;
}

void Npc::CreateMatrixHorizontal(std::vector<Hand>::iterator &Roopitr, int hMat)
{
	D3DXMatrixTranslation(&tMat_, (Roopitr->xPosition + (hMat * CARD_INTERVAL)), Roopitr->yPosition + 0.05f, CARD_POSITION_Z_NPC);
	D3DXMatrixRotationX(&rXMat_, D3DXToRadian(-90));
	D3DXMatrixRotationY(&rYMat_, D3DXToRadian(Roopitr->rotateY));
	cardMat_[hMat] = rXMat_ * rYMat_ * tMat_;
}

void Npc::CreateMatrixVertical(std::vector<Hand>::iterator &Roopitr, int hMat)
{
	D3DXMatrixTranslation(&tMat_, (Roopitr->xPosition + (hMat * CARD_INTERVAL)), Roopitr->yPosition + 0.05f, CARD_POSITION_Z_NPC);
	D3DXMatrixRotationX(&rXMat_, D3DXToRadian(-90));
	D3DXMatrixRotationY(&rYMat_, D3DXToRadian(Roopitr->rotateY));
	cardMat_[hMat] = rXMat_ * rYMat_ * tMat_;
}

void Npc::Release()
{
	SAFE_DELETE(pAI_);
}

void Npc::SelectChip()
{
	////ランダムで数字を取得する
	//int num = (rand() % 15) + 1;

	////渡された数値に対応した金額を賭ける
	//if (num <= 0)
	//{
	//	return;
	//}
	//else if (num <= BET_FIVE)
	//{
	//	bet_ = FIVE_CHIP;
	//}
	//else if (num <= BET_TWENTY_FIVE)
	//{
	//	bet_ = TWENTY_FIVE_CHIP;
	//}
	//else if (num <= BET_FIFTY)
	//{
	//	bet_ = FIFTY_CHIP;
	//}
	//else
	//{
	//	bet_ = ONE_HUNDRED_CHIP;
	//}

	//賭けられるかチェック
	//CanBet(bet_);
	CanBet(pAI_->Howmuchbet(role_, bluf_, maxBet_, chip_, totalBet_));
}

void Npc::TurnNpcCard()
{
	for (auto Roopitr = NPCCard_.begin(); Roopitr != NPCCard_.end(); ++Roopitr)
	{
		if (0 < Roopitr->rotateY)
		{
			Roopitr->rotateY -= rotateSpeed;
		}

		else
		{
			PlayScene::gamestatus_ = STOP_GAME;
			Player::turn_ = HUMAN_TURN;
		}
	}
}

void Npc::NPCCardStateReset()
{
	for (auto Roopitr = NPCCard_.begin(); Roopitr != NPCCard_.end(); ++Roopitr)
	{
		if (Roopitr->status != STOP)
		{
			allStopFlg_ = false;
			break;
		}
		allStopFlg_ = true;
	}
}

void Npc::CardSetPosition()
{
	int hCard = 0;

	//各カードのポジションの設定
	for (auto Roopitr = NPCCard_.begin(); Roopitr != NPCCard_.end(); ++Roopitr)
	{
		switch (Roopitr->status)
		{
		case UNCHECK:
		case hairu:
			MoveUncheck(Roopitr);
			break;
		case CHECK:
			MoveCheck(Roopitr);
			break;
		case MOVE_OUT:
			MoveOut(Roopitr, hCard);
			break;
		case MOVE_IN:
			MoveIn(Roopitr);
			break;
		}
		hCard++;
	}
}

void Npc::MoveIn(std::vector<Hand>::iterator &Roopitr)
{
	if (Roopitr->xPosition >= -CARD_POSITION_X)
	{
		Roopitr->xPosition -= MOVE_CARD;
		if (Roopitr->xPosition <= -CARD_POSITION_X)
		{
			Roopitr->status = hairu;
		}
	}
}

void Npc::MoveOut(std::vector<Hand>::iterator &Roopitr, int hCard)
{
	if (Roopitr->xPosition < REVERSE_CARD)
	{
		Roopitr->xPosition += MOVE_CARD;
		if (Roopitr->xPosition >= REVERSE_CARD)
		{
			//ここでカードを交換する

			//カードのモデルを呼びなおす
			Player::ChangeCard(Roopitr);
			Roopitr->status = MOVE_IN;
		}
	}
}

void Npc::MoveCheck(std::vector<Hand>::iterator &Roopitr)
{
	Roopitr->yPosition += MOVE_CARD;
	if (Roopitr->yPosition >= UNSELECT_CARD)
	{
		Roopitr->yPosition = UNSELECT_CARD;
	}
}

void Npc::MoveUncheck(std::vector<Hand>::iterator &Roopitr)
{
	//カードを選択状態じゃなかったら
	if (Roopitr->yPosition > SELECT_CARD)
	{
		Roopitr->yPosition -= MOVE_CARD;
		if (Roopitr->yPosition <= SELECT_CARD)
		{
			Roopitr->status = STOP;
		}
	}
}

void Npc::ChangeCard()
{
	//カード交換開始
	for (auto Roopitr = NPCCard_.begin(); Roopitr != NPCCard_.end(); ++Roopitr)
	{
		if (Roopitr->yPosition >= UNSELECT_CARD)
		{
			if (Roopitr->status == CHECK)
			{
				Roopitr->status = MOVE_OUT;
			}
		}
	}
}

void Npc::ChangeCardSelect()
{


	//カード交換するかどうかのフラグ
	//こいつを使ってそれぞれのカードのステータスをキメル（CHECKかSTOPか）
	bool changeFlg[HAND_MAX];
	//初期化
	for (int i = 0; i < HAND_MAX; i++)
	{
		changeFlg[i] = true;
	}
	//ここでAIthinkを実行？
	pAI_->Thinking(NPCCard_, changeFlg);

	int cnum = 0;
	for (auto RoopItr = NPCCard_.begin(); RoopItr != NPCCard_.end(); RoopItr++)
	{
		if (changeFlg[cnum])
		{
			RoopItr->status = CHECK;
		}
		cnum++;
	}
	tradeFlg_ = false;
}


std::vector<Hand> Npc::GetHand()
{
	return NPCCard_;
}