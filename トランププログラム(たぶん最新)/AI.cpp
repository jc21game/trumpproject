#include "AI.h"
#include "Engine/Model.h"


//int AI::turn_ = HUMAN_TURN;

AI::AI(IGameObject* parent) :
	IGameObject(parent, "AI"),
	isDuplication_(false), allStopFlg_(true)
{
}

AI::~AI()
{
}

void AI::Initialize()
{
}

void AI::Update()
{
}

void AI::Draw()
{
}

void AI::Release()
{
}





bool AI::CheckPair(std::vector<Hand> hand)
{
	//hand.begin() = HumanCard_.begin();
	for (auto baseItr = hand.begin(); baseItr != hand.end(); baseItr++)
	{
		for (auto trgItr = baseItr + 1; trgItr != hand.end(); trgItr++)
		{
			//カードの数値が一緒
			if (baseItr->num == trgItr->num)
			{
				return true;
			}
		}
	}
	return false;
}

bool AI::CheckMark(std::vector<Hand> hand, int remaining)
{
	//int sameMarkCount = 0;
	////hand.begin() = HumanCard_.begin();
	//for (auto baseItr = hand.begin()/*hand.begin()*/; baseItr != hand.end()/*hand.end()*/; baseItr++)
	//{
	//	for (auto trgItr = baseItr + 1; trgItr != hand.end(); trgItr++)
	//	{
	//		//カードのマークが一緒
	//		if (baseItr->mark == trgItr->mark)
	//		{
	//			sameMarkCount++;
	//		}
	//	}
	//}

	////同一マークが3枚以上
	//if (sameMarkCount >= THREE_ONE_ONE)
	//{
	//	return true;
	//}
	//else
	//{
	//	return false;
	//}

	//各マークがいくつあるかを保存する配列
	int mark[4];
	//初期化
	for (int i = 0; i < 4; i++)
	{
		mark[i] = 0;
	}

	//配列に登録
	for (auto baseItr = hand.begin(); baseItr < hand.end(); baseItr++)
	{
		//int suit = baseItr->mark;
		mark[baseItr->mark]++;
	}

	//登録されたマークの数を確認して
	//同一マークが3つ以上ならtrue
	for (int i = 0; i < 4; i++)
	{
		if (mark[i] >= remaining)
		{
			return true;
		}
	}
	return false;
}

bool AI::CheckStraight(std::vector<Hand> hand, int remaining)
{
	/*int sum = 0;
	hand.begin() = HumanCard_.begin();
	for (auto baseItr = hand.begin(); baseItr != hand.end(); baseItr++)
	{
		for (auto trgItr = baseItr + 1; trgItr != hand.end(); trgItr++)
		{
			sum += abs(baseItr->num - trgItr->num);
			if (sum > 20)
			{
				return false;
			}
		}
	}*/
	//return true;

	//各数値がいくつあるかを保存する配列
	int var[14];
	//初期化
	for (int i = 0; i < 14; i++)
	{
		var[i] = 0;
	}

	//配列に登録
	for (auto baseItr = hand.begin(); baseItr < hand.end(); baseItr++)
	{
		//int num = baseItr->num;
		var[baseItr->num]++;
		if (baseItr->num == 0)
		{
			var[13]++;
		}
	}

	//探索本体
	//登録した配列を0から順に探索する
	for (int i = 0; i < 10; i++)
	{
		//登録されているか
		if (var[i])
		{
			//解除フラグ
			int breakFlg = 5;
			//iの次の数値から4枚確認
			for (int a = i + 1; a < i + 5; a++)
			{
				//登録されていない
				if (!var[a])
				{
					//解除フラグ++
					breakFlg--;
				}
				//breakFlg: ストレートを狙うときに何枚そろってないか
				if (breakFlg < remaining)
				{
					breakFlg = 5;
					break;
				}
				//4枚探索成功した
				if (a == (i + 4))
				{
					return true;
				}
			}
		}
	}
	//連番が弱かった
	return false;
}

void AI::ChangeRand(std::vector<Hand> hand, bool(&changeFlg)[5])
{

	//テス
	//std::cout << "ランダムで交換" << std::endl;

	//bool cardChangeFlg[5];

	//交換するかどうかを判断する(ランダムで)
	srand((unsigned int)time(NULL));
	for (int i = 0; i < 5; i++)
	{
		//cardChangeFlg[i] = false;
		//50ぱーで保持
		if (rand() % 2)
		{
			//cardChangeFlg[i] = true;
			changeFlg[i] = false;
		}
	}

	//Humanitr_ = HumanCard_.begin();

	////交換した枚数をカウントする
	//int changeCount = 0;
	//
	////テス
	//int cardNum = 0;

	//for (auto baseItr = hand.begin(); baseItr != hand.end(); baseItr++)
	//{
	//	if (cardChangeFlg[cardNum] == true)
	//	{
	//		changeFlg[cardNum] = true;
	//	}
	//}



	/*for (int i = 0; i < 5; i++)
	{
		if (cardChangeFlg[i] == true)
		{
			Humanitr_->mark = shuffleitr_->mark;
			Humanitr_->num = shuffleitr_->num;
			++shuffleitr_;
			++changeCount;
		}
		++Humanitr_;
	}
	shufflehand_.erase(shufflehand_.begin(), shufflehand_.begin() + changeCount);

	std::cout << "\n" << "交換後(プレイヤーのみ)" << std::endl;
	std::cout << "交換枚数" << changeCount << "枚" << "\n" << std::endl;*/
}

void AI::ChangePair(std::vector<Hand> hand, bool (&changeFlg)[5])
{

	//テス
	//std::cout << "ペア残しで交換" << std::endl;


	/*bool cardChangeFlg[5];
	for (int i = 0; i < 5; i++)
	{
		cardChangeFlg[i] = true;
	}*/


	//Humanitr_ = HumanCard_.begin();
	int baseLockCount = 0;
	for (auto baseItr = hand.begin(); baseItr != hand.end(); baseItr++)
	{
		int targetLockCount = baseLockCount + 1;
		for (auto trgItr = baseItr + 1; trgItr != hand.end(); trgItr++)
		{
			//カードの数値が一緒
			if (baseItr->num == trgItr->num)
			{
				/*cardChangeFlg[baseLockCount] = false;
				cardChangeFlg[targetLockCount] = false;*/
				changeFlg[baseLockCount] = false;
				changeFlg[targetLockCount] = false;
			}
			++targetLockCount;
		}
		++baseLockCount;
	}



	//Humanitr_ = HumanCard_.begin();

	//交換した枚数をカウントする
	/*int changeCount = 0;

	for (int i = 0; i < 5; i++)
	{
		if (cardChangeFlg[i] == true)
		{
			Humanitr_->mark = shuffleitr_->mark;
			Humanitr_->num = shuffleitr_->num;
			++shuffleitr_;
			++changeCount;
		}
		++Humanitr_;
	}
	shufflehand_.erase(shufflehand_.begin(), shufflehand_.begin() + changeCount);

	std::cout << "\n" << "交換後(プレイヤーのみ)" << std::endl;
	std::cout << "交換枚数" << changeCount << "枚" << "\n" << std::endl;*/
}

void AI::ChangeFlash(std::vector<Hand> hand, bool(&changeFlg)[5])
{
	//各マークがいくつあるかを保存する配列
	int mark[4];
	//初期化
	for (int i = 0; i < 4; i++)
	{
		mark[i] = 0;
	}

	//配列に登録
	for (auto baseItr = hand.begin(); baseItr < hand.end(); baseItr++)
	{
		//int suit = baseItr->mark;
		mark[baseItr->mark]++;
	}
	//最多マークを保存する領域
	int maxMark = 0;
	//最多マークの個数を保存する領域
	int maxMarkCount = 0;

	//最多を検索
	for (int i = 0; i < 4; i++)
	{
		if (maxMarkCount < mark[i])
		{
			maxMarkCount = mark[i];
			maxMark = i;
		}
	}

	//何番目のカード
	int carNum = 0;
	//changeに登録
	for (auto baseItr = hand.begin(); baseItr < hand.end(); baseItr++)
	{
		if (baseItr->mark == maxMark)
		{
			changeFlg[carNum] = false;
		}
		carNum++;
	}
}

void AI::ChangeStraight(std::vector<Hand> hand, bool(&changeFlg)[5], int remaining)
{

	//各数値がいくつあるかを保存する配列
	int var[14];
	//どの数値を残しておくか
	bool lockNum[14];
	//最もそろっているときのbreakFlgの状態を保持する
	int minBreakFlg = 5;

	//初期化
	for (int i = 0; i < 14; i++)
	{
		var[i] = 0;
		lockNum[i] = false;
	}

	//配列に登録
	for (auto baseItr = hand.begin(); baseItr < hand.end(); baseItr++)
	{
		//int num = baseItr->num;
		var[baseItr->num]++;
		if (baseItr->num == 0)
		{
			var[13]++;
		}
	}

	//探索本体
	//登録した配列を0から順に探索する
	for (int i = 0; i < 10; i++)
	{
		//登録されているか
		if (var[i])
		{
			//解除フラグ:ストレートを狙うときに何枚そろってないか
			int breakFlg = 5;
			//iの次の数値から4枚確認
			for (int a = i + 1; a < i + 5; a++)
			{
				//登録されていない
				if (!var[a])
				{
					//解除フラグ++
					breakFlg--;
				}
				//規定値そろっていなかった
				if (breakFlg < remaining)
				{
					breakFlg = 5;
					break;
				}
				//4枚探索成功した
				if (a == (i + 4))
				{
					//最もそろっていれば登録されている内容をすべてクリアし
					//新たに登録する
					if (breakFlg <= minBreakFlg)
					{
						minBreakFlg = breakFlg;
						for (int b = 0; b < 14; b++)
						{
							lockNum[b] = false;
						}
						//ストレートを狙える数値をLOCKする
						for (int startLock = i; startLock < i + 5; startLock++)
						{
							lockNum[startLock] = true;
							if (lockNum[13])
							{
								lockNum[0] = true;
							}
						}
					}
				}
			}
		}
	}

	//何番目のカードか
	int carNum = 0;
	for (auto baseItr = hand.begin(); baseItr != hand.end(); baseItr++)
	{
		for (int i = 0; i < 13; i++)
		{
			if ((baseItr->num == i) && (lockNum[i]))
			{
				changeFlg[carNum] = false;
				break;
			}
		}
		carNum++;
	}


}

// ペアなし交換でハイカードを一枚残す交換
void AI::ChangeHigh(std::vector<Hand> hand, bool(&changeFlg)[5], int leavenum)
{
	struct hData {
		//数値
		int num = -1;
		//どの手札か
		int cardNum = 0;
	};
	int highCard = 0;
	int lockCardCount = 0;
	int cardCount = 0;
	//大きいものから順に登録するためのもの
	hData data[5];
	//登録
	for (auto baseItr = hand.begin(); baseItr != hand.end(); baseItr++)
	{
		//登録する場所を探す
		//ブラフ対応させるために入れてみた
		if (changeFlg[cardCount])
		{
			for (int i = 0; i < 5; i++)
			{
				//数値が1である
				if (baseItr->num == 0)
				{
					//登録されたデータをずらす処理
					for (int j = 4; j > i; j--)
					{
						data[j] = data[j - 1];
					}
					data[i].num = 13;
					data[i].cardNum = cardCount;
					break;
				}
				//登録されていない
				else if (data[i].num == -1)
				{
					data[i].num = baseItr->num;
					data[i].cardNum = cardCount;
					break;
				}
				//登録されている値よりも大きい
				else if (data[i].num < baseItr->num)
				{
					//登録されたデータをずらす処理
					for (int j = 4; j > i; j--)
					{
						data[j] = data[j - 1];
					}
					data[i].num = baseItr->num;
					data[i].cardNum = cardCount;
					break;
				}
			}
		}

		cardCount++;
	}

	for (int i = 0; i < leavenum; i++)
	{
		changeFlg[data[i].cardNum] = false;
	}
}

void AI::ChangePairBluff(std::vector<Hand> hand, bool(&changeFlg)[5])
{
	enum Pair {
		NONE_0,
		ONE_PAIR,
		TWO_PAIR,
		THREE_CARD,
		FULLHOUSE,
		NONE_5,
		FOUR_CARD
	};

	int pair = NONE_0;
	int baseLockCount = 0;
	for (auto baseItr = hand.begin(); baseItr != hand.end(); baseItr++)
	{
		int targetLockCount = baseLockCount + 1;
		for (auto trgItr = baseItr + 1; trgItr != hand.end(); trgItr++)
		{
			//カードの数値が一緒
			if (baseItr->num == trgItr->num)
			{
				changeFlg[baseLockCount] = false;
				changeFlg[targetLockCount] = false;
				pair++;
			}
			++targetLockCount;
		}
		++baseLockCount;
	}
	switch (pair)
	{
	case ONE_PAIR:
		ChangeHigh(hand, changeFlg, 2);
		break;
	case THREE_CARD:
		ChangeHigh(hand, changeFlg, 1);
		break;
	}
}

int AI::Detailbet(int betdata, int maxbet, int chip, int totalbet)
{
	//ベット額が所持金以下
	if (betdata <= chip)
	{
		//bet額とtotalbet額の和がmaxbet以下
		if (totalbet + betdata <= maxbet)
		{
			//指定されたbet額を返す
			return betdata;
		}
	}
	//bet額の更新(減少)
	int nextBetData = betdata - 5;

	return Detailbet(nextBetData, maxbet, chip, totalbet);
}
