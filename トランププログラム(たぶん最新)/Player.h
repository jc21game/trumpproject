#include "PlayScene.h"
#include <iostream>
#include <vector>
#include <time.h>
#include "Engine/Model.h"
#include "Image.h"

#pragma once
//手札配列の最大値
const int HAND_MAX = 5;
const int CARD_MARK_MAX = 4;
const int CARD_NUMBER_MAX = 13;
const int CARD_SUM = 52;

//初期の所持金
const int INITIAL_CHIP = 300;

//チップ達
const int FIVE_CHIP = 5;
const int TWENTY_FIVE_CHIP = 25;
const int FIFTY_CHIP = 50;
const int ONE_HUNDRED_CHIP = 100;

//カード移動用
const float REVERSE_CARD = 1.5f;
const float UNSELECT_CARD = 1.0f;
const float SELECT_CARD = 0.7f;
const float BACK_CARD = 180.0f;
const float MOVE_CARD = 0.05f;
const float CARD_POSITION_X = 0.6f;
const float CARD_POSITION_Y = 0.7f;
const float CARD_INTERVAL = 0.3f;

//カード回転用
const int ROTATION_ANGLE = -90;

//Handの初期値
const float INIT_ROTATE_ANGLE = 180.0f;
const float INIT_POSITION_X = -0.6f;
const float ININ_POSITION_Y = 1.5f;
const std::string INIT_MODELNAME = "";

enum TURN
{
	HUMAN_TURN,
	NPC_TURN
};

class Player : public IGameObject
{
protected:

	//マーク
	std::string name[CARD_MARK_MAX] =
	{
		"Club",
		"Diamond",
		"Spade",
		"Heart"
	};

	//カードのナンバー
	std::string number[CARD_NUMBER_MAX] =
	{
		"1","2","3","4","5","6","7","8","9","10","11","12","13"
	};

	bool isDuplication_;

	//テス
	//手札のモデル番号登録用
	int hModel_[HAND_MAX];

	//何番目の手札を指しているのかを保持する
	int handnum_;

	//所持金
	int chip_;

	//ベット金額
	int isBet_;

	//１回１回のベット金額
	int bet_;

	//１勝負の全ベット金額
	int totalBet_;

	//ベットする金額の上限
	int maxBet_;

	//どっちのターンか
	static int turn_;
	//全てのカードがSTOPか
	bool allStopFlg_;

	//降りるかどうか
	bool isFold_;

	//継承先でプレイシーンの値をコピーして使う
	//シャッフル後の山札
	std::vector<Hand> shuffleCardSum_;
	//シャッフル後の山札のイテレータ
	std::vector<Hand>::iterator shuffleitr_;

public:
	Player(IGameObject * parent);
	~Player();
	//初期化
	virtual void Initialize() override;

	//更新
	virtual void Update() override;

	//描画
	virtual void Draw() override;

	//開放
	virtual void Release() override;

	//賭けられるか確認
	virtual void CanBet(int chip);

	//賭ける
	void Bet(int chip);

	std::vector<Hand>::iterator ChangeCard(std::vector<Hand>::iterator changecard);

	void ReturnCard(std::vector<Hand> reversehand);

	//所持金情報を渡す
	int GetChip() { return chip_; }

	//ベットしたかどうかを伝える
	bool GetIsBet() { return isBet_; }

	//自分がベットした合計金額を渡す
	int GetTotalBet() { return totalBet_; }

	//降りるかどうかを伝える
	bool GetIsFold() { return isFold_; }

	//勝った時に所持金が増える
	void SetChip(int chip) { chip_ += chip; }

	//ベットしていない状態にしてもらう
	void SetIsBet(bool isBet) { isBet_ = isBet; }

	//１回１回のベット金額をクリアする
	void SetBet(int bet) { bet_ = bet; }

	//降りた状態にする
	void SetIsFold(int isFold) { isFold_ = isFold; }

	//ベットした金額を0にする
	void SetTotalBet(int totalBet);

	//ベット金額の上限を設定する
	void SetMaxBet(int maxBet) { maxBet_ = maxBet; }

	//初期化。Initializeとは別の用途
	virtual void InitializeGame();
};