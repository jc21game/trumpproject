#pragma once
#include "IGameObject.h"

//シーン番号を管理するenum
enum SCENE_ID
{
	SCENE_ID_SPLASH,
	SCENE_ID_TITLE,
	SCENE_ID_SELECT_DIFFICULT,
	SCENE_ID_PLAY
};

//シーンを管理するクラス
class SceneManager : public IGameObject
{
	//今と後のシーン。この2つが違うとnextSceneIDのシーンに切り替わる
	static SCENE_ID currentSceneID_;	//今のシーン
	static SCENE_ID nextSceneID_;		//次のシーン

	static IGameObject* pCurrentScene_;

public:
	//コンストラクタ
	SceneManager(IGameObject* parent);

	//デストラクタ
	~SceneManager();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;

	static void ChangeScene(SCENE_ID next);

	static IGameObject* GetCurrentScene()
	{
		return pCurrentScene_;
	}
};