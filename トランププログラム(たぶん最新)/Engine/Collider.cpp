#include "Collider.h"


//コンストラクタでコライダー情報を入力する
//第1引数 ... オブジェクトの中心
//第2引数 ... オブジェクトの半径
//第3引数 ... オブジェクトのオーナー
Collider::Collider(D3DXVECTOR3 center, float radius,IGameObject* owner)
{
	owner_  = owner;
	center_ = center;
	radius_ = radius;

}


Collider::~Collider()
{
}

//当たったかどうかの判定を行う
//第1引数 ... 当たったオブジェクト
bool Collider::IsHit(Collider& target)
{
	D3DXVECTOR3 v = ( center_ + owner_->Getposition()) -
		(target.center_ + target.owner_->Getposition());

	float length = D3DXVec3Length(&v);	//ベクトルを距離に変換

	//当たってるかの判定
	//敵と自分の距離が敵と自分の半径を足した値より小さかったら当たってる
	if ( length <= radius_ + target.radius_ )
	{
		return true;
	}

	return false;
}
