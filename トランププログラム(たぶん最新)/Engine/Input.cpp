#include "Input.h"
#include <assert.h>
namespace Input
{
	//ここで行う変数宣言は他クラスでは使わない変数のみ
	LPDIRECTINPUT8   pDInput = nullptr;			//LPDIRECTINPUT8型の変数
	LPDIRECTINPUTDEVICE8 pKeyDevice = nullptr;
	BYTE keyState[256] = { 0 };					//BYTE型 ... 1バイト分の変数のこと
	BYTE prevKeyState[256];						//前フレームでの各キーの状態

	//マウス
	LPDIRECTINPUTDEVICE8 pMouseDevice; //デバイスオブジェクト
	DIMOUSESTATE mouseState;    //マウスの状態
	DIMOUSESTATE prevMouseState;   //前フレームのマウスの状態

	HWND hWnd_;

	//初期化(引数 : ウィンドウハンドル)
	void Initialize(HWND hWnd)
	{
		//DirectInput8を作成
		DirectInput8Create(
			GetModuleHandle(nullptr),
			DIRECTINPUT_VERSION,
			IID_IDirectInput8,
			(VOID**)&pDInput,
			nullptr
		);
		assert(pDInput != nullptr);

		//デバイスオブジェクトを作成
		pDInput->CreateDevice(GUID_SysKeyboard, &pKeyDevice, nullptr);

		//デバイスの種類(今回はキーボード)を指定
		pKeyDevice->SetDataFormat(&c_dfDIKeyboard);

		//ほかの実行中のアプリに対する優先度の設定
		pKeyDevice->SetCooperativeLevel(hWnd, DISCL_NONEXCLUSIVE | DISCL_BACKGROUND);
		assert(pKeyDevice != nullptr);

		//マウス
		pDInput->CreateDevice(GUID_SysMouse, &pMouseDevice, nullptr);
		assert(pKeyDevice);
		pMouseDevice->SetDataFormat(&c_dfDIMouse);
		pMouseDevice->SetCooperativeLevel(hWnd, DISCL_NONEXCLUSIVE | DISCL_BACKGROUND);

		hWnd_ = hWnd;
	}

	//更新
	void Update()
	{
		//前回押したキーを覚えておこう
		memcpy(prevKeyState, keyState, sizeof(keyState));

		//Acquire ... なんかキーボードを見失った時にもっかい探してくれる関数
		pKeyDevice->Acquire();

		//その瞬間のキーを入れる
		pKeyDevice->GetDeviceState(sizeof(keyState), &keyState);

		//マウス
		memcpy(&prevMouseState, &mouseState, sizeof(mouseState));
		pMouseDevice->Acquire();
		pMouseDevice->GetDeviceState(sizeof(mouseState), &mouseState);
	}

	//任意のキーが押されているか
	//この方法では、キーを押したつもりが連打になってしまう
	bool IsKey(int keyCode)
	{
		//左辺の8ビット目の中身を見るときはこれをする
		if (keyState[keyCode] & 0x80)
		{
			return true;
		}
		return false;
	}

	//キーを押した瞬間を取得
	bool IsKeyDown(int keyCode)
	{
		//今は押してて、前回は押してない
		if (IsKey(keyCode) && !((prevKeyState[keyCode] & 0x80)))
		{
			return true;	
		}
		return false;
	}

	//キーを放した瞬間を取得
	bool IsKeyUp(int keyCode)
	{
		//さっきは押してて、今は押してない状態
		if (!(IsKey(keyCode)) && (prevKeyState[keyCode] & 0x80))
		{
			return true;
		}
		return false;
	}

	//開放
	void Release()
	{
		//宣言した順とは逆になることを忘れずに
		SAFE_RELEASE(pMouseDevice);
		SAFE_RELEASE(pKeyDevice);
		SAFE_RELEASE(pDInput);
	}

	///////////////////////////// マウス情報取得 //////////////////////////////////

//マウスのボタンが押されているか調べる
	bool IsMouseButton(int buttonCode)
	{
		//押してる
		if (mouseState.rgbButtons[buttonCode] & 0x80)
		{
			return true;
		}
		return false;
	}

	//マウスのボタンを今押したか調べる（押しっぱなしは無効）
	bool IsMouseButtonDown(int buttonCode)
	{
		//今は押してて、前回は押してない
		if (IsMouseButton(buttonCode) && !(prevMouseState.rgbButtons[buttonCode] & 0x80))
		{
			return true;
		}
		return false;
	}

	//マウスのボタンを今放したか調べる
	bool IsMouseButtonUp(int buttonCode)
	{
		//今押してなくて、前回は押してる
		if (!IsMouseButton(buttonCode) && prevMouseState.rgbButtons[buttonCode] & 0x80)
		{
			return true;
		}
		return false;
	}

	//マウスカーソルの位置を取得
	D3DXVECTOR3 GetMousePosition()
	{
		POINT mousePos;

		//こいつはディスプレイそのものの左上から数える
		//そのため、その情報をスクリーン座標に変換する必要がある
		GetCursorPos(&mousePos);
		ScreenToClient(hWnd_, &mousePos);

		D3DXVECTOR3 result = D3DXVECTOR3((float)mousePos.x, (float)mousePos.y, 0.0);
		return result;
	}


	//そのフレームでのマウスの移動量を取得
	D3DXVECTOR3 GetMouseMove()
	{
		D3DXVECTOR3 result((float)mouseState.lX, (float)mouseState.lY, (float)mouseState.lZ);
		return result;
	}

}