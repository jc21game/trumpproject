#include "Sprite.h"
#include "Direct3D.h"

Sprite::Sprite() :
	pSprite_(nullptr), pTexture_(nullptr)
{
}


Sprite::~Sprite()
{
	SAFE_RELEASE(pTexture_);
	SAFE_RELEASE(pSprite_);
}

void Sprite::Load(const char* fileName)
{
	//スプライト作成
	D3DXCreateSprite(Direct3D::pDevice, &pSprite_);
	assert(pSprite_ != nullptr);

	//テクスチャ作成
	D3DXCreateTextureFromFileEx(Direct3D::pDevice, fileName,
		0, 0, 0, 0, D3DFMT_UNKNOWN, D3DPOOL_DEFAULT, D3DX_FILTER_NONE,
		D3DX_DEFAULT, 0, 0, 0, &pTexture_);
	assert(pTexture_ != nullptr);
}

void Sprite::Draw(const D3DXMATRIX& matrix)
{
	//行列をセット
	pSprite_->SetTransform(&matrix);

	//ゲーム画面の描画
	pSprite_->Begin(D3DXSPRITE_ALPHABLEND);
	pSprite_->Draw(pTexture_, nullptr, nullptr, nullptr, D3DXCOLOR(1, 1, 1, 1));
	pSprite_->End();

}

void Sprite::Draw(const D3DXMATRIX& matrix, const D3DXVECTOR3 position, float alpha)			//参照渡し
{
	//行列をセット
	pSprite_->SetTransform(&matrix);

	//ゲーム画面の描画
	pSprite_->Begin(0);
	pSprite_->Draw(pTexture_, nullptr, nullptr, &position/*nullptr*/, D3DXCOLOR(1, 1, 1, alpha));
	pSprite_->End();
}