#include "Fbx.h"
#include "FbxParts.h"
#include "Global.h"
#include "Direct3D.h"

//コンストラクタ
Fbx::Fbx():
	_animSpeed(0)
{

}

//デストラクタ
Fbx::~Fbx()
{
	//各パーツの開放
	for (u_int i = 0; i < parts.size(); i++)
	{
		SAFE_DELETE(parts[i]);
	}
	parts.clear();

	//テクスチャの開放
	for (auto it = _pTexture.begin(); it != _pTexture.end(); it++)
	{
		SAFE_RELEASE(it->second);
	}
	_pTexture.clear();

	//大元の開放
	_pScene->Destroy();
	_pManager->Destroy();
}


//FBXファイル読み込み
HRESULT Fbx::Load(std::string fileName)
{
	//ロードに必要なやつらを準備
	_pManager = FbxManager::Create();				//一番えらいやつ
	_pImporter = FbxImporter::Create(_pManager, "");//ファイルを読み込むやつ
	_pScene = FbxScene::Create(_pManager, "");		//読み込んだファイルを管理するやつ

	//インポーターを使ってファイルロード
	if (_pImporter->Initialize(fileName.c_str()) == false)
	{
		return E_FAIL;
	}
	_pImporter->Import(_pScene);

	//インポーターはお役御免
	_pImporter->Destroy();

	// アニメーションのタイムモードの取得
	_frameRate = _pScene->GetGlobalSettings().GetTimeMode();


	//現在のカレントディレクトリを覚えておく
	char defaultCurrentDir[MAX_PATH];
	GetCurrentDirectory(MAX_PATH, defaultCurrentDir);

	//カレントディレクトリをファイルがあった場所に変更
	char dir[MAX_PATH];
	_splitpath_s(fileName.c_str(), nullptr, 0, dir, MAX_PATH, nullptr, 0, nullptr, 0);
	SetCurrentDirectory(dir);


	//ルートノードを取得して
	FbxNode* rootNode = _pScene->GetRootNode();

	//そいつの子供の数を調べて
	int childCount = rootNode->GetChildCount();

	//1個ずつチェック
	for (int i = 0; childCount > i; i++)
	{
		CheckNode(rootNode->GetChild(i), &parts);
	}


	//カレントディレクトリを元の位置に戻す
	SetCurrentDirectory(defaultCurrentDir);

	return S_OK;
}

//ノードの中身を調べる
void Fbx::CheckNode(FbxNode* pNode, std::vector<FbxParts*> *pPartsList)
{
	//そのノードにはメッシュ情報が入っているだろうか？
	FbxNodeAttribute* attr = pNode->GetNodeAttribute();
	if (attr != nullptr && attr->GetAttributeType() == FbxNodeAttribute::eMesh)
	{
		//パーツを用意
		FbxParts* pParts = new FbxParts;

		//マテリアル情報を準備
		pParts->InitMaterial(pNode, this);

		//頂点情報
		pParts->InitVertexBuffer(pNode->GetMesh());

		//インデックス情報
		pParts->InitIndexBuffer(pNode->GetMesh());

		//アニメーションに関する情報を取得
		pParts->InitAnimation(pNode->GetMesh());

		//パーツ情報を動的配列に追加
		pPartsList->push_back(pParts);
	}

	//子ノードにもデータがあるかも！！
	{
		//子供の数を調べて
		int childCount = pNode->GetChildCount();

		//一人ずつチェック
		for (int i = 0; i < childCount; i++)
		{
			CheckNode(pNode->GetChild(i), pPartsList);
		}
	}
}


//描画
void Fbx::Draw(D3DXMATRIX matrix, int frame)
{
	//パーツを1個ずつ描画
	for (u_int k = 0; k < parts.size(); k++)
	{
		// その瞬間の自分の姿勢行列を得る
		FbxTime     time;
		time.SetTime(0, 0, 0, frame, 0, 0, _frameRate);


		//スキンアニメーション（ボーン有り）の場合
		if (parts[k]->GetSkinInfo() != nullptr)
		{
			parts[k]->DrawSkinAnime(matrix, time);
		}

		//メッシュアニメーションの場合
		else
		{
			parts[k]->DrawMeshAnime(matrix, time, _pScene);
		}
	}
}


//アニメーションの最初と最後のフレームを指定
void Fbx::SetAnimFrame(int startFrame, int endFrame, float speed)
{
	_startFrame = startFrame;
//	_frame = startFrame;
	_endFrame = endFrame;
	_animSpeed = speed;
}


//テクスチャのロード
LPDIRECT3DTEXTURE9 Fbx::LoadTexture(std::string fileName)
{
	//ロード済みテクスチャか
	auto it = _pTexture.find(fileName);

	//まだロードしてない
	if (it == _pTexture.end())
	{
		//ファイル名+拡張だけにする
		char name[_MAX_FNAME];	//ファイル名
		char ext[_MAX_EXT];		//拡張子
		_splitpath_s(fileName.c_str(), nullptr, 0, nullptr, 0, name, _MAX_FNAME, ext, _MAX_EXT);
		wsprintf(name, "%s%s", name, ext);

		//画像ファイルを読み込んでテクスチャ作成
		LPDIRECT3DTEXTURE9 pTexture;
		HRESULT hr = D3DXCreateTextureFromFileEx(Direct3D::pDevice, name, 0, 0, 0, 0, D3DFMT_UNKNOWN, D3DPOOL_DEFAULT,
			D3DX_FILTER_NONE, D3DX_DEFAULT, 0, nullptr, nullptr, &pTexture);

		if (FAILED(hr))
		{
			std::string message = "テクスチャ「";
			message += name;
			message += "」が開けません。\nFBXファイルと同じ場所にありますか？";

			MessageBox(NULL, message.c_str(), "BaseProjDx9エラー", MB_OK);
		}

		//一覧に追加
		_pTexture.insert(std::make_pair(fileName, pTexture));

		return pTexture;
	}

	//ロード済み
	return it->second;
}


//任意のボーンの位置を取得
D3DXVECTOR3 Fbx::GetBonePosition(std::string boneName)
{
	D3DXVECTOR3 position = D3DXVECTOR3(0, 0, 0);
	for (u_int i = 0; i < parts.size(); i++)
	{
		if (parts[i]->GetBonePosition(boneName, &position))
			break;
	}


	return position;
}


//レイキャスト（レイを飛ばして当たり判定）
void Fbx::RayCast(RayCastData * data)
{
	//すべてのパーツと判定
	for (u_int i = 0; i < parts.size(); i++)
	{
		parts[i]->RayCast(data);
	}
}

//各オブジェクトからテクスチャを指定して貼り付ける場合に使う
void Fbx::SetTexture(std::string textureName)
{
	//ロード済みテクスチャか
	auto it = _pTexture.find(textureName);
	LPDIRECT3DTEXTURE9 pTexture;

	//まだロードしてない
	if (it == _pTexture.end())
	{
		//画像ファイルを読み込んでテクスチャ作成
		HRESULT hr = D3DXCreateTextureFromFileEx(Direct3D::pDevice, textureName.c_str(), 0, 0, 0, 0, D3DFMT_UNKNOWN, D3DPOOL_DEFAULT,
			D3DX_FILTER_NONE, D3DX_DEFAULT, 0, nullptr, nullptr, &pTexture);

		//エラー処理
		if (FAILED(hr))
		{
			std::string message = "テクスチャ「";
			message += textureName;
			message += "」が開けません。\nFBXファイルと同じ場所にありますか？";

			MessageBox(NULL, message.c_str(), "BaseProjDx9エラー", MB_OK);
		}

		//一覧に追加
		_pTexture.insert(std::make_pair(textureName, pTexture));

	}

	//ロード済みのテクスチャなら
	else
	{
		//そのままテクスチャのポインタをセット
		pTexture = it->second;
	}

	//パーツの数だけループ
	for (int i = 0; i < (signed)parts.size(); i++)
	{
		//パーツごとにテクスチャを指定して貼り付ける
		parts[i]->SetTexture(pTexture);
	}
}
