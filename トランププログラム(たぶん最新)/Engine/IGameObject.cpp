#include "IGameObject.h"
#include "Global.h"
#include "Model.h"
#include "Collider.h"

//引数が未入力の場合のコンストラクタ
IGameObject::IGameObject() :
	IGameObject(nullptr, "")	//nullptr , 無名としてコンストラクタに送る
{
}

//引数が親のみの場合のコンストラクタ
IGameObject::IGameObject(IGameObject * parent) :
	IGameObject(parent, "")		//parent , 無名としてコンストラクタに送る
{
}

//基本的にはこのコンストラクタが呼ばれる
IGameObject::IGameObject(IGameObject * parent, const std::string & name) :
	//初期化
	pParent_(parent),
	name_(name),
	position_(D3DXVECTOR3(0, 0, 0)),
	rotate_(D3DXVECTOR3(0, 0, 0)),
	scale_(D3DXVECTOR3(1, 1, 1)),
	dead_(false),
	pCollider_(nullptr)
{

}

IGameObject::~IGameObject()
{
	//開放する際、親をデリートしたら子も死ぬのでここでデリートしちゃだめ
	SAFE_DELETE(pCollider_);
}


//行列を作成するメソッド
void IGameObject::Transform()
{
	//行列を作成
	D3DXMATRIX matp;
	D3DXMATRIX matrx;
	D3DXMATRIX matry;
	D3DXMATRIX matrz;
	D3DXMATRIX mats;

	//単位行列を作成
	//D3DXMatrixIdentity(&mat);	

	//移動行列を作成
	D3DXMatrixTranslation(&matp, position_.x, position_.y, position_.z);

	//回転行列を作成
	D3DXMatrixRotationX(&matrx, D3DXToRadian(rotate_.x));
	D3DXMatrixRotationY(&matry, D3DXToRadian(rotate_.y));
	D3DXMatrixRotationZ(&matrz, D3DXToRadian(rotate_.z));

	//拡大縮小行列を作成
	D3DXMatrixScaling(&mats, scale_.x, scale_.y, scale_.z);

	//行列を組み合わせる
	localMatrix_ =  mats *  matrz * matrx * matry * matp;

	//ワールドマトリックスを計算。
	//親のワールドマトリックスにローカルマトリックスになっている
	//できない...。worldMatrixの中身が不定だからなのでは？
	if(GetParent() == nullptr)
	{
		worldMatrix_ = localMatrix_;
	}

	else
	{
		worldMatrix_ = localMatrix_ * pParent_->worldMatrix_;
	}
}

void IGameObject::UpdateSub()
{
	//とりあえず自分のUpadateを呼ぶ
	Update();

	//Transformを呼ぶがあくまで仮
	Transform();

	if (pCollider_ != nullptr )
	{
		Collision(SceneManager::GetCurrentScene());
	}

	//そのあと、子のUpdateを呼ぶ
	for (auto it = pChildlist_.begin(); it != pChildlist_.end(); it++)
	{
		//イテレータの中の関数を呼ぶ
		(*it)->UpdateSub();
	}

	for (auto it = pChildlist_.begin(); it != pChildlist_.end();)
	{
		if ((*it)->dead_ == true)
		{
			//ReleaseSubにしないと子もまでちゃんと消してくれない
			(*it)->ReleaseSub();
			SAFE_DELETE(*it);
			it = pChildlist_.erase(it);
		}

		else
		{
			it++;
		}
	}
}

void IGameObject::DrawSub()
{
	//とりあえず自分のDrawを呼ぶ
	Draw();

	//そのあと、子のDrawを呼ぶ
	for (auto it = pChildlist_.begin(); it != pChildlist_.end(); it++)
	{
		(*it)->DrawSub();
	}
}

void IGameObject::ReleaseSub()
{
	//とりあえず自分のReleaseを呼ぶ
	Release();

	//そのあと、子のReleaseを呼ぶ
	for (auto it = pChildlist_.begin(); it != pChildlist_.end(); it++)
	{
		(*it)->ReleaseSub();
		SAFE_DELETE(*it);
	}
}

//自滅する関数
void IGameObject::KillMe()
{
	//これから消えるフラグを立てる
	dead_ = true;
}

void IGameObject::Collision(IGameObject * targetobject)
{
	if (targetobject != this &&
		targetobject->pCollider_ != nullptr &&
		pCollider_->IsHit(*targetobject->pCollider_))
	{
		//this->
		OnCollision(targetobject);
	}

	for ( auto it = targetobject->pChildlist_.begin();
		it != targetobject->pChildlist_.end();it++ )
	{
		Collision(*it);
	}
}

//親情報を受け取るゲッター
IGameObject * IGameObject::GetParent()
{
	return pParent_;
}

//positionを受け取るゲッター
D3DXVECTOR3 IGameObject::Getposition()
{
	return position_;
}

//回転情報を受け取るゲッター
D3DXVECTOR3 IGameObject::Getrotate()
{
	return rotate_;
}

//大きさを受け取るゲッター
D3DXVECTOR3 IGameObject::Getscale()
{
	return scale_;
}



void IGameObject::PushBackChild(IGameObject* pObj)
{
	pChildlist_.push_back(pObj);
}

void IGameObject::SetCollider(D3DXVECTOR3 & center, float radius)
{
	//コライダーの情報を送る
	pCollider_ = new Collider(center, radius, this);
}
