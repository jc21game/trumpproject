#define DEBUG 1
#include "SceneManager.h"
#include "../TitleScene.h"
#include "../SelectDifficult.h"
#include "../PlayScene.h"
#include "../SplashScene.h"

SCENE_ID SceneManager::currentSceneID_ = SCENE_ID_SPLASH;
SCENE_ID SceneManager::nextSceneID_ = SCENE_ID_SPLASH;

IGameObject* SceneManager::pCurrentScene_ = nullptr;

//コンストラクタ
SceneManager::SceneManager(IGameObject * parent)
	:IGameObject(parent, "SceneManager")
{

}

//デストラクタ
SceneManager::~SceneManager()
{
}

//初期化
void SceneManager::Initialize()
{
	//TitleSceneを作成
	pCurrentScene_ = CreateGameObject<SplashScene>(this);
	
	//デバック用に作成
//#ifdef DEBUG
//	pCurrentScene_ = CreateGameObject<SelectDifficult>(this);
//#endif
}

//更新
void SceneManager::Update()
{
	//一番先頭の子を消す
	if (currentSceneID_ != nextSceneID_)
	{
		auto itr = pChildlist_.begin();
		(*itr)->ReleaseSub();
		SAFE_DELETE(*itr);

		//pChildlist_をclaerする。
		pChildlist_.clear();

		//nextSceneID_に入っているものを作成
		switch (nextSceneID_)
		{
			case SCENE_ID_SPLASH:
				CreateGameObject<SplashScene>(this);
				break;

			case SCENE_ID_TITLE:
				CreateGameObject<TitleScene>(this);
				break;

			case SCENE_ID_SELECT_DIFFICULT:
				CreateGameObject<SelectDifficult>(this);
				break;

			case SCENE_ID_PLAY:
				CreateGameObject<PlayScene>(this);
				break;
		}

		//シーン切り替えを行う
		currentSceneID_ = nextSceneID_;
	}
}

//描画
void SceneManager::Draw()
{

}

//開放
void SceneManager::Release()
{
}

//シーンを切り替えることを教える関数
void SceneManager::ChangeScene(SCENE_ID next)
{
	nextSceneID_ = next;
}
