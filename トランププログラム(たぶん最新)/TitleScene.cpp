#include "TitleScene.h"
#include "Engine/Camera.h"
#include "Engine/Sprite.h"
#include "Engine/Input.h"
#include "Image.h"

//コンストラクタ
TitleScene::TitleScene(IGameObject * parent) :
	//ここでIGameObjectのコンストラクタを呼んでいる
	IGameObject(parent, "TitleScene")
{

}

TitleScene::~TitleScene()
{

}

//初期化
void TitleScene::Initialize()
 {

	//画像データの名前
	std::string name[PICT_MAX] =
	{
		"TitleLogo",
		"BackGroundSplash",
		"PushEnter",
	};

	//画像データを読み込み
	for (int i = 0; i < PICT_MAX; i++)
	{
		hPict_[i] = Image::Load("Data/" + name[i] + ".png");
	}
}

//更新
void TitleScene::Update()
{
	//スペースキーで難易度選択画面に遷移
	if ( Input::IsKeyDown( DIK_RETURN))
	{
		SceneManager::ChangeScene(SCENE_ID_SELECT_DIFFICULT);
	}
}

//描画
void TitleScene::Draw()
{
	//画像配置
	D3DXMatrixTranslation(&matPic_[TITLE],  -50.0f,  150.0f,  0.0);
	D3DXMatrixTranslation(&matPic_[BACKGROUND], 0.0f, 0.0f, 0.0f);

	//背景表示
	Image::SetMatrix(hPict_[BACKGROUND], matPic_[BACKGROUND]);
	Image::Draw(hPict_[BACKGROUND]);

	//キー入力メッセージ表示
	Image::SetMatrix(hPict_[PUSH_ENTER], worldMatrix_);
	Image::Draw(hPict_[PUSH_ENTER]);

	//タイトル表示
	Image::SetMatrix(hPict_[TITLE], matPic_[TITLE]);
	Image::Draw(hPict_[TITLE]);

}

//開放
void TitleScene::Release()
{

}