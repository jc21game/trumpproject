#include "Player.h"
#include "Engine/Model.h"

int Player::turn_ = HUMAN_TURN;

Player::Player(IGameObject* parent) :
	IGameObject(parent, "Player"),
	isDuplication_(false), allStopFlg_(true), handnum_(0), isBet_(false),chip_(INITIAL_CHIP), bet_(0), totalBet_(0), isFold_(false)
{
}

Player::~Player()
{
}

void Player::Initialize()
{
}

void Player::Update()
{
}

void Player::Draw()
{
}

void Player::Release()
{
}

void Player::CanBet(int chip)
{
	//引数として渡ってきた「賭けたい金額」が自分の現在の所持金以下なら
	if (chip <= chip_)
	{
		//現在の全ベット金額と賭けたい金額が足してベット金額上限を超えるなら
		if (maxBet_ < (totalBet_ + chip))
		{
			//賭けられないから、終了
			return;
		}
		else
		{
			//賭けられるなら、賭ける
			Bet(chip);
		}
	}
	//多かったら
	else
	{
		//賭けられないから、終了
		return;
	}
}

void Player::Bet(int chip)
{
	chip_ -= chip;
	totalBet_ += chip;
}

std::vector<Hand>::iterator Player::ChangeCard(std::vector<Hand>::iterator changecard)
{

	//初期化
	std::vector<Hand>::iterator returncard = changecard;

	//交換するカードをしまう
	((PlayScene*)pParent_)->InCard( changecard );

	//カードを取ってくる
	shuffleitr_ = ((PlayScene*)pParent_)->GetShuffleCardSum_()->begin();
	shuffleitr_->modelname =
		("Data/CardModels/CardTexture" + name[shuffleitr_->mark] + number[shuffleitr_->num] + ".jpg");

	returncard->mark = shuffleitr_->mark;
	returncard->num = shuffleitr_->num;
	returncard->modelname = shuffleitr_->modelname;

	//その分カードを抜く
	((PlayScene*)pParent_)->RemoveCardToChange();

	return returncard;
}

void Player::ReturnCard( std::vector<Hand> reversehand )
{
	for ( auto reversecard = reversehand.begin(); reversecard != reversehand.end(); ++reversecard )
	{
		//交換するカードをしまう
		((PlayScene*)pParent_)->InCard( reversecard );
	}
}

void Player::SetTotalBet(int totalBet)
{
	//0が渡されたら、
	if (totalBet == 0)
	{
		//そのまま代入
		totalBet_ = totalBet;
	}
	//それ以外の数字の時は、
	else
	{
		//足す
		totalBet_ += totalBet;
	}
}

void Player::InitializeGame()
{
}

