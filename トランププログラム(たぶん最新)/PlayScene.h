#pragma once
#include "Engine/global.h"
#include "PlaySceneEnum.h"
#include <vector>

//他クラスの読み込み
class Human;
class Npc;
class Judge;

const int SELECT_DO_BET = 0;		//ベット済み
const int SELECT_DONOT_BET = 1;		//未ベット

const int CHANGE = 10;				//役のUIの勝ち負けのタイプを入れ替える
const int METER_MIX = 5;			//メーターの最大値
const int DIVI_METER = 5;			//メーターの分割数
const int ENDGAMECOUNT = 4;			//ゲームが終わるまでの回数

const int HUMAN_CHIP_POS_Y = 960;	//Humanの所持金の Y の位置（数値を変更してBetの位置にもする）
const int NPC_CHIP_POS_Y = 10;		//NPCの所持金の Y の位置（数値を変更してBetの位置にもする）
const int NPC_CHIP_POS_X = 1790;	//NPCの所持金の X の位置（数値を変更してBetの位置にもする）
const int VALUE_PLACE = 70;			//数字の配置間隔

const int UPPER_LIMIT_METER = 25;	//メーターの上限値
const int LOWER_LIMIT_METER = -1;	//メーターの下限値

const int NOT_HAVE_BETCHIP = 0;		//ベットチップを持ってない状態

class PlayScene : public IGameObject
{
private:
	//ACT_SELECTにて動作を選択するカーソル
	int ActCorsor_;

	//モデル,UI用
	int hPict_[PICT_MAX];		//画像番号
	int rolePict_[ROLE_MAX];	//上がった役のUI
	int actSelectPict_[ACT_SELECT_UI_MAX];	//賭けるか降りるかを決める時のUI
	int betTimeUi_;			//ベット中だよって表示するUI
	int turnPict_;			//ターン数を表示するUI
	int hMeterFrame_;		//メーターの枠の画像番号
	int hModel_;			//テーブル用モデル番号
	int hRoom_;				//背景のモデル番号
	int battlecnt_;			//勝負数はこれで管理

	//判定結果
	int playerHand_;	//プレイヤーの役
	int npcHand_;		//NPCの役
	int result_;		//勝敗
	int witchWin;		//どっちが勝ったか

	//プレイヤーたちの所持金管理
	int chipPict_[CHIP_PICT_MAX];			//数字画像
	int humanChipDigits_[CHIP_DIGITS_MAX];	//ユーザーの所持金の○桁の数字を格納する
	int npcChipDigits_[CHIP_DIGITS_MAX];	//NPCの所持金の○桁の数字を格納する
	int maxBet_;							//ベット上限金額
	int humanBet_;							//Humanのベットした金額の合計を格納
	int humanBetDigits_[CHIP_DIGITS_MAX];	//Humanのベットの○桁の数字を格納する
	int npcBet_;							//Npcのベットした金額の合計を格納
	int npcBetDigits_[CHIP_DIGITS_MAX];		//Npcのベットの○桁の数字を格納する

	//bool型変数
	bool isBetSecondTime_;	//2回目のベットチップ交換時間か
	int betCnt_;			//2回目のベットチップ交換時間か
	bool isDuplication_;	//重複しているかの判定
	bool isSelectBet_;		//賭けることにしたかの状態を抜けたか

	//初期化終了したかどうか
	bool initFinish_;
	bool oneTime_;

	//山札関係
	std::vector<Hand> cardSum_;
	std::vector<Hand>::iterator cardSumItr_;

	std::vector<Hand> shuffleCardSum_;
	std::vector<Hand>::iterator shuffleitr_;

	//他クラスのポインタ
	Human* pHuman_;
	Npc* pNpc_;
	Judge* pJudge_;

	//最終勝敗判定時にボイス鳴らす
	bool resultVoiceFlg_;

	std::string bgmName_[8];
	int hBgm_;
	int hVoice_;

public:

	//ゲームの進行状態
	static int gamestatus_;

	//コンストラクタ
	//引数：parent  親オブジェクト（SceneManager）
	PlayScene(IGameObject* parent);

	//初期化
	void Initialize() override;

	//音源をまとめてロード
	void LoadAudio();

	//カードモデルをロードして初期化
	void CardModelLoad();

	//カードをシャッフル
	void CardShuffle();

	//カードを初期状態に戻す
	void CardReset();

	//モデル、画像のデータロード
	void LoadData();

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;

	//プレイヤーオブジェクトのゲッター
	Human *GetHuman() { return pHuman_; }
	Npc *GetNpc() { return pNpc_; }
	Judge *GetJudge() { return pJudge_; }
	int GetbetCnt() { return betCnt_; }

	//山札のゲッター関係
	std::vector<Hand> *GetShuffleCardSum_() { return &shuffleCardSum_; }
	std::vector<Hand>::iterator GetShuffleitr_() { return shuffleitr_; }

	//ゲームの状態のゲッター
	//int *GetGameStatus() { return &gamestatus_; };

	//所持金の桁を計算する
	void DigitsCalculation(int chip, int* pDigits);

	//両者の上限値を決定する
	void DecisionMaxBet();

	bool GetBetTime() { return isBetSecondTime_; }

	//カードを抜く作業
	void RemoveCardToChange();
	void RemoveCardToInit();
	void InCard(std::vector<Hand>::iterator incard);

	//所持金とベット金額を描画（下記二つ共）
	void NumDraw(int* humanDigits, int* npcDigits, int hYPos, int nYPos);
	void ChipDraw(int* playerDigits, int digits, int chip, const int xPos, const int yPos);

	
};