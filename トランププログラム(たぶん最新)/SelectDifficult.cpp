#include "SelectDifficult.h"
#include "Image.h"
#include "SceneBetweenPassData.h"

const float alphaSpeed = 0.03f;

//コンストラクタ
SelectDifficult::SelectDifficult(IGameObject * parent)
	: IGameObject(parent, "SelectDifficult"), Select_(1)
{
}

//初期化
void SelectDifficult::Initialize()
{
	//画像データの名前
	std::string name[PICT_MAX] =
	{
		"BeginnerSample",
		"BeginnerSampleSelect",
		"NormalSample",
		"NormalSampleSelect",
		"GamblerSample",
		"GamblerSampleSelect",
		"BackGroundSelectDiff",
		"SelectDiff",
	};

	//画像データを読み込み
	for (int i = 0; i < PICT_MAX; i++)
	{
		hPict_[i] = Image::Load("Data/" + name[i] + ".png");
	}
}

//更新
void SelectDifficult::Update()
{
	//プレイ画面に遷移
	if (Input::IsKeyDown(DIK_RETURN))
	{
		SceneBetweenPassData::SetLevelData((AI_LEVEL) Select_);
		SceneManager::ChangeScene(SCENE_ID_PLAY);
	}

	//左矢印を押されたら
	if(Input::IsKeyDown(DIK_LEFT))
	{
		//セレクトの値が 0 じゃない時だけ
		if (Select_ != 0)
		{
			Select_--;
		}
	}

	//右矢印を押されたら
	else if (Input::IsKeyDown(DIK_RIGHT))
	{
		//セレクトの値が 2 じゃない時だけ
		if (Select_ != 2)
		{
			Select_++;
		}
	}

}

//描画
void SelectDifficult::Draw()
{
	//行列作成
	D3DXMatrixTranslation(&mat[BEGINNER], 96.0f, 350.0f, 0.0f);
	D3DXMatrixTranslation(&mat[BEGINNER_SELECT], 96.0f, 350.0f, 0.0f);
	D3DXMatrixTranslation(&mat[NORMAL], 704.0f, 350.0f, 0.0f);
	D3DXMatrixTranslation(&mat[NORMAL_SELECT], 704.0f, 350.0f, 0.0f);
	D3DXMatrixTranslation(&mat[GAMBLER], 1312.0f, 350.0f, 0.0f);
	D3DXMatrixTranslation(&mat[GAMBLER_SELECT], 1312.0f, 350.0f, 0.0f);
	D3DXMatrixTranslation(&mat[BACK_GROUND], 0.0f, 0.0f, 0.0f);

	//背景表示
	Image::SetMatrix(hPict_[BACK_GROUND], mat[BACK_GROUND]);
	Image::Draw(hPict_[BACK_GROUND]);

	//「難易度選択」の表示
	Image::SetMatrix(hPict_[SELECT_DIFF], worldMatrix_);
	Image::Draw(hPict_[SELECT_DIFF]);

	if (Select_ == 0)
	{
		//初心者表示選択中
		Image::SetMatrix(hPict_[BEGINNER_SELECT], mat[BEGINNER_SELECT]);
		Image::Draw(hPict_[BEGINNER_SELECT]);
	}
	else
	{
		//初心者表示
		Image::SetMatrix(hPict_[BEGINNER], mat[BEGINNER]);
		Image::Draw(hPict_[BEGINNER]);
	}

	if (Select_ == 1)
	{
		//中級者表示選択中
		Image::SetMatrix(hPict_[NORMAL_SELECT], mat[NORMAL_SELECT]);
		Image::Draw(hPict_[NORMAL_SELECT]);
	}
	else
	{
		//中級者表示
		Image::SetMatrix(hPict_[NORMAL], mat[NORMAL]);
		Image::Draw(hPict_[NORMAL]);
	}

	if (Select_ == 2)
	{
		//上級者表示選択中
		Image::SetMatrix(hPict_[GAMBLER_SELECT], mat[GAMBLER_SELECT]);
		Image::Draw(hPict_[GAMBLER_SELECT]);
	}
	else
	{
		//上級者表示
		Image::SetMatrix(hPict_[GAMBLER], mat[GAMBLER]);
		Image::Draw(hPict_[GAMBLER]);
	}

}

//開放
void SelectDifficult::Release()
{
}