#include "Judge.h"
#include <iostream>

#define IS_FOURCARD 4
#define IS_THREECARD 3
#define IS_ONE_PAIR 2
#define CARD_NUM_MAX 14
#define CARD_MARK_MAX 4

Judge::Judge(IGameObject* parent) : Player(parent),
	playerPokerHand_(0), npcPokerHand_(0), playerMaxNum_(0), npcMaxNum_(0)
{
}


Judge::~Judge()
{
}

int Judge::handCheck(std::vector<Hand> hand, int who)
{
	//手札数値登録
	int var[CARD_NUM_MAX];
	//手札マーク登録
	int mark[CARD_MARK_MAX];

	int stMax = 0;
	int flMax = 0;

	bool pairFlg = false;
	bool stFlg = false;
	bool flFlg = false;

	int returnResist = 0;

	for (int i = 0; i < CARD_NUM_MAX; i++)
	{
		var[i] = 0;
	}
	for (int i = 0; i < CARD_MARK_MAX; i++)
	{
		mark[i] = 0;
	}
	//配列に手札の数値、マークを登録
	for (auto baseItr = hand.begin(); baseItr != hand.end(); baseItr++)
	{
		var[baseItr->num]++;
		mark[baseItr->mark]++;
	}
	//Aのカードの枚数を幻の14の数値として登録
	var[CARD_NUM_MAX - 1] = var[0];

	//ペアがあるかの確認
	for (int i = 0; i < CARD_NUM_MAX; i++)
	{
		if (var[i] >= 2)
		{
			//ペア系の役を確認させるようにする
			pairFlg = true;
			break;
		}
	}

	//ストレートが成立しているかの確認
	for (int i = 0; i < 10; i++)
	{
		if ((var[i] == 1) && (var[i + 1] == 1) && (var[i + 2] == 1) && (var[i + 3] == 1) && (var[i + 4] == 1))
		{
			//ストレートフラグを成立させる
			stFlg = true;
			//最大値の保存
			stMax = i + 4;
		}
	}

	//フラッシュが成立しているかの確認
	for (int i = 0; i < CARD_MARK_MAX; i++)
	{
		if (mark[i] == 5)
		{
			for (int i = CARD_NUM_MAX - 1; i < 0; i--)
			{
				if (var[i])
				{
					flMax = i;
					break;
				}
			}
			//フラッシュフラグを成立させる
			flFlg = true;
		}
	}


	//ロイヤルストレートフラッシュ登録
	if (((var[0] == 1) && (var[9] == 1) && (var[10] == 1) && (var[11] == 1) && (var[12] == 1))
		&& stFlg && flFlg)
	{
		if (who)
		{
			returnResist = RegistHuman(ROYAL_STRAIGHT_FLASH, CARD_NUM_MAX);
		}
		else
		{
			returnResist = RegistNpc(ROYAL_STRAIGHT_FLASH, CARD_NUM_MAX);
		}
		return returnResist;
	}

	//ストレートフラッシュ登録
	if (stFlg && flFlg)
	{
		if (who)
		{
			returnResist = RegistHuman(STRAIGHT_FLASH, stMax);
		}
		else
		{
			returnResist = RegistNpc(STRAIGHT_FLASH, stMax);
		}
		return returnResist;
	}
	//フラッシュ
	if (flFlg)
	{
		if (who)
		{
			returnResist = RegistHuman(FLASH, flMax);
		}
		else
		{
			returnResist = RegistNpc(FLASH, flMax);
		}
		return returnResist;
	}
	//ストレート
	if (stFlg)
	{
		if (who)
		{
			returnResist = RegistHuman(STRAIGHT, stMax);
		}
		else
		{
			returnResist = RegistNpc(STRAIGHT, stMax);
		}
		return returnResist;;
	}

	//ペア系
	if (pairFlg)
	{
		//仮でペアができているかどうかを確認する
		bool isRegist = false;

		//フォーカード
		isRegist = IsWhatPair(var, who, IS_FOURCARD, FOUR_CARD);
		if (isRegist)
		{
			return FOUR_CARD;
		}
		//フルハウス
		//samecountの使い方がおかしい(動きは問題なし)
		if (!isRegist)
		{
			isRegist = IsFullHouse(var, who);
			if ( isRegist == true )
			{
				return FULLHOUSE;
			}
		}

		//スリーカード
		if (!isRegist)
		{
			isRegist = IsWhatPair(var, who, IS_THREECARD, THREE_CARD);
			if (isRegist == true)
			{
				return THREE_CARD;
			}
		}

		//ツーペア
		if (!isRegist)
		{
			isRegist = IsTwoPair(var, who);
			if (isRegist == true)
			{
				return TWO_PAIR;
			}
		}

		//ワンペア
		if (!isRegist)
		{
			isRegist = IsWhatPair(var, who, IS_ONE_PAIR, ONE_PAIR);
			if (isRegist == true)
			{
				return ONE_PAIR;
			}
		}
		//return ;
	}

	//ノーペア
	int noMax = 0;
	for (int i = CARD_NUM_MAX - 1; i >= 0; i--)
	{
		if (var[i])
		{
			noMax = i;
			break;
		}
	}
	if (who)
	{
		returnResist = RegistHuman(NO_PAIR, noMax);
	}
	else
	{
		returnResist = RegistNpc(NO_PAIR, noMax);
	}

	return returnResist;

}

//void Judge::Regist(int who)
//{
//	//役を登録する(プレイヤー)
//	if (who)
//	{
//		switch (pairPoint_)
//		{
//		case 0:
//			if (rsfFlg_ && flashFlg_)
//			{
//				playerPokerHand_ = ROYAL_STRAIGHT_FLASH;
//			}
//			else if (straightFlg_ && flashFlg_)
//			{
//				playerPokerHand_ = STRAIGHT_FLASH;
//			}
//			else if (flashFlg_)
//			{
//				playerPokerHand_ = FLASH;
//			}
//			else if (straightFlg_)
//			{
//				playerPokerHand_ = STRAIGHT;
//			}
//			else
//			{
//				playerPokerHand_ = NO_PAIR;
//			}
//			break;
//		case 1:
//			playerPokerHand_ = ONE_PAIR;
//			break;
//		case 2:
//			playerPokerHand_ = TWO_PAIR;
//			break;
//		case 3:
//			playerPokerHand_ = THREE_CARD;
//			break;
//		case 4:
//			playerPokerHand_ = FULLHOUSE;
//			break;
//		case 6:
//			playerPokerHand_ = FOUR_CARD;
//			break;
//		default:
//			std::cout << "エラー？" << std::endl;
//			break;
//		}
//		std::cout << "自分の役は" << playerPokerHand_ << std::endl;
//	}
//	//役の登録(npc)
//	else
//	{
//		switch (pairPoint_)
//		{
//		case 0:
//			if (rsfFlg_ && flashFlg_)
//			{
//				npcPokerHand_ = ROYAL_STRAIGHT_FLASH;
//			}
//			else if (straightFlg_ && flashFlg_)
//			{
//				npcPokerHand_ = STRAIGHT_FLASH;
//			}
//			else if (flashFlg_)
//			{
//				npcPokerHand_ = FLASH;
//			}
//			else if (straightFlg_)
//			{
//				npcPokerHand_ = STRAIGHT;
//			}
//			else
//			{
//				npcPokerHand_ = NO_PAIR;
//			}
//			break;
//		case 1:
//			npcPokerHand_ = ONE_PAIR;
//			break;
//		case 2:
//			npcPokerHand_ = TWO_PAIR;
//			break;
//		case 3:
//			npcPokerHand_ = THREE_CARD;
//			break;
//		case 4:
//			npcPokerHand_ = FULLHOUSE;
//			break;
//		case 6:
//			npcPokerHand_ = FOUR_CARD;
//			break;
//		default:
//			std::cout << "エラー？" << std::endl;
//			break;
//		}
//		std::cout << "相手の役は" << npcPokerHand_ << std::endl;
//	}
//}



int Judge::GameJudge(bool isFold)
{
	/*if ((playerPokerHand_ == 0) && (npcPokerHand_ == 0))
	{
		return DROW;
	}*/
	//pHumanが降りていたら
	if (isFold)
	{
		//pHumanの負け
		return LOSE;
	}

	if (playerPokerHand_ < npcPokerHand_)
	{
		return LOSE;
	}
	else if (playerPokerHand_ == npcPokerHand_)
	{
		//（最大の）数値の比較をしてどっちが勝ったかを判定する
		if (playerMaxNum_ < npcMaxNum_)
		{
			return LOSE;
		}
		else if (playerMaxNum_ == npcMaxNum_)
		{
			return DROW;
		}
		else
		{
			return WIN;
		}
	}
	else
	{
		return WIN;
	}


}

int Judge::RegistHuman(int pokerhand, int maxnum)
{
	playerMaxNum_ = maxnum;
	playerPokerHand_ = pokerhand;
	//std::cout << "自分の役は" << playerPokerHand_ << std::endl;
	return playerPokerHand_;
}

int Judge::RegistNpc(int pokerhand, int maxnum)
{
	npcMaxNum_ = maxnum;
	npcPokerHand_ = pokerhand;
	//std::cout << "相手の役は" << npcPokerHand_ << std::endl;
	return npcPokerHand_;
}

bool Judge::IsWhatPair(int var[CARD_NUM_MAX], int who, int pair, int pairname)
{
	for (int i = 1; i < CARD_NUM_MAX; i++)
	{
		if (var[i] == pair)
		{
			if (who)
			{
				RegistHuman(pairname, i);
			}
			else
			{
				RegistNpc(pairname, i);
			}
			return true;
		}
	}
	return false;
}

bool Judge::IsFullHouse(int var[CARD_NUM_MAX], int who)
{
	//ペアがいくつあるのかをカウントする
	int sameCount = 0;
	int fullMax = 0;
	for (int i = 1; i < CARD_NUM_MAX; i++)
	{
		if (var[i] == 2)
		{
			sameCount++;
		}
		if (var[i] == 3)
		{
			sameCount += 2;
			fullMax = i;
		}

		if (sameCount == 3)
		{
			if (who)
			{
				RegistHuman(FULLHOUSE, fullMax);
			}
			else
			{
				RegistNpc(FULLHOUSE, fullMax);
			}
			return true;
		}
	}
	return false;
}

bool Judge::IsTwoPair(int var[CARD_NUM_MAX], int who)
{
	//ペアがいくつあるのかをカウントする
	int sameCount = 0;
	int twoMax = 0;
	for (int i = 1; i < CARD_NUM_MAX; i++)
	{
		if (var[i] == 2)
		{
			sameCount++;
			twoMax = i;
		}
		if (sameCount == 2)
		{
			if (who)
			{
				RegistHuman(TWO_PAIR, twoMax);
			}
			else
			{
				RegistNpc(TWO_PAIR, twoMax);
			}
			return true;
		}
	}
	return false;
}

void Judge::Initialize()
{
}

void Judge::Update()
{
}

void Judge::Draw()
{
}

void Judge::Release()
{
}



/*
	変更点

	handCheck内を全変更
	RegistHuman以下の関数を実装

	RegistHuman,RegistNpcを使うように変更したため通常のRegistを削除

	頭にdefineを3つ追加
	使わなくなったpairpoint, 各役のフラグを削除
*/

/*	judge更新したので移植時の注意点

　　HandCheck内で配列にhandを登録するときに-1の値を使っているが本体では入れないこと

  　各レジストを追加
　　どの役(ペア)がそろっているかを確認できる関数を実装
  　define追加

   judgeのhandcheckは全体的に変わっているのでほぼ全写しでもいいかも
*/