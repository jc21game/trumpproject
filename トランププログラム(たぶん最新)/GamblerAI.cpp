#include "GamblerAI.h"
#include "Engine/Model.h"


//int GamblerAI::turn_ = HUMAN_TURN;

GamblerAI::GamblerAI(IGameObject* parent) :
	AI(parent),
	isDuplication_(false), allStopFlg_(true)
{
}

GamblerAI::~GamblerAI()
{
}

void GamblerAI::Initialize()
{
}

void GamblerAI::Update()
{
}

void GamblerAI::Draw()
{
}

void GamblerAI::Release()
{
}

void GamblerAI::Thinking(std::vector<Hand> hand, bool(&changeFlg)[5])
{

	if (CheckPair(hand))
	{
		ChangePairBluff(hand, changeFlg);
	}
	else
	{
		for (int remaining = 5; remaining >= 4; remaining--)
		{
			if (CheckStraight(hand, remaining))
			{
				ChangeStraight(hand, changeFlg, remaining);
				return;
			}
			else if (CheckMark(hand, remaining))
			{
				ChangeFlash(hand, changeFlg);
				return;
			}
		}
		ChangeHigh(hand, changeFlg, 4);
	}
}

bool GamblerAI::IsDoBluf(int role, int numMax)
{
	//セミブラフ(2ペアか3カードの時に発動)
	if (role == TWOPAIR || role == THREECARD)
	{
		return true;
	}
	//ピュアブラフ
	if (role == ONEPAIR && numMax <= 7)
	{
		srand((unsigned int)time(NULL));
		if (rand() % 5 == 0)
		{
			return true;
		}
	}
	return false;
}

int GamblerAI::Howmuchbet(int role, bool bluf, int maxbet, int chip, int totalbet)
{
	int betdata;

	if (bluf)
	{
		betdata = 10 + ((rand() % 5) * 5);
	}
	else
	{
		betdata = 40 + ((rand() % 5) * 5);
	}
	return Detailbet(betdata, maxbet, chip, totalbet);
}
