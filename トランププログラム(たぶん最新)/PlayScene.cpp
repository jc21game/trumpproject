#include <string>
#include <vector>
#include "PlayScene.h"
#include "Player.h"
#include "Judge.h"
#include "Human.h"
#include "Npc.h"
#include "Image.h"
#include "Engine/Model.h"
#include "Engine/Camera.h"
#include "SceneBetweenPassData.h"
#include "Engine/Audio.h"

#define NPC		 	 0
#define HUMAN		 1
#define DRAW		 2

int PlayScene::gamestatus_ = SHUFFLE;

//コンストラクタ
PlayScene::PlayScene(IGameObject * parent)
	: IGameObject(parent, "PlayScene"), battlecnt_(-1),isBetSecondTime_(false), cardSum_(CARD_SUM), cardSumItr_(cardSum_.begin()),
	shuffleCardSum_(CARD_SUM), shuffleitr_(shuffleCardSum_.begin()),hModel_(-1),hRoom_(-1),
	playerHand_(0),npcHand_(0),result_(0),
	betCnt_(1), humanBet_(0), npcBet_(0),witchWin(0),ActCorsor_(-1),isSelectBet_(false), initFinish_(false), oneTime_(false),
	hMeterFrame_(1), betTimeUi_(-1), resultVoiceFlg_(true), hBgm_(0), hVoice_(0)
{
}

//初期化
void PlayScene::Initialize()
{
	srand((unsigned)time(NULL));

	//音源ロード
	LoadAudio();

	//画像データのロード
	LoadData();

	//カードのモデルのロード
	CardModelLoad();

	//選択された易度のデータを受け取る
	AI_LEVEL diff_ = SceneBetweenPassData::GetLevelData();
	//難易度に応じたAIを割り当てる
	//※ここに関数追加

	//BGM鳴らす(キュー名だけ指定すればおｋ)
	std::string name[8] = { "BGM1", "BGM2", "BGM3", "BGM4", "BGM5", "BGM6","BGM7", "BGM8" };
	
	for (int i = 0; i < 8; i++)
	{
		bgmName_[i] = name[i].c_str();
	}

	hBgm_ = rand() % 8;
	Audio::Play( (char*)bgmName_[hBgm_].c_str() );

	CardShuffle();

	pJudge_ = CreateGameObject<Judge>(this);
	pHuman_ = CreateGameObject<Human>(this);
	pNpc_ = CreateGameObject<Npc>(this);
}

//音源をまとめてロード
void PlayScene::LoadAudio()
{
	//BGM、ボイスのロード
	Audio::Loadxwb();
	Audio::Loadxsb();

}

void PlayScene::CardModelLoad()
{
	cardSumItr_ = cardSum_.begin();
	//カードを初期化
	for (int i = 0; i < CARD_MARK_MAX; i++)
	{
		for (int j = 0; j < CARD_NUMBER_MAX; j++)
		{
			cardSumItr_->mark = i;
			cardSumItr_->num = j;
			++cardSumItr_;
		}
	}
}

void PlayScene::CardShuffle()
{
	//シャッフルの処理
	for (cardSumItr_ = cardSum_.begin(); cardSumItr_ != cardSum_.end(); ++cardSumItr_)
	{
		//全シャッフル用のカードから無作為に番地を取り、そこに山札のカードを仕込む
		while (isDuplication_ == false)
		{
			shuffleitr_ = shuffleCardSum_.begin() + rand() % CARD_SUM;

			//ランダムにとった番地にすでに値が入っていないときにのみ
			if (shuffleitr_->mark == -1)
			{
				shuffleitr_->mark = cardSumItr_->mark;
				shuffleitr_->num = cardSumItr_->num;
				shuffleitr_->modelname = cardSumItr_->modelname;
				isDuplication_ = true;
			}
		}
		isDuplication_ = false;
	}
}

void PlayScene::CardReset()
{
	//カードを初期化
	shuffleitr_ = shuffleCardSum_.begin();
	for (int i = 0; i < CARD_MARK_MAX; i++)
	{
		for (int j = 0; j < CARD_NUMBER_MAX; j++)
		{
			shuffleitr_->mark = -1;
			shuffleitr_->num = -1;
			shuffleitr_->modelname = ("");
			++shuffleitr_;
		}
	}
	shuffleitr_ = shuffleCardSum_.begin();
}

void PlayScene::LoadData()
{
	//テーブルのモデルをロード
	hModel_ = Model::Load("Data/table.fbx");
	assert(hModel_ >= 0);

	hRoom_ = Model::Load("Data/Room.fbx");
	assert(hRoom_ >= 0);

	//roomのテクスチャの彩度下げたやつ用意
	//ちょっと目に優しくなった?
	//意見求む
	//↓これコメントアウトすると本家の画
	Model::SetTexture("Data/RoomTexture2.jpg", hRoom_);

	//ベット金額、ターンとかのUI
	std::string name[PICT_MAX] =
	{
		"HumanBetChipTotalSampleFS",
		"NPCBetChipTotalSampleFS",
		"WinLogo",
		"LoseLogo",
		"DrowLogo",
		"ResultPushSpace"
	};

	//画像データを読み込み
	for (int i = 0; i < PICT_MAX; i++)
	{
		hPict_[i] = Image::Load("Data/" + name[i] + ".png");
		assert(hPict_[i] >= 0);
	}

	//数字画像
	std::string chipPictName[CHIP_PICT_MAX] =
	{
		"Num0_Sample",
		"Num1_Sample",
		"Num2_Sample",
		"Num3_Sample",
		"Num4_Sample",
		"Num5_Sample",
		"Num6_Sample",
		"Num7_Sample",
		"Num8_Sample",
		"Num9_Sample",
		"GOLD_Sample"
	};

	//画像データを読み込み
	for (int i = 0; i < CHIP_PICT_MAX; i++)
	{
		chipPict_[i] = Image::Load("Data/" + chipPictName[i] + ".png");
		assert(chipPict_[i] >= 0);
	}

	//上がった役のUI
	std::string roleName[ROLE_MAX]
	{
		"HighCardsWin",
		"OnePairWin",
		"TwoPairWin",
		"ThreeCardsWin",
		"StraightWin",
		"FulushWin",
		"HullHouseWin",
		"FourCardsWin",
		"StraightFlushWin",
		"RoyalStraightFlushWin",
		"HighCardsLose",
		"OnePairLose",
		"TwoPairLose",
		"ThreeCardsLose",
		"StraightLose",
		"FulushLose",
		"HullHouseLose",
		"FourCardsLose",
		"StraightFlushLose",
		"RoyalStraightFlushLose",
	};

	//画像データを読み込み
	for (int i = 0; i < ROLE_MAX; i++)
	{
		rolePict_[i] = Image::Load("Data/RoleUI/" + roleName[i] + ".png");
		assert(rolePict_[i] >= 0);
	}

	//メーターの枠だけ別にロード
	hMeterFrame_ = Image::Load("Data/MeterFrame.png");
	assert(hMeterFrame_ >= 0);


	//賭けるか降りるかの状態のUI
	std::string actSelectUiName[ACT_SELECT_UI_MAX]
	{
		"CheckFoldUI",
		"BetButtonUI",
		"FoldButtonUI",
		"BetButtonSelectUI",
		"FoldButtonSelectUI",
	};

	for (int i = 0; i < ACT_SELECT_UI_MAX; i++)
	{
		actSelectPict_[i] = Image::Load("Data/CheckFoldUI/" + actSelectUiName[i] + ".png");
	}

	//ベット中に表示するUI
	betTimeUi_ = Image::Load("Data/BetTimeUI.png");

	//test oohisa
	//ターンの「/」コレ表示
	turnPict_ = Image::Load("Data/TurnUI.png");

	//カメラ作成
	Camera* pCamera = CreateGameObject<Camera>(this);
	pCamera->SetPosition(D3DXVECTOR3(0, 1.15f, -1.0f));

}

//更新
void PlayScene::Update()
{

	//各カードのポジションの設定
	switch (gamestatus_)
	{
	//ゲームの準備
	case SHUFFLE:
		battlecnt_++;

		//CardShuffle();
		pHuman_->InitializeGame();
		pNpc_->InitializeGame();

		//ベット上限金額を決定する
		DecisionMaxBet();

		//カードを交換し始める前に
		gamestatus_ = BEGIN_GAME;
		break;

	case BEGIN_GAME:
		//所持金を更新する
		DigitsCalculation(pHuman_->GetChip(), humanChipDigits_);
		DigitsCalculation(pNpc_->GetChip(), npcChipDigits_);

		//ベット金額を更新する
		DigitsCalculation(pHuman_->GetTotalBet(), humanBetDigits_);
		DigitsCalculation(pNpc_->GetTotalBet(), npcBetDigits_);

		if (Input::IsKeyDown(DIK_RETURN))
		{
			//ベットタイムへ
			gamestatus_ = BET_TIME;
			break;
		}

	//賭けるか、降りるかを選択する状態
	case ACT_SELECT:

		//賭けたとき
		if (Input::IsKeyDown(DIK_LEFT)) ActCorsor_ = SELECT_DO_BET;
		
		//降りたとき
		else if(Input::IsKeyDown(DIK_RIGHT)) ActCorsor_ = SELECT_DONOT_BET;

		//降りるを選択したとき
		if (Input::IsKeyDown(DIK_RETURN) && (ActCorsor_ == SELECT_DONOT_BET))
		{
			//降りたことにする
			pHuman_->SetIsFold(true);
			pHuman_->SetIsBet(true);

			//合計ベット金額を0にする
			pHuman_->SetTotalBet(0);
			pNpc_->SetTotalBet(0);

			//両者のベット金額を格納
			humanBet_ = pHuman_->GetTotalBet();
			npcBet_ = pNpc_->GetTotalBet();

			//ベット金額を更新する
			DigitsCalculation(humanBet_, humanBetDigits_);
			DigitsCalculation(npcBet_, npcBetDigits_);

			//「賭けていない状態」に戻す
			pHuman_->SetIsBet(false);
			pNpc_->SetIsBet(false);

			ActCorsor_ = -1;
			gamestatus_ = SHOW_RESULT;
		}

		//賭けたとき
		else if (Input::IsKeyDown(DIK_RETURN) && (ActCorsor_ == SELECT_DO_BET))
		{
			//両者のベット金額を格納
			humanBet_ = pHuman_->GetTotalBet();
			npcBet_ = pNpc_->GetTotalBet();

			//ベット金額を更新する
			DigitsCalculation(humanBet_, humanBetDigits_);
			DigitsCalculation(npcBet_, npcBetDigits_);

			//Humanのベット金額がnpcのベット金額より低かったら
			if (humanBet_ < npcBet_)
			{
				//不足分を計算
				int hChip = npcBet_ - humanBet_;

				//セットで所持金を引く
				pHuman_->SetChip(-hChip);

				//合計ベット金額を更新
				pHuman_->SetTotalBet(hChip);

				//所持金を更新する
				DigitsCalculation(pHuman_->GetChip(), humanChipDigits_);

				//ベット金額を更新する
				DigitsCalculation(pHuman_->GetTotalBet(), humanBetDigits_);
			}

			//npcのベット金額がHumanのベット金額より低かったら
			else if (npcBet_ < humanBet_)
			{
				//不足分を計算
				int hChip = humanBet_ - npcBet_;

				//セットで所持金を引く
				pNpc_->SetChip(-hChip);

				//合計ベット金額を更新
				pNpc_->SetTotalBet(hChip);

				//ベット金額を更新する
				DigitsCalculation(pNpc_->GetTotalBet(), npcBetDigits_);


				DigitsCalculation(pNpc_->GetChip(), npcChipDigits_);
			}

			////賭ける処理があるはず...
			////エンターキーで賭けれる状態に
			//if (Input::IsKeyDown(DIK_RETURN))
			//{
			//}

			//pNpcが賭けていなかったら
			if (!(pNpc_->GetIsBet()))
			{
				//賭けさせる
				pNpc_->SelectChip();

				pNpc_->SetIsBet(true);
			}

			//pHumanが賭けていなかったら
			if (!(pHuman_->GetIsBet()))
			{
				//賭けさせる
				pHuman_->InputBet();
			}


			isSelectBet_ = true;
			ActCorsor_ = -1;
			gamestatus_ = BET_TIME;
		}

		break;

	//ベットの時間(タイミングは仕様書に記載)
	case BET_TIME:
		if (Input::IsKeyDown(DIK_P)) battlecnt_ = 4;

		if ( isBetSecondTime_ == true && isSelectBet_ == false )
		{
			//所持金を更新する
			DigitsCalculation(pHuman_->GetChip(), humanChipDigits_);
			DigitsCalculation(pNpc_->GetChip(), npcChipDigits_);

			isSelectBet_ = true;
			gamestatus_ = ACT_SELECT;
			break;
		}

		//エンターキーで賭けれる状態に
		//if (Input::IsKeyDown(DIK_RETURN))
		//{
		//}

		//pNpcが賭けていなかったら
		if (!(pNpc_->GetIsBet()))
		{
			//賭けさせる
			pNpc_->SelectChip();

			//「賭けた状態」にする
			pNpc_->SetIsBet(true);
		}

		//pHumanが賭けていなかったら
		if (!(pHuman_->GetIsBet()))
		{
			//賭けさせる
			pHuman_->InputBet();

			//所持金を更新する
			DigitsCalculation(pHuman_->GetChip(), humanChipDigits_);

			//ベット金額を更新する
			DigitsCalculation(pHuman_->GetTotalBet(), humanBetDigits_);
		}

		//両者が賭けたなら
		if ((pHuman_->GetIsBet()) && (pNpc_->GetIsBet()))
		{
			//NPCが賭けたあとの金額はこのタイミングで開示
			DigitsCalculation(pNpc_->GetChip(), npcChipDigits_);

			//両者のベット金額を格納
			humanBet_ = pHuman_->GetTotalBet();
			npcBet_ = pNpc_->GetTotalBet();

			//ベット金額を更新する
			DigitsCalculation(npcBet_, npcBetDigits_);

			//1回目のベット時間が終了
			if (isBetSecondTime_ == false)
			{
 				gamestatus_ = PLAY_GAME;
				isBetSecondTime_ = true;

				//ベットをクリア
				pHuman_->SetBet(0);
				pNpc_->SetBet(0);

				//２回目のベット上限金額を決定
				pHuman_->SetMaxBet(maxBet_ + humanBet_);
				pNpc_->SetMaxBet(maxBet_ + npcBet_);

				//「賭けていない状態」に戻す
				pHuman_->SetIsBet(false);
				pNpc_->SetIsBet(false);
			}

			//2回目のベット時間が終了
			else if (isBetSecondTime_ == true)
			{
				gamestatus_ = SHOW_RESULT;
				//isBetSecondTime_ = false;

				//合計ベット金額を0にする
				pHuman_->SetTotalBet(0);
				pNpc_->SetTotalBet(0);

				//ベット金額を更新する
				DigitsCalculation(pHuman_->GetTotalBet(), humanBetDigits_);
				DigitsCalculation(pNpc_->GetTotalBet(), npcBetDigits_);

				//「賭けていない状態」に戻す
				pHuman_->SetIsBet(false);
				pNpc_->SetIsBet(false);
			}
		}
		break;

	case PLAY_GAME:
		//現在ゲーム中
		break;

	//結果を表示
	case SHOW_RESULT:

		//判定
		//役を判定してもらう
		playerHand_ = pJudge_->handCheck(pHuman_->GetHand(), HUMAN);
		npcHand_ = pJudge_->handCheck(pNpc_->GetHand(), NPC);
		result_ = pJudge_->GameJudge(pHuman_->GetIsFold());
		// ↑これに勝ち負けが入っている

		//勝ち負けをNPC,Playerごとにカウントしておく
		if (result_ == WIN)
		{
			//ボイス(ランダム)NPC負け
			std::string voiceName[3] =
			{ 
			  "line-girl1_line-girl1-しくしく 3",
			  "line-girl1_line-girl1-げげー… 2",
			  "line - girl1_line - girl1 - ううっ…（泣く）"
			};
			hVoice_	= rand() % 3;
			Audio::Play((char*)voiceName[hVoice_].c_str());

			//Humanの所持金を変更
			pHuman_->SetChip((humanBet_ + npcBet_));

			//Humanの所持金を更新する
			DigitsCalculation(pHuman_->GetChip(), humanChipDigits_);

			//現在5ターン目なら
			if (battlecnt_ < ENDGAMECOUNT)
			{
				//Npcの所持金が0なら
				if ((pNpc_->GetChip()) == NOT_HAVE_BETCHIP)
				{
					//5回以上勝負したことにして、強制終了するようにする
					battlecnt_ = ENDGAMECOUNT;
				}
			}
		}
		else if (result_ == LOSE)
		{
			//ボイス(ランダム)NPC勝ち
			std::string voiceName[4] =
			{
			  "info-girl1_info-girl1-当たり 6",
			  "info-girl1_info-girl1-グット！",
			  "line-girl1_line-girl1-えへへ…",
			  "line-girl1_line-girl1-うふふ1"
			};

			hVoice_ = rand() % 4;
			Audio::Play((char*)voiceName[hVoice_].c_str());

			//Npcの所持金を変更
			pNpc_->SetChip((humanBet_ + npcBet_));

			//Npcの所持金を更新する
			DigitsCalculation(pNpc_->GetChip(), npcChipDigits_);

			//現在5ターン目なら
			if (battlecnt_ < ENDGAMECOUNT)
			{
				//Humanの所持金が0なら
				if ((pHuman_->GetChip()) == NOT_HAVE_BETCHIP)
				{

					//5回以上勝負したことにして、強制終了するようにする
					battlecnt_ = ENDGAMECOUNT;
				}
			}
		}
		else if (result_ == DROW)
		{
			//ボイス(ランダム)引き分け
			std::string voiceName[3] =
			{
			  "info-girl1_info-girl1-あとちょとだったね 7",
			  "info-girl1_info-girl1-おっーしい 8",
			  "line-girl1_line-girl1-おおー！ 2"
			};

			hVoice_ = rand() % 3;
			Audio::Play((char*)voiceName[hVoice_].c_str());

			//所持金を賭ける前の状態に戻す
			pHuman_->SetChip(humanBet_);
			DigitsCalculation(pHuman_->GetChip(), humanChipDigits_);

			//所持金を賭ける前の状態に戻す
			pNpc_->SetChip(npcBet_);
			DigitsCalculation(pNpc_->GetChip(), npcChipDigits_);
		}

		gamestatus_ = TURN_CARD;
		break;

	case TURN_CARD:
		//カードを回転中
		break;

	//ゲーム間の停止状態
	case STOP_GAME:

		//ここでベット金額を更新
		//humanの所持金が、npcの所持金より少なかったら
		if (pHuman_->GetChip() < pNpc_->GetChip())
		{
			//npcの勝利
			witchWin = NPC;
		}
		//多かったら（現在は、ドローが無いため、ドローの場合はhumanの勝ちになります)
		else if (pHuman_->GetChip() > pNpc_->GetChip())
		{
			//humanの勝利
			witchWin = HUMAN;
		}

		//同じだったら
		else if (pHuman_->GetChip() == pNpc_->GetChip())
		{
			//引き分けとする
			witchWin = DRAW;
		}

		if (oneTime_ == false)
		{
			oneTime_ = true;
			break;
		}

		if (Input::IsKeyDown(DIK_SPACE))
		{
			//ベット金額を更新する
			DigitsCalculation(pHuman_->GetTotalBet(), humanBetDigits_);
			DigitsCalculation(pNpc_->GetTotalBet(), npcBetDigits_);

			//まだゲームが5回行ってなかったら
			if ( battlecnt_ < ENDGAMECOUNT )
			{
				//ここで手札を戻してあげる
				pHuman_->ReturnCard( pHuman_->GetHand() );
				pNpc_->ReturnCard(pNpc_->GetHand());

				CardReset();
				CardShuffle();
				isBetSecondTime_ = false;
				isSelectBet_ = false;
				gamestatus_ = SHUFFLE;
			}
			
			//5回勝負したら終わり
			else
			{
				gamestatus_ = END_GAME;
			}
		}
		break;

	//ゲーム終了の処理
	case END_GAME:
		//リザルト画面表示中にエンターキー入力で
		if (Input::IsKeyDown(DIK_RETURN))
		{
			//難易度選択に戻る
			battlecnt_ = 0;
			gamestatus_ = SHUFFLE;

			SceneManager::ChangeScene(SCENE_ID_SELECT_DIFFICULT);
		}

		//BGM停止
		Audio::Stop((char*)bgmName_[hBgm_].c_str());
		break;
	}

	
}

//描画
void PlayScene::Draw()
{
	//描画の処理//
	//テーブル
	Model::Draw(hModel_);

	D3DXMATRIX rotateMat;
	D3DXMatrixRotationX(&rotateMat, D3DXToRadian(25));   //Y軸で90°回転させる行列
	Model::SetMatrix(hRoom_, worldMatrix_ * rotateMat);
	Model::Draw(hRoom_);

	//UI関係の描画
	//一つの配列にまとめたのでUIの分だけ描画
	for (int i = 0; i < WIN_LOGO; i++)
	{
		Image::SetMatrix(hPict_[i], worldMatrix_);
		Image::Draw(hPict_[i]);
	}

	D3DXMATRIX turnUiMat;
	D3DXMatrixTranslation(&turnUiMat, 30, 35, 0);

	//ターンの「 / 」<-これ
	Image::SetMatrix(turnPict_, turnUiMat);
	Image::Draw(turnPict_);
	
	//ベットで使っている数字を使ってターンのUI表示
	//分母の5
	D3DXMATRIX turnNumUnderMat;
	D3DXMatrixTranslation(&turnNumUnderMat, 60, 65, 0);
	Image::SetMatrix(chipPict_[NUM5_SAMPLE], turnNumUnderMat);
	Image::Draw(chipPict_[NUM5_SAMPLE]);
	
	//分子
	D3DXMATRIX turnNumUpMat;
	D3DXMatrixTranslation(&turnNumUpMat, 5, 5, 0);
	Image::SetMatrix(chipPict_[battlecnt_ + 1], worldMatrix_);
	Image::Draw(chipPict_[battlecnt_ + 1]);

	//ユーザーの単位
	D3DXMATRIX hGoldMat;
	D3DXMatrixTranslation(&hGoldMat, VALUE_PLACE * 4, HUMAN_CHIP_POS_Y, 0);

	Image::SetMatrix(chipPict_[GOLD], hGoldMat);
	Image::Draw(chipPict_[GOLD]);

	//NPCの単位
	D3DXMATRIX nGoldMat;
	D3DXMatrixTranslation(&nGoldMat, NPC_CHIP_POS_X, NPC_CHIP_POS_Y, 0);

	Image::SetMatrix(chipPict_[GOLD], nGoldMat);
	Image::Draw(chipPict_[GOLD]);

	//所持金を描画
	NumDraw(humanChipDigits_, npcChipDigits_, HUMAN_CHIP_POS_Y, NPC_CHIP_POS_Y);

	//ベット金額を描画
	NumDraw(humanBetDigits_, npcBetDigits_, HUMAN_CHIP_POS_Y - 120, NPC_CHIP_POS_Y + 130);

	//ゲームの進行状態に合わせて描画するものを変更
	switch (gamestatus_)
	{
	case ACT_SELECT:
		//Bet,Foldはカーソルに合わせて選択されている方を描画
		//※最初はどちらも選択されておらず、
		for (int i = 0; i <= FOLD; i++)
		{
			Image::SetMatrix(actSelectPict_[i], worldMatrix_);
			Image::Draw(actSelectPict_[i]);
		}

		if (ActCorsor_ == SELECT_DO_BET)
		{
			Image::SetMatrix(actSelectPict_[BET_SELECT], worldMatrix_);
			Image::Draw(actSelectPict_[BET_SELECT]);
		}

		else if (ActCorsor_ == SELECT_DONOT_BET)
		{
			Image::SetMatrix(actSelectPict_[FOLD_SELECT], worldMatrix_);
			Image::Draw(actSelectPict_[FOLD_SELECT]);
		}

		break;

	case BET_TIME:

		Image::SetMatrix(betTimeUi_, worldMatrix_);
		Image::Draw(betTimeUi_);

		break;

	case STOP_GAME:
		if (result_ == WIN)
		{
			Image::SetMatrix(rolePict_[playerHand_], worldMatrix_);
			Image::Draw(rolePict_[playerHand_]);

			Image::SetMatrix(hPict_[WIN_LOGO], worldMatrix_);
			Image::Draw(hPict_[WIN_LOGO]);
		}

		else if (result_ == LOSE)
		{
		//	Image::SetMatrix(rolePict_[playerHand_ + CHANGE], worldMatrix_);
		//	Image::Draw(rolePict_[playerHand_ + CHANGE]);

			Image::SetMatrix(hPict_[LOSE_LOGO], worldMatrix_);
			Image::Draw(hPict_[LOSE_LOGO]);
		}

		else if (result_ == DROW)
		{
		//	Image::SetMatrix(hPict_[DRAW_LOGO], worldMatrix_);
		//	Image::Draw(hPict_[DRAW_LOGO]);

			Image::SetMatrix(hPict_[DRAW_LOGO], worldMatrix_);
			Image::Draw(hPict_[DRAW_LOGO]);
		}
		break;

		//結果表示
	case END_GAME:
		D3DXMATRIX mat;
		switch (witchWin)
		{
		case HUMAN:
			D3DXMatrixTranslation(&mat, 300, 300, 0);
			Image::SetMatrix(hPict_[WIN_LOGO], mat);
			Image::Draw(hPict_[WIN_LOGO]);

			//ボイス(ランダム)NPC負け
			if (resultVoiceFlg_)
			{
				std::string voiceName[3] =
				{ 
					"info-girl1_info-girl1-おめでとう 9",
					"info-girl1_info-girl1-すごいすごい！ 3",
					"info-girl1_info-girl1-合格です 2"
				};
				hVoice_ = rand() % 3;
				Audio::Play((char*)voiceName[hVoice_].c_str());
				resultVoiceFlg_ = false;
			}
			break;

		case NPC:
			D3DXMatrixTranslation(&mat, 250, 300, 0);
			Image::SetMatrix(hPict_[LOSE_LOGO], mat);
			Image::Draw(hPict_[LOSE_LOGO]);

			if (resultVoiceFlg_)
			{
				//ボイス(ランダム)NPC勝ち
				std::string voiceName[3] =
				{
					"info-girl1_info-girl1-エクセレント！",
					"info-girl1_info-girl1-大当たり 5",
					"line-girl1_line-girl1-ありがと！1"
				};

				hVoice_ = rand() % 3;
				Audio::Play((char*)voiceName[hVoice_].c_str());

				resultVoiceFlg_ = false;
			}
			break;

		case DRAW:
			Image::SetMatrix(hPict_[DRAW_LOGO], mat);
			Image::Draw(hPict_[DRAW_LOGO]);
			break;
		}
	}
}

//開放
void PlayScene::Release()
{
}

//金額計算
void PlayScene::DigitsCalculation(int chip, int* pDigits)
{
	for (int i = 0; i < CHIP_DIGITS_MAX; i++)
	{
		switch (i)
		{
			case A_DIGIT:
				//１桁目を算出
				pDigits[i] = (chip % 10);
				break;

			case DOUBLE_DIGITS:
				//２桁目を算出
				pDigits[i] = ((chip / 10) % 10);
				break;

			case THREE_DIGITS:
				//３桁目算出
				pDigits[i] = (chip / 100);
				break;
		}
	}
}

void PlayScene::DecisionMaxBet()
{
	//両者の所持金を取得
	int humanChip = pHuman_->GetChip();
	int npcChip = pNpc_->GetChip();

	if (humanChip < npcChip)
	{
		//Humanの所持金の50%を上限に
		maxBet_ = (humanChip / 2);
	}
	else if (npcChip < humanChip)
	{
		//npcの所持金の50%を上限に
		maxBet_ = (npcChip / 2);
	}
	else
	{
		pHuman_->GetHand();

		//金額が等しい場合はとりあえずHumanの所持金の50%を上限に
		maxBet_ = (humanChip / 2);
	}

	//一回目のベット上限金額を決定
	pHuman_->SetMaxBet(maxBet_);
	pNpc_->SetMaxBet(maxBet_);
}

//交換する際のカード抜
void PlayScene::RemoveCardToChange()
{
	shuffleCardSum_.erase( shuffleCardSum_.begin());
}

//初期化の際のカード抜
void PlayScene::RemoveCardToInit()
{
	shuffleCardSum_.erase(shuffleCardSum_.begin(), shuffleCardSum_.begin() + 5);
}

//カードを戻す
void PlayScene::InCard(std::vector<Hand>::iterator incard)
{
	Hand changeHand;
	changeHand.mark = incard->mark;
	changeHand.modelname = "";
	changeHand.num = incard->num;

	shuffleCardSum_.push_back(changeHand);
}

//所持金とベット金額を描画する
void PlayScene::NumDraw(int* humanDigits, int* npcDigits, int hYPos, int nYPos)
{
	//桁毎
	for (int digitsNum = 0; digitsNum < CHIP_DIGITS_MAX; digitsNum++)
	{
		//金の種類毎
		for (int chipNum = 0; chipNum < (CHIP_PICT_MAX - 1); chipNum++)
		{
			int xPos;

			//Humanのお金を描画
			//今見ている桁の数字が、Humanの所持金の桁の数字と同じなら
			if (humanDigits[digitsNum] == chipNum)
			{
				switch (digitsNum)
				{
				case A_DIGIT:
					xPos = VALUE_PLACE * 3;
					break;

				case DOUBLE_DIGITS:
					xPos = VALUE_PLACE * 2;
					break;

				case THREE_DIGITS:
					xPos = VALUE_PLACE;
					break;
				}
				ChipDraw(humanDigits, digitsNum, chipNum, xPos, hYPos);
			}

			//NPCのお金を描画
			//今見ている桁の数字が、NPCの所持金の桁の数字と同じなら
			if (npcDigits[digitsNum] == chipNum)
			{
				switch (digitsNum)
				{
				case A_DIGIT:
					xPos = NPC_CHIP_POS_X - VALUE_PLACE;
					break;

				case DOUBLE_DIGITS:
					xPos = NPC_CHIP_POS_X - (VALUE_PLACE * 2);
					break;

				case THREE_DIGITS:
					xPos = NPC_CHIP_POS_X - (VALUE_PLACE * 3);
					break;
				}
				ChipDraw(npcDigits, digitsNum, chipNum, xPos, nYPos);
			}
		}
	}
}

//お金を描画
void PlayScene::ChipDraw(int* playerDigits, int digits, int chip, const int xPos, const int yPos)
{
	D3DXMATRIX mat;

	//描画するか
	bool isDraw = false;

	D3DXMatrixTranslation(&mat, xPos, yPos, 0);

	switch (digits)
	{
		case A_DIGIT:
			//描画する
			isDraw = true;

			break;

		case DOUBLE_DIGITS:
			//２桁目が0以外の時
			if (playerDigits[digits] != NUM0_SAMPLE)
			{
				//描画する
				isDraw = true;
			}
			//２桁目が0の時、３桁目が0以外の時は
			else if (playerDigits[digits + 1] != NUM0_SAMPLE)
			{
				//描画する
				isDraw = true;
			}
			break;

		case THREE_DIGITS:
			//数字が0以外なら
			if (playerDigits[digits] != NUM0_SAMPLE)
			{
				//描画する
				isDraw = true;
			}
			break;
	}

	//描画するなら
	if (isDraw)
	{
		Image::SetMatrix(chipPict_[chip], mat);
		Image::Draw(chipPict_[chip]);
	}
}
