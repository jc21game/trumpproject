#include "BeginnerAI.h"
#include "Engine/Model.h"


//int BeginnerAI::turn_ = HUMAN_TURN;

BeginnerAI::BeginnerAI(IGameObject* parent) :
	AI(parent),
	isDuplication_(false), allStopFlg_(true)
{
}

BeginnerAI::~BeginnerAI()
{
}

void BeginnerAI::Initialize()
{
}

void BeginnerAI::Update()
{
}

void BeginnerAI::Draw()
{
}

void BeginnerAI::Release()
{
}

void BeginnerAI::Thinking(std::vector<Hand> hand, bool(&changeFlg)[5])
{
	srand((unsigned int)time(NULL));

	//int bluff = rand() % 5;

	//if (bluff)
	//{
		if (CheckPair(hand))
		{
			ChangePair(hand, changeFlg);
		}
		else
		{
			for (int remaining = 5; remaining >= 3; remaining--)
			{
				if (CheckStraight(hand, remaining))
				{
					ChangeStraight(hand, changeFlg, remaining);
					return;
				}
				else if (CheckMark(hand, remaining))
				{
					ChangeFlash(hand, changeFlg);
					return;
				}
			}
			//ChangeHigh(hand, changeFlg, 3);
		}
	//}
}

void BeginnerAI::Changerand(bool(&changeFlg)[5], int exchange)
{
	int count = 0;

	while (1)
	{
		int x = rand() % 5;
		if (changeFlg[x])
		{
			changeFlg[x] = false;
			count++;
		}
		if (count >= exchange)
		{
			break;
		}
	}
}

bool BeginnerAI::IsDoBluf(int role, int numMax)
{
	return false;
}

int BeginnerAI::Howmuchbet(int role, bool bluf, int maxbet, int chip, int totalbet)
{
	int roleData = role;

	//所持金が0の時
	if (chip <= 0)
	{
		return 0;
	}
	//下記の計算 : (roleに応じての最低値) + 0 or 5 or 10
	srand((unsigned int)time(NULL));

	int betdata = ((roleData + 1) * 5) + ((rand() % 3) * 5);

	return Detailbet(betdata, maxbet, chip, totalbet);
}
