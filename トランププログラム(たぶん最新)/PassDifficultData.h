#pragma once

enum AI_LEVEL
{

	LEVEL_EASY,		//初心者
	LEVEL_NORMAL,	//普通
	LEVEL_HARD,		//上級者

};

//難易度データをプレイシーンへ渡すための名前空間
namespace PassDifficultData
{
	void SetLevelData(AI_LEVEL aiLevel);	//プレイシーン側でセットする用
	AI_LEVEL GetLevelData();	//難易度選択画面で選択した難易度を持ってくる用

	//void SetMeterStatus(int meter);

	//int GetMeterStatus();

};

