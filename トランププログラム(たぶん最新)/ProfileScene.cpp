#include "ProfileScene.h"

//コンストラクタ
ProfileScene::ProfileScene(IGameObject * parent)
	: IGameObject(parent, "ProfileScene")
{
}

//初期化
void ProfileScene::Initialize()
{
}

//更新
void ProfileScene::Update()
{
}

//描画
void ProfileScene::Draw()
{
}

//開放
void ProfileScene::Release()
{
}