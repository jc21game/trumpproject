#pragma once

//プレイシーンを管理するクラス
#include "Engine/Global.h"

//ヘッダーは使いたくはないがclassを使いたいときに行う
//プロトタイプ宣言
class Camera;

class TitleScene : public IGameObject
{
	//モジュールの名前
	enum
	{
		TITLE,
		BACKGROUND,
		PUSH_ENTER,
		PICT_MAX,
	};

	int hModel_;			//モデル番号(背景のみ)
	int hPict_[PICT_MAX];	//画像番号

	D3DXMATRIX matPic_[PICT_MAX];			//画像用行列

public:
	//コンストラクタ
	//引数：parent  親オブジェクト（SceneManager）
	TitleScene(IGameObject* parent);

	//TitleScene();
	~TitleScene();


	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;
};

