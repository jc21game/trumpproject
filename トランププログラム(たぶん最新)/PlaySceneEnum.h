#pragma once

//UI画像追加するなら必ず「WIN_LOGO上」に追加
//ループの都合上順番大事
//リザルト関係は「NPC_...より下」に追加
//カードの状態
enum STATUS
{
	STOP,		//基本状態
	UNCHECK,	//カードのy座標を下げる状態
	CHECK,		//カードのy座標を上げる状態
	MOVE_OUT,	//カードのx座標を右に移動する状態
	MOVE_IN,	//カードのx座標を左に移動する状態
	hairu,		//UNCHECKと同じことをしているのでいらない？
	LOCK,		//リタイア画像出してる時にカードは動かさない
};

enum ACT_SELECT_UI
{
	CHECK_FOLD,		//降りますか、賭けますか
	BET,			//賭けるボタン
	FOLD,			//降りるボタン
	BET_SELECT,		//賭けるボタン選択中
	FOLD_SELECT,	//降りるボタン選択中
	ACT_SELECT_UI_MAX,	
};

//UI画像表示用の列挙
enum
{
	HUMAN_BET_CHIP_TOTAL,	//ヒューマン側のベット金額
	NPC_BET_CHIP_TOTAL,		//NPC側のベット金額
	WIN_LOGO,		//winのロゴ
	LOSE_LOGO,		//loseのロゴ
	DRAW_LOGO,		//drowのロゴ
	RESUL_TPUSH_SPACE,	//プッシュスペース
	PICT_MAX,
};

enum METER
{
	//METER_0,		//メーター0/5の画像
	//METER_1,		//メーター1/5の画像
	//METER_2,		//メーター2/5の画像
	//METER_3,		//メーター3/5の画像
	//METER_4,		//メーター4/5の画像
	//METER_5,		//メーター5/5の画像
	METER_LOW_2,	//円柱の時のメーター
	METER_LOW_1,	//ローポリ時のメーター
	METER_DEFAULT,	//普通の美少女時(デフォルト時)のメーター
	METER_HI_1,		//美少女(装飾品+1)
	METER_HI_2,		//美少女(装飾品+2)
	METER_MAX
};

//役と勝敗を合わせた画像表示用の列挙
enum
{
	HIGH_CARD_WIN,		//ノーカード勝ち
	ONE_PAIR_WIN,		//ワンペア勝ち
	TWO_PAIR_WIN,		//ツーペア勝ち
	THREE_CARD_WIN,		//スリーカード勝ち
	STRAIGHT_WIN,		//ストレート勝ち
	FULUSH_WIN,		//フラッシュ勝ち
	HULL_HOUSE_WIN,		//フルハウス勝ち
	FOUR_CARDS_WIN,		//フォーカード勝ち
	STRAIGHT_FLUSH_WIN,			//ストレートフラッシュ勝ち
	ROYAL_STRAIGHT_FLUSH_WIN,	//ロイヤルストレートフラッシュ勝ち
	HIGH_CARD_LOSE,		//ノーカード負け
	ONE_PAIR_LOSE,		//ワンペア負け
	TWO_PAIR_LOSE,		//ツーペア負け
	THREE_CARD_LOSE,	//スリーカード負け
	STRAIGHT_LOSE,		//ストレート負け
	FULUSH_LOSE,	//フラッシュ負け
	HULL_HOUSE_LOSE,	//フルハウス負け
	FOUR_CARDS_LOSE,	//フォーカード負け
	STRAIGHT_FLUSH_LOSE,		//ストレートフラッシュ負け
	ROYAL_STRAIGHT_FLUSH_LOSE,	//ロイヤルストレートフラッシュ負け
	ROLE_MAX,
};

//ゲームの状態の列挙
enum
{
	SHUFFLE,	//シャッフル中
	BEGIN_GAME,	//ゲーム開始前
	PLAY_GAME,	//ゲーム中
	BET_TIME,	//ベット時間
	ACT_SELECT,	//賭けるか降りるか選択	
	TURN_CARD,	//カード回転中
	SHOW_RESULT, //結果表示
	EXTENSION_METER,	//メーターを増減
	STOP_GAME,	//1ゲーム毎の間の停止状態
	END_GAME,	//リザルト状態
	MAX_STATUS
};

//勝敗の列挙
enum
{
	DROW,
	LOSE,
	WIN
};

//所持金関係の画像
enum CHIP
{
	NUM0_SAMPLE,
	NUM1_SAMPLE,
	NUM2_SAMPLE,
	NUM3_SAMPLE,
	NUM4_SAMPLE,
	NUM5_SAMPLE,
	NUM6_SAMPLE,
	NUM7_SAMPLE,
	NUM8_SAMPLE,
	NUM9_SAMPLE,
	GOLD,
	CHIP_PICT_MAX
};

//所持金の桁数用
enum CHIP_DIGITS
{
	A_DIGIT,		//１桁目
	DOUBLE_DIGITS,	//２桁目
	THREE_DIGITS,	//３桁目
	CHIP_DIGITS_MAX	//桁数上限
};

enum
{
	TURN1,
	TURN2,
	TURN3,
	TURN4,
	TURN5,
	TURN_UI_MAX
};

struct Hand
{
	int num = -1;				//カードのナンバー
	int mark = -1;				//カードのマーク
	std::string modelname;		//モデルの名前
	STATUS status;					//カードの状態
	float yPosition = 1.5f;		//Y座標
	float xPosition = -0.6f;	//X座標
	float rotateY = 180.0f;		//Y軸回転
};