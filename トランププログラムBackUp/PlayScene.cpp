#include <string>
#include "PlayScene.h"
#include "Judge.h"
#include "Human.h"
#include "Npc.h"
//#include "Player.h"
#include "Image.h"
#include "Engine/Model.h"
#include "Engine/Camera.h"

#define NPC 0
#define HUMAN 1

const int CHIP_POS_X = -70;			//所持金の枠画像のxの値
const int NPC_CHIP_POS_Y = -950;	//NPCの所持金の枠画像のyの値

//コンストラクタ
PlayScene::PlayScene(IGameObject * parent)
	: IGameObject(parent, "PlayScene"), battlecnt_(0),isDrawRetire_(false), isRetire_(false), isDrawResult_(false)
{
}

//初期化
void PlayScene::Initialize()
{
	srand((unsigned)time(NULL));

	pHuman_ = CreateGameObject<Human>(this);
	pNpc_ = CreateGameObject<Npc>(this);
	//CreateGameObject<Judge>(this);
 	/*Player* pPlayer = CreateGameObject<Player>(this);*/

	LoadData();

}

void PlayScene::LoadData()
{
	std::string pictName[RETIRE_PICT_MAX]
	{
		"Retire",
		"NotRetire",
		"Result",
	};

	//画像データの名前
	std::string name[PICT_MAX] =
	{
		"TurnSample2FS",
		"HumanBetChipTotalSampleFS",
		"NPCBetChipTotalSampleFS",
	};

	//数字画像
	std::string chipPictName[CHIP_PICT_MAX] =
	{
		"Num0_Sample",
		"Num1_Sample",
		"Num2_Sample",
		"Num3_Sample",
		"Num4_Sample",
		"Num5_Sample",
		"Num6_Sample",
		"Num7_Sample",
		"Num8_Sample",
		"Num9_Sample",
		"GOLD_Sample"
	};

	hModel_ = Model::Load("data/table.fbx");
	assert(hModel_ >= 0);

	Camera* pCamera = CreateGameObject<Camera>(this);
	pCamera->SetPosition(D3DXVECTOR3(0, 1.15f, -1.0f));

	for (int i = 0; i < RETIRE_PICT_MAX; i++)
	{
		hPict_[i] = Image::Load("Data/" + pictName[i] + ".png");
		assert(hPict_[i] >= 0);
	}

	//画像データを読み込み
	for (int i = 0; i < PICT_MAX; i++)
	{
		mPict_[i] = Image::Load("Data/" + name[i] + ".png");
		assert(mPict_[i] >= 0);
	}

	//画像データを読み込み
	for (int i = 0; i < CHIP_PICT_MAX; i++)
	{
		chipPict_[i] = Image::Load("Data/" + chipPictName[i] + ".png");
		assert(chipPict_[i] >= 0);
	}
}

//更新
void PlayScene::Update()
{
	//クラスの生成
	//Human human;
	//Npc npc;
	//Player player;

	//手札の開示(テスト用)
	//human_->ShowCard();
	//npc_->ShowCard();

	////判定
	//judge.handCheck(human.GetHand(), HUMAN);
	//judge.handCheck(npc.GetHand(), NPC);
	//judge.GameJudge();

	//所持金を計算する関数を呼ぶ
	//ユーザー
	humanChip_ = pHuman_->GetChip();
	DigitsCalculation(humanChip_, humanDigits_);

	//Npc
	npcChip_ = pNpc_->GetChip();
	DigitsCalculation(npcChip_, npcDigits_);

	std::cout << "--------------------------------------------" << std::endl;

	battlecnt_++;

	//とりあえず5回勝負したら終わり
	if ( battlecnt_ == 5 )
	{
		//リザルト画面を表示
	}

	///////////////////////////
	////リタイア関係の処理////
	//////////////////////////

	if (Input::IsKeyDown(DIK_ESCAPE))
	{
		//リタイアメッセージ表示用
		isDrawRetire_ = true;
	}

	if (isDrawRetire_)
	{
		//「いいえ」ボタン選択中左キー入力で
		if (Input::IsKeyDown(DIK_LEFT) && !(isRetire_))
		{
			//「はい」に移動
			isRetire_ = true;
		}

		//「はい」ボタン選択中右キー入力で
		else if(Input::IsKeyDown(DIK_RIGHT) && isRetire_)
		{
			//「いいえ」に移動
			isRetire_ = false;
		}

		//「いいえ」ボタン選択中エンターキー入力で
		if (Input::IsKeyDown(DIK_RETURN) && !(isRetire_))
		{
			isDrawRetire_ = false;
		}

		//「はい」ボタン選択中エンターキー入力で
		else if (Input::IsKeyDown(DIK_RETURN) && isRetire_)
		{
			//リザルト画面表示
			isDrawResult_ = true;
		}
	}

	//リザルト画面表示中にスペースキー入力で
	if (isDrawResult_ && Input::IsKeyDown(DIK_SPACE))
	{
		//難易度選択に戻る
		SceneManager::ChangeScene(SCENE_ID_SELECT_DIFFICULT);
	}
}

//描画
void PlayScene::Draw()
{
	//描画の処理//
	//テーブル
	Model::Draw(hModel_);


	//UI関係の描画（ターン、所持金の枠）
	for(int i = 0; i < PICT_MAX; i++)
	{
		Image::SetMatrix(mPict_[i], worldMatrix_);
		Image::Draw(mPict_[i]);
	}

	//ユーザーの単位
	Image::SetMatrix(chipPict_[GOLD], worldMatrix_);
	Image::Draw(chipPict_[GOLD]);

	D3DXMATRIX gMat;
	D3DXMatrixTranslation(&gMat, 0, NPC_CHIP_POS_Y, 0);

	//NPCの単位
	Image::SetMatrix(chipPict_[GOLD], gMat);
	Image::Draw(chipPict_[GOLD]);


	//所持金を桁毎に描画
	for (int i = 0; i < CHIP_DIGITS_MAX; i++)
	{
		for (int j = 0; j < ( CHIP_PICT_MAX - 1 ); j++)
		{
			if (humanDigits_[i] == j)
			{
				D3DXMATRIX mat;

				//描画するか
				bool isDraw = false;

				switch (i)
				{
					case A_DIGIT:
						D3DXMatrixTranslation(&mat, CHIP_POS_X, 0, 0);

						//描画する
						isDraw = true;

						break;

					case DOUBLE_DIGITS:
						D3DXMatrixTranslation(&mat, CHIP_POS_X * 2, 0, 0);

						//２桁目が0以外の時
						if (humanDigits_[i] != NUM0_SAMPLE)
						{
							//描画する
							isDraw = true;
						}
						//２桁目が0の時、３桁目が0以外の時は
						else if (humanDigits_[i + 1] != NUM0_SAMPLE)
						{
							//描画する
							isDraw = true;
						}
						break;

					case THREE_DIGITS:
						D3DXMatrixTranslation(&mat, CHIP_POS_X * 3, 0, 0);

						//数字が0以外なら
						if (j != NUM0_SAMPLE)
						{
							//描画する
							isDraw = true;
						}
						break;
				}

				//描画するなら
				if (isDraw)
				{
					Image::SetMatrix(chipPict_[j], mat);
					Image::Draw(chipPict_[j]);
				}
			}

			if (npcDigits_[i] == j)
			{
				D3DXMATRIX mat;

				//描画するか
				bool isDraw = false;

				switch (i)
				{
					case A_DIGIT:
						D3DXMatrixTranslation(&mat, CHIP_POS_X, NPC_CHIP_POS_Y, 0);

						//描画する
						isDraw = true;

						break;

					case DOUBLE_DIGITS:
						D3DXMatrixTranslation(&mat, CHIP_POS_X * 2, NPC_CHIP_POS_Y, 0);

						//２桁目が0以外の時
						if (npcDigits_[i] != NUM0_SAMPLE)
						{
							//描画する
							isDraw = true;
						}
						//２桁目が0の時、３桁目が0以外の時は
						else if (npcDigits_[i + 1] != NUM0_SAMPLE)
						{
							//描画する
							isDraw = true;
						}
						break;

					case THREE_DIGITS:
						D3DXMatrixTranslation(&mat, CHIP_POS_X * 3, NPC_CHIP_POS_Y, 0);

						//数字が0以外なら
						if (j != NUM0_SAMPLE)
						{
							//描画する
							isDraw = true;
						}
						break;
				}

				//描画するなら
				if (isDraw)
				{
					Image::SetMatrix(chipPict_[j], mat);
					Image::Draw(chipPict_[j]);
				}
			}
		}
	}

	//リタイア画面の描画
	if (isDrawRetire_)
	{
		//はい
		if (isRetire_)
		{
			Image::SetMatrix(hPict_[RETIRE], worldMatrix_);
			Image::Draw(hPict_[RETIRE]);
		}

		//いいえ
		else
		{
			Image::SetMatrix(hPict_[NOT_RETIRE], worldMatrix_);
			Image::Draw(hPict_[NOT_RETIRE]);
		}
	}

	//リザルト画面の描画
	if (isDrawResult_)
	{
		Image::SetMatrix(hPict_[RESULT], worldMatrix_);
		Image::Draw(hPict_[RESULT]);
	}
}

//開放
void PlayScene::Release()
{
}

bool PlayScene::GetIsDrawRetire()
{
	return isDrawRetire_;
}

//金額計算
void PlayScene::DigitsCalculation(int chip, int* pDigits)
{
	for (int i = 0; i < CHIP_DIGITS_MAX; i++)
	{
		switch (i)
		{
			case A_DIGIT:
				//１桁目を算出
				pDigits[i] = (chip % 10);
				break;

			case DOUBLE_DIGITS:
				//２桁目を算出
				pDigits[i] = ((chip / 10) % 10);
				break;

			case THREE_DIGITS:
				//３桁目算出
				pDigits[i] = ( chip / 100 );
				break;
		}
	}
}