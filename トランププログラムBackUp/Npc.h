#pragma once
#include "Player.h"
class Npc :public Player
{
private:

	//int hModel_;    //モデル番号
public:
	Npc(IGameObject * parent);
	~Npc();

	void RegistHand();

	void ShowCard();

	std::vector<Hand> GetHand();

	std::vector<Hand> NPCCard_;
	std::vector<Hand>::iterator NPCitr_;

	//仮置き交換手札選択フラグ
	bool tradeFlg_;
	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;
};

