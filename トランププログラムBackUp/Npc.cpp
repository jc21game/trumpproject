#include "Npc.h"
#include "Human.h"

#define rotateSpeed 3.0f

const int INITIAL_CHIP = 0;

Npc::Npc(IGameObject* parent) :Player(parent), NPCCard_(HAND_MAX), NPCitr_(NPCCard_.begin()), tradeFlg_(true)
{
	//カードを配当
	//これではコンストラクタ呼ばれているから駄目だな...
	Human *pHuman = new Human(parent);
	bool isDuplication_ = false;
	int cnt = 0;

	shufflehand_.erase(shufflehand_.begin(), shufflehand_.begin() + 5);
	shuffleitr_ = shufflehand_.begin();
	pHuman->Humanitr_ = pHuman->HumanCard_.begin();

	//Humanとカードが被らないようにカードを配布
	while (cnt != 5)
	{
		//山札から持ってきたカードの絵柄が相手も持っていたら弾く
		if (shuffleitr_->modelname == pHuman->Humanitr_->modelname)
		{
			isDuplication_ = true;
			++shuffleitr_;
			break;
		}

		//持ってなかったらvectorの前に入れる
		if (isDuplication_ != true)
		{
			NPCitr_->mark = shuffleitr_->mark;
			NPCitr_->num = shuffleitr_->num;
			NPCitr_->modelname = shuffleitr_->modelname;
			cnt++;
			++pHuman->Humanitr_;
			++shuffleitr_;
			++NPCitr_;
		}
		isDuplication_ = false;
	}
	shufflehand_.erase(shufflehand_.begin(), shufflehand_.begin() + 5);
	delete( pHuman );
}


Npc::~Npc()
{
}

void Npc::ShowCard()
{
	//カードを表示
	shuffleitr_ = shufflehand_.begin();
	NPCitr_ = NPCCard_.begin();
	std::cout << "NPCのカード : ";
	for (int i = 0; i < HAND_MAX; i++)
	{
		std::cout << "[" << NPCitr_->mark << ":" << NPCitr_->num << "] ";
		++shuffleitr_;
		++NPCitr_;
	}
	std::cout << std::endl;
}

std::vector<Hand> Npc::GetHand()
{
	return NPCCard_;
}

void Npc::Initialize()
{
	chip_ = INITIAL_CHIP;

	int i = 0;

	//モデル配列にカードの絵柄を入れる
	for (auto Roopitr = NPCCard_.begin(); Roopitr != NPCCard_.end(); ++Roopitr )
	{
		hModel_[i] = Model::Load(Roopitr->modelname);
		i++;
	}

	//最後にPlayerのモデルを入れる
	hModel_[i] = Model::Load("Data/Player.fbx");
	assert(hModel_ >= 0);

	NPCitr_ = NPCCard_.begin();
}

void Npc::Update()
{
	/*static int g;
	g++;

	if ((g % 60) == 0)
	{
		if (10 <= chip_)
		{
			if (chip_ < 100)
			{
				chip_ += 10;
			}
		}
		else
		{
			chip_ += 1;
		}
	}*/

	if (turn_)
	{
		srand(time(NULL));
		//どのカード交換するか
		if (tradeFlg_)
		{
			for (/*int i = 0; i < HAND_MAX; i++*/ auto Roopitr = NPCCard_.begin(); Roopitr != NPCCard_.end(); ++Roopitr)
			{
				if (rand() % 2)
				{
					Roopitr->status = CHECK;
				}
			}
			tradeFlg_ = false;
		}

		//カード交換開始
		for (/*int i = 0; i < HAND_MAX; i++*/ auto Roopitr = NPCCard_.begin(); Roopitr != NPCCard_.end(); ++Roopitr)
		{
			if (Roopitr->yPosition >= 1.5f)
			{
				if (Roopitr->status == CHECK)
				{
					Roopitr->status = MOVE_OUT;
				}
			}
		}


		//各カードのポジションの設定
		for (/*int i = 0; i < HAND_MAX; i++*/ auto Roopitr = NPCCard_.begin(); Roopitr != NPCCard_.end(); ++Roopitr)
		{
			switch (NPCitr_->status)
			{
			case UNCHECK:
			case hairu:
				if (Roopitr->yPosition > 1.2f)
				{
					Roopitr->yPosition -= 0.05f;

					if (Roopitr->yPosition <= 1.2f)
					{
						Roopitr->status = STOP;
					}
				}
				break;
			case CHECK:
				Roopitr->yPosition += 0.05f;
				if (Roopitr->yPosition >= 1.5f)
				{
					Roopitr->yPosition = 1.5f;
				}
				break;
			case MOVE_OUT:
				if (Roopitr->xPosition < 1.5f)
				{
					Roopitr->xPosition += 0.05f;
					if (Roopitr->xPosition >= 1.5f)
					{
						//ここでカードを交換するみたい

						//カードのモデルを呼びなおす
						//hModel_[i] = Model::Load("Data/CardModels/Club2.fbx");
						Roopitr->status = MOVE_IN;
					}
				}
				break;
			case MOVE_IN:
				if (Roopitr->xPosition >= -0.6f)
				{
					Roopitr->xPosition -= 0.05f;
					if (Roopitr->xPosition <= -0.6f)
					{
						Roopitr->status = hairu;
					}
				}
				break;
			}
		}

		for (/*int i = 0; i < HAND_MAX; i++*/ auto Roopitr = NPCCard_.begin(); Roopitr != NPCCard_.end(); ++Roopitr)
		{
			if (Roopitr->status != STOP)
			{
				allStopFlg_ = false;
				break;
			}
			allStopFlg_ = true;
		}

		//カード回転(確認)
		if (allStopFlg_)
		{
			//openFlg_ = true;
		}
		if (allStopFlg_)
		{
			for (/*int i = 0; i < HAND_MAX; i++*/ auto Roopitr = NPCCard_.begin(); Roopitr != NPCCard_.end(); ++Roopitr)
			{
				if (0 < Roopitr->rotateY)
				{
					Roopitr->rotateY -= rotateSpeed;
				}
			}
		}
	}
}

void Npc::Draw()
{

	D3DXMATRIX cardMat[HAND_MAX];
	D3DXMATRIX tMat, rXMat, rYMat;

	int i = 0;

	for (/*int i = 0; i < HAND_MAX; i++*/ auto Roopitr = NPCCard_.begin(); Roopitr != NPCCard_.end(); ++Roopitr)
	{
		if (allStopFlg_)
		{
			//回転
			/*if (i < 5)
			{*/
			//奥カード
			D3DXMatrixTranslation(&tMat, (-0.6f + (i * 0.3f)), 1.2f, 0.5f);
			D3DXMatrixRotationX(&rXMat, D3DXToRadian(-90));
			D3DXMatrixRotationY(&rYMat, D3DXToRadian(Roopitr->rotateY));
			cardMat[i] = rXMat * rYMat * tMat;
			//}
			i++;
		}
		//裏向き
		else
		{
			/*if (i < 5)
			{*/
			switch (Roopitr->status)
			{
				//縦移動
			case CHECK:
			case UNCHECK:
			case hairu:
				D3DXMatrixTranslation(&tMat, (Roopitr->xPosition + (i * 0.3f)), Roopitr->yPosition, 0.5f);
				D3DXMatrixRotationX(&rXMat, D3DXToRadian(-90));
				D3DXMatrixRotationY(&rYMat, D3DXToRadian(Roopitr->rotateY));
				cardMat[i] = rXMat * rYMat * tMat;
				break;
				//横移動
			case MOVE_OUT:
			case MOVE_IN:
				D3DXMatrixTranslation(&tMat, (Roopitr->xPosition + (i * 0.3f)), 1.55f, 0.5f);
				D3DXMatrixRotationX(&rXMat, D3DXToRadian(-90));
				D3DXMatrixRotationY(&rYMat, D3DXToRadian(Roopitr->rotateY));
				cardMat[i] = rXMat * rYMat * tMat;
				break;
				//停止状態
			default:
				D3DXMatrixTranslation(&tMat, (-0.6f + (i * 0.3f)), 1.2f, 0.5f);
				D3DXMatrixRotationX(&rXMat, D3DXToRadian(-90));
				D3DXMatrixRotationY(&rYMat, D3DXToRadian(Roopitr->rotateY));
				cardMat[i] = rXMat * rYMat * tMat;
				break;
				//}
				//	D3DXMATRIX tMat, rXMat, rYMat;
				//	//奥カード
				//	D3DXMatrixTranslation(&tMat, (-0.6f + (i * 0.3f)), 1.2f, 0.5f);
				//	D3DXMatrixRotationX(&rXMat, D3DXToRadian(-90));
				//	D3DXMatrixRotationY(&rYMat, D3DXToRadian(Roopitr->rotateY));
				//	cardMat[i] = rXMat * rYMat * tMat;
				//}
			}
			i++;
		}
	}

	for (int i = 0; i < 6; i++)
	{
		if (i < HAND_MAX)
		{
			Model::SetMatrix(hModel_[i], cardMat[i]);
		}
		else
		{
			Model::SetMatrix(hModel_[i], worldMatrix_);
		}
	}
	for (int i = 0; i < 6; i++)
	{
		Model::Draw(hModel_[i]);
	}
}

void Npc::Release()
{
}
