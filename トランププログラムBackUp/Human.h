#pragma once
#include "Player.h"

class Human :public Player
{
private:

	std::vector<Hand> GetHand();


	//Update処理前の状態を記憶しておくための変数
	std::vector<Hand> preHandState_;

	//交換終了か
	bool changeEndFlg_;
public:
	Human(IGameObject* parent);
	~Human();

	std::vector<Hand> HumanCard_;
	std::vector<Hand>::iterator Humanitr_;

	void RegistHand();

	void ShowCard();

	void ChangeHand(std::vector<Hand>::iterator changecard );

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;
};