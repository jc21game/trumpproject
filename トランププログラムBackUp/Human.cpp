#include "Human.h"
#include "Npc.h"

const int INITIAL_CHIP = 100;

Human::Human(IGameObject* parent) :Player(parent),  HumanCard_(HAND_MAX),preHandState_(HAND_MAX), Humanitr_(HumanCard_.begin()), changeEndFlg_(false)
{
	shuffleitr_ = shufflehand_.begin();
	for (int i = 0; i < HAND_MAX; i++)
	{
		Humanitr_->mark = shuffleitr_->mark;
		Humanitr_->num = shuffleitr_->num;
		Humanitr_->modelname = shuffleitr_->modelname;
		++shuffleitr_;
		++Humanitr_;
	}
	shufflehand_.erase(shufflehand_.begin(), shufflehand_.begin() + HAND_MAX);
}


Human::~Human()
{
}

void Human::ShowCard()
{
	//カード開示
	shuffleitr_ = shufflehand_.begin();
	Humanitr_ = HumanCard_.begin();
	std::cout << "Playerのカード : ";
	for (int i = 0; i < HAND_MAX; i++)
	{
		std::cout << "[" << Humanitr_->mark << ":" << Humanitr_->num << "] ";
		++shuffleitr_;
		++Humanitr_;
	}
	std::cout << std::endl;
}

//交換用の関数
void Human::ChangeHand(std::vector<Hand>::iterator changecard )
{


}

std::vector<Hand> Human::GetHand()
{
	return HumanCard_;
}

void Human::Initialize()
{
	chip_ = INITIAL_CHIP;

	int i = 0;

	//モデル配列にカードの絵柄を入れる
	for (auto Roopitr = HumanCard_.begin(); Roopitr != HumanCard_.end(); ++Roopitr)
	{
		hModel_[i] = Model::Load(Roopitr->modelname);
		i++;
	}

	Humanitr_ = HumanCard_.begin();
}

void Human::Update()
{
	/*static int g;
	g++;

	if ((g % 60) == 0)
	{
		if (chip_ <= 10)
		{
			if (0 < chip_)
			{
				chip_ -= 1;
			}
		}
		else
		{
			chip_ -= 10;
		}
	}*/

	//プレイシーンで「リザルト画面を表示しているかどうか」の情報取得
	if (((PlayScene*)pParent_)->GetIsDrawRetire())
	{
		//表示中なら
		for (auto Roopitr = HumanCard_.begin(); Roopitr != HumanCard_.end(); ++Roopitr)
		{
			Roopitr->status = LOCK;
		}
	}

	//強制的に「STOP」にするから戻っちゃうみたい...
	else
	{
		auto PreRoopitr = preHandState_.begin();
		for (auto Roopitr = HumanCard_.begin(); Roopitr != HumanCard_.end(); ++Roopitr)
		{
			Roopitr->status = PreRoopitr->status;
			++PreRoopitr;
		}
	}

	if (Humanitr_->status != LOCK)
	{
		if (!turn_)
		{
			if (!changeEndFlg_)
			{
				//選択カード移動
				if (Input::IsKeyDown(DIK_RIGHT) && (handnum_ < 4) && (Humanitr_ != HumanCard_.end()))
				{
					handnum_++;
					++Humanitr_;
				}

				if (Input::IsKeyDown(DIK_LEFT) && (handnum_ > 0) && (Humanitr_ != HumanCard_.begin()))
				{
					handnum_--;
					--Humanitr_;
				}

				//カード選択
				if (Input::IsKeyDown(DIK_SPACE))
				{
					if (Humanitr_->status == CHECK)
					{
						Humanitr_->status = UNCHECK;
					}

					else
					{
						//これでカードが選ばれた状態になる
						Humanitr_->status = CHECK;
					}
				}

				if (Input::IsKeyDown(DIK_RETURN))
				{
					for (auto Roopitr = HumanCard_.begin(); Roopitr != HumanCard_.end(); ++Roopitr)
					{
						if (Roopitr->status == CHECK)
						{
							Roopitr->status = MOVE_OUT;
							changeEndFlg_ = true;
						}
					}
				}

				if (Input::IsKeyDown(DIK_F))
				{
					changeEndFlg_ = true;
				}
			}


			auto PreRoopitr = preHandState_.begin();
			//各カードのポジションの設定
			for (auto Roopitr = HumanCard_.begin(); Roopitr != HumanCard_.end(); ++Roopitr)
			{
				switch (Roopitr->status)
				{
				case UNCHECK:
				case hairu:
					if (Roopitr->yPosition > 1.2f)
					{
						Roopitr->yPosition -= 0.05f;
						if (Roopitr->yPosition <= 1.2f)
						{
							Roopitr->status = STOP;
						}
					}
					break;
				case CHECK:
					Roopitr->yPosition += 0.05f;
					if (Roopitr->yPosition >= 1.5f)
					{
						Roopitr->yPosition = 1.5f;
					}
					break;
				case MOVE_OUT:
					if (Roopitr->xPosition < 1.5f)
					{
						Roopitr->xPosition += 0.05f;
						if (Roopitr->xPosition >= 1.5f)
						{
							//ここでカードを交換するみたい

							//カードのモデルを呼びなおす
							ChangeHand( Roopitr );
							Roopitr->status = MOVE_IN;
						}
					}
					break;
				case MOVE_IN:
					if (Roopitr->xPosition >= -0.6f)
					{
						Roopitr->xPosition -= 0.05f;
						if (Roopitr->xPosition <= -0.6f)
						{
							Roopitr->status = hairu;
						}
					}
					break;
				}
				//更新前最後のカードの状態を記憶
				PreRoopitr->status = Roopitr->status;
				++PreRoopitr;
			}
		}

		for (auto Roopitr = HumanCard_.begin(); Roopitr != HumanCard_.end(); ++Roopitr)
		{
			if (Roopitr->status != STOP)
			{
				allStopFlg_ = false;
				break;
			}
			allStopFlg_ = true;
		}

		//賭け
		if (Input::IsKeyDown(DIK_1))
		{
			chip_ -= 5;

			isBet_ = true;
		}
		if (Input::IsKeyDown(DIK_2))
		{
			chip_ -= 10;

			isBet_ = true;
		}
		if (Input::IsKeyDown(DIK_3))
		{
			chip_ -= 20;

			isBet_ = true;

		}
		if (Input::IsKeyDown(DIK_4))
		{
			chip_ -= 50;

			isBet_ = true;
		}
		if (Input::IsKeyDown(DIK_5))
		{
			chip_ -= 100;

			isBet_ = true;
		}

		if (changeEndFlg_ && allStopFlg_ && isBet_)
		{
			turn_ = NPC_TURN;
		}
	}
}

void Human::Draw()
{

	D3DXMATRIX cardMat[5];
	int i = 0;

	for (auto Roopitr = HumanCard_.begin(); Roopitr != HumanCard_.end(); ++Roopitr)
	{
		D3DXMATRIX tMat, rXMat, rYMat;
		switch (Roopitr->status)
		{
			//縦移動
		case CHECK:
		case UNCHECK:
		case hairu:
			D3DXMatrixTranslation(&tMat, (Roopitr->xPosition + (i * 0.3f)), Roopitr->yPosition + 0.05f, -1.0f);
			D3DXMatrixRotationX(&rXMat, D3DXToRadian(-90));
			cardMat[i] = rXMat * tMat;
			break;

			//横移動
		case MOVE_OUT:
		case MOVE_IN:
			D3DXMatrixTranslation(&tMat, (Roopitr->xPosition + (i * 0.3f)), 1.55f, -1.0f);
			D3DXMatrixRotationX(&rXMat, D3DXToRadian(-90));
			cardMat[i] = rXMat * tMat;
			break;
			//停止状態
		default:
			if (changeEndFlg_)
			{
				D3DXMatrixTranslation(&tMat, (-0.6f + (i * 0.3f)), 1.2f, -1.0f);
				D3DXMatrixRotationX(&rXMat, D3DXToRadian(-90));
				cardMat[i] = rXMat * tMat;
			}

			else if (handnum_ == i)
			{
				D3DXMatrixTranslation(&tMat, (-0.6f + (i * 0.3f)), 1.25f, -1.0f);
				D3DXMatrixRotationX(&rXMat, D3DXToRadian(-90));
				cardMat[i] = rXMat * tMat;
			}
			else
			{
				D3DXMatrixTranslation(&tMat, (-0.6f + (i * 0.3f)), 1.2f, -1.0f);
				D3DXMatrixRotationX(&rXMat, D3DXToRadian(-90));
				cardMat[i] = rXMat * tMat;
			}
			break;
		}
		i++;
	}

	for (int i = 0; i < 5; i++)
	{
		Model::SetMatrix(hModel_[i], cardMat[i]);
	}
	for (int i = 0; i < 5; i++)
	{
		Model::Draw(hModel_[i]);
	}

	//Model::Draw(numPictModel_);
}

void Human::Release()
{
}