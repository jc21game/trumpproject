#pragma once
#include "Engine/global.h"

class Human;
class Npc;

//プレイシーンを管理するクラス
class PlayScene : public IGameObject
{
private:
	
	enum
	{
		RETIRE,			//はい( リタイアする )
		NOT_RETIRE,		//いいえ( リタイアしない )
		RESULT,			//リザルト画面
		RETIRE_PICT_MAX,
	};

	enum
	{
		TURN,
		HUMAN_BET_CHIP_TOTAL,
		NPC_BET_CHIP_TOTAL,
		PICT_MAX
	};

	//所持金関係の画像
	enum CHIP
	{
		NUM0_SAMPLE,
		NUM1_SAMPLE,
		NUM2_SAMPLE,
		NUM3_SAMPLE,
		NUM4_SAMPLE,
		NUM5_SAMPLE,
		NUM6_SAMPLE,
		NUM7_SAMPLE,
		NUM8_SAMPLE,
		NUM9_SAMPLE,
		GOLD,
		CHIP_PICT_MAX
	};

	//所持金の桁数用
	enum CHIP_DIGITS
	{
		A_DIGIT,		//１桁目
		DOUBLE_DIGITS,	//２桁目
		THREE_DIGITS,	//３桁目
		CHIP_DIGITS_MAX	//桁数上限
	};

	int mPict_[PICT_MAX];				//画像番号
	int battlecnt_;						//勝負数はこれで管理
	int hPict_[RETIRE_PICT_MAX];		//画像データ
	int chipPict_[CHIP_PICT_MAX];			//数字画像
	int hModel_;						//テーブル用モデル番号
	int humanChip_;						//所持金
	int npcChip_;						//所持金
	int humanDigits_[CHIP_DIGITS_MAX];	//ユーザーの○桁の数字を格納する
	int npcDigits_[CHIP_DIGITS_MAX];	//NPC　○桁の数字を格納する
	bool isDrawRetire_;					//リタイア画面を表示するかどうか
	bool isRetire_;						//リタイア「はい」or「いいえ」
	bool isDrawResult_;					//リザルト画面を表示するかどうか

	//↓実験用
	float posX_ = 0.0f;
	D3DXMATRIX mat;

	Human* pHuman_;
	Npc* pNpc_;

public:
	//コンストラクタ
	//引数：parent  親オブジェクト（SceneManager）
	PlayScene(IGameObject* parent);

	//初期化
	void Initialize() override;

	//モデル、画像のデータロード
	void LoadData();

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;

	//リザルト画面表示中かどうかのフラグのゲッター
	bool GetIsDrawRetire();

	//所持金の桁を計算する
	void DigitsCalculation(int chip, int* pDigits);
};