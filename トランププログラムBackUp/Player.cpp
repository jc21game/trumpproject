#include "Player.h"
#include "Engine/Model.h"

const int CARD_MARK_MAX = 4;
const int CARD_NUMBER_MAX = 13;
const int CARD_SUM = 52;

int Player::turn_ = HUMAN_TURN;

Player::Player(IGameObject* parent) :
	IGameObject(parent, "Player"),
	hand_(CARD_SUM), handitr_(hand_.begin()),
	shufflehand_(CARD_SUM), shuffleitr_(shufflehand_.begin()),
	isDuplication_(false), allStopFlg_(true), isBet_(false)
{
	//画像データの名前
	std::string name[CARD_MARK_MAX] =
	{
		"Club",
		"Diamond",
		"Spade",
		"Heart"
	};

	//カードのナンバー
	std::string number[CARD_NUMBER_MAX] =
	{
		"1","2","3","4","5","6","7","8","9","10","11","12","13"
	};

	for (int i = 0; i < CARD_MARK_MAX; i++)
	{
		//カードを初期化
		for (int j = 0; j < CARD_NUMBER_MAX; j++)
		{
			handitr_->mark = i;
			handitr_->num = j;
			handitr_->modelname = ("Data/CardModels/" + name[i] + number[j] + ".fbx");
			++handitr_;
		}
	}

	//シャッフルの処理
	for (handitr_ = hand_.begin(); handitr_ != hand_.end(); ++handitr_)
	{
		//全シャッフル用のカードから無作為に番地を取り、そこに山札のカードを仕込む
		while (isDuplication_ == false)
		{
			shuffleitr_ = shufflehand_.begin() + rand() % CARD_SUM;

			//ランダムにとった番地にすでに値が入っていないときにのみ
			if (shuffleitr_->mark == -1)
			{
				shuffleitr_->mark = handitr_->mark;
				shuffleitr_->num = handitr_->num;
				shuffleitr_->modelname = handitr_->modelname;
				isDuplication_ = true;
			}
		}
		isDuplication_ = false;
	}
}

Player::~Player()
{
}

void Player::Initialize()
{
}

void Player::Update()
{
}

void Player::Draw()
{
}

void Player::Release()
{
}