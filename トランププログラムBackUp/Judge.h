#pragma once
#include "Player.h"
enum PAIR {
	NO_PAIR,
	ONE_PAIR,
	TWO_PAIR,
	THREE_CARD,
	STRAIGHT,
	FLASH,
	FULLHOUSE,
	FOUR_CARD,
	STRAIGHT_FLASH,
	ROYAL_STRAIGHT_FLASH
};

class Judge
{
private:
	int pairPoint_;
	int playerPokerHand_;
	int npcPokerHand_;
	int playerMaxNum_;
	int npcMaxNum_;

	bool reWrightFlg_;
	bool straightFlg_;
	bool flashFlg_;
	bool rsfFlg_;

public:
	Judge();
	~Judge();

	//手札を見て何の役か判定する
	//戻り値:なし
	//引数:なし
	void handCheck(std::vector<Hand> hand, int who);

	//各プレイヤーの役を登録する
	//戻り値:なし
	//引数:なし
	void Regist(int who);

	//1回の対戦でどちらが勝ったかを判定する
	//戻り値:なし
	//引数:なし
	void GameJudge();
};



//同じ数値が見つかったら1点ずつ増やしていく
//↓		↓点数
//ブタ	 〇　0
//1ペア  〇　1
//2ペア  〇　2
//3カード〇　3
//ST	 ×　0
//FL	 ×　0
//FULL	 ×　4
//4カード×　6
//SF	 ×　0
//RSF	 ×　0



//ST->差の絶対値の和が20?
//例>
//A B C D E
//1 2 3 4 5
//A,Bの差 A,Cの差 A,Dの差, A,Eの差
//B,Cの差 B,Dの差 B,Eの差
//C,Dの差 C,Eの差
//D,Eの差
//差は絶対値にして、上記の差の和を求めると必ず20になる？
//20を超えてしまったらSTの可能性を排除し
//以降の確認時に差の絶対値の計算を行わない


//FL->確認時にマークを見て一つでもマークが異なっていたらFLの可能性を排除し
//以降の確認時にマークの確認を行わない

//FULL->点数が4点の時に登録する

//4カード->点数が6点の時に登録する

//SF->STとFLが同時に成立している場合に登録

//RSF->カードの数値を見て対象の数値でなければ可能性を排除する
//対象の数値かつFLが同時に成立したときに登録




//フルハウス同士の時は3枚のほうを比較して勝敗判定する

//ストレート、フラッシュに関してはとにかく13に近い数を持っているものを勝ちとする