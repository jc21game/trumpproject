#include "Fbx.h"
#include "Direct3D.h"

#pragma comment(lib,"libfbxsdk-mt.lib")


Fbx::Fbx() : pVertexBuffer_(nullptr),
ppIndexBuffer_(nullptr),
pTexture_(nullptr),
pManager_(nullptr),
pImporter_(nullptr),
pScene_(nullptr),
pmaterial_(nullptr),
materialCount_(0),
vertexCount_(0),
polygonCount_(0),
indexCount_(0),
polygonCountOfMaterial_(nullptr)
{
}


Fbx::~Fbx()
{
	//メモリ開放
	SAFE_DELETE_ARRAY(polygonCountOfMaterial_);
	SAFE_DELETE(pmaterial_);

	//複数のインデックスバッファを作ったため1個1個Releaseしたあと、
	//配列もdeleteしないといけない
	for (int i = 0; i < materialCount_; i++)
	{
		SAFE_RELEASE(ppIndexBuffer_[i]);
		SAFE_RELEASE(pTexture_[i]);
	}
	SAFE_DELETE_ARRAY(ppIndexBuffer_);
	SAFE_DELETE_ARRAY(pTexture_);
	SAFE_RELEASE(pVertexBuffer_);

	//これらはマネージャーのDestory関数ですべて開放できる
	pScene_->Destroy();
	pManager_->Destroy();
}

void Fbx::Load(const char* fileName)
{
	pManager_ = FbxManager::Create();
	pImporter_ = FbxImporter::Create(pManager_, "");
	pScene_ = FbxScene::Create(pManager_, "");

	//インポーターを使ってFBXファイルを開く
	pImporter_->Initialize(fileName);

	//その開いたファイルをシーンに渡す
	pImporter_->Import(pScene_);

	//ここでインポーターの出番は終了なので開放処理
	pImporter_->Destroy();

	//現在見ているカレントディレクトリをdefaultCurrentdirに入れる
	char defaultCurrentDir[MAX_PATH];
	GetCurrentDirectory(MAX_PATH, defaultCurrentDir);

	char dir[MAX_PATH];
	_splitpath_s(fileName, nullptr, 0, dir, MAX_PATH, nullptr, 0, nullptr, 0);
	//これ以降はこのカレントディレクトリを見てね
	SetCurrentDirectory(dir);

	//rootNodeにルートノードの情報を入れる
	FbxNode* rootNode = pScene_->GetRootNode();

	//ルートノードの子の数を調べる
	int childCount = rootNode->GetChildCount();

	for (int i = 0; childCount > i; i++)
	{
		//ノードの内容をチェック
		CheckNode(rootNode->GetChild(i));
	}

	//カレントディレクトリを元に戻しておく
	SetCurrentDirectory(defaultCurrentDir);
}

void Fbx::CheckNode(FbxNode * pNode)
{
	//受けとったノード属性を取得し、そのタイプがFbxNodeAttribute::EMeshだったら、
	//このノードはメッシュ情報となる
	FbxNodeAttribute* attr = pNode->GetNodeAttribute();
	if (attr->GetAttributeType() == FbxNodeAttribute::eMesh)
	{
		//メッシュノードだった
		materialCount_ = pNode->GetMaterialCount();
		pmaterial_ = new D3DMATERIAL9[materialCount_];
		pTexture_ = new LPDIRECT3DTEXTURE9[materialCount_];

		//ここらのはすべてダブルポインタの中身を初期化するもの
		ZeroMemory(pTexture_, sizeof(LPDIRECT3DTEXTURE9) * materialCount_);
		/*for(int i = 0; i < materialCount_; i++)
		{
			pTexture_[i] = nullptr;
		}*/

		//1つずつマテリアル情報を読み込んでいく
		for (int i = 0; i < materialCount_; i++)
		{
			FbxSurfaceLambert* lambert = (FbxSurfaceLambert*)pNode->GetMaterial(i);
			FbxDouble3 diffuse = lambert->Diffuse;	//Diffuse(ポリゴンの色)
			FbxDouble3 ambient = lambert->Ambient;	//Ambient(環境光)

			//これをmaterial_に入れていく(普通ここでは透明度は指定しない)
			ZeroMemory(&pmaterial_[i], sizeof(D3DMATERIAL9));

			pmaterial_[i].Diffuse.r = (float)diffuse[0];
			pmaterial_[i].Diffuse.g = (float)diffuse[1];
			pmaterial_[i].Diffuse.b = (float)diffuse[2];
			pmaterial_[i].Diffuse.a = 1.0f;

			pmaterial_[i].Ambient.r = (float)ambient[0];
			pmaterial_[i].Ambient.g = (float)ambient[1];
			pmaterial_[i].Ambient.b = (float)ambient[2];
			pmaterial_[i].Ambient.a = 1.0f;

			//設定されたテクスチャの情報を取得
			FbxProperty lProperty = pNode->GetMaterial(i)->FindProperty(FbxSurfaceMaterial::sDiffuse);

			//それに使われたファイルの情報を取得
			FbxFileTexture* textureFile = lProperty.GetSrcObject<FbxFileTexture>(0);

			if (textureFile == nullptr)
			{
				pTexture_[i] = nullptr;
			}
			else
			{
				const char* textureFileName = textureFile->GetFileName();

				char name[_MAX_FNAME];
				char ext[_MAX_EXT];
				_splitpath_s(textureFileName, nullptr, 0, nullptr, 0, name, _MAX_FNAME, ext, _MAX_EXT);

				//ファイル名を合体させる
				wsprintf(name, "%s%s", name, ext);

				//画像ファイルを読み込んでテクスチャを作る
				//0xcdcdcd...とかいうわけわからん値が入っていたら読み込めてない証拠
				D3DXCreateTextureFromFileEx(Direct3D::pDevice, name, 0, 0, 0, 0, D3DFMT_UNKNOWN,
					D3DPOOL_DEFAULT, D3DX_FILTER_NONE, D3DX_DEFAULT, 0, 0, 0, &pTexture_[i]);
				assert(pTexture_[i] != nullptr);
			}
		}

		CheckMesh(pNode->GetMesh());

	}

	//違ったら、その子供がメッシュ情報の可能性もあるため、
	//子の数を調べ、それぞれの子に対してもcheckNodeで調べる必要がある
	else
	{
		//メッシュ以外のデータだった
		int childCount = pNode->GetChildCount();
		for (int i = 0; childCount > i; i++)
		{
			//再帰
			CheckNode(pNode->GetChild(i));
		}
	}
}

void Fbx::CheckMesh(FbxMesh* pMesh)
{
	//専用の型のポインタを用意して頂点情報(先頭アドレス)を読み込む。
	FbxVector4* pVertexPos = pMesh->GetControlPoints();

	//こいつに頂点数を入れる
	vertexCount_ = pMesh->GetControlPointsCount();

	//そして頂点の数と同じサイズのVertex構造体型の配列を作成する
	Vertex* vertexList = new Vertex[vertexCount_];

	//ここでポリゴンとインデックスの数を取得する
	polygonCount_ = pMesh->GetPolygonCount();
	indexCount_ = pMesh->GetPolygonVertexCount();

	//二次元配列になっているvertexPosから、vertexListのメンバ変数posにデータをコピーする
	for (int i = 0; vertexCount_ > i; i++)
	{
		vertexList[i].pos.x = (float)pVertexPos[i][0];
		vertexList[i].pos.y = (float)pVertexPos[i][1];
		vertexList[i].pos.z = (float)pVertexPos[i][2];
	}

	//法線
	for (int i = 0; i < polygonCount_; i++)
	{
		//最初の頂点をstartIndexに入れる
		int startIndex = pMesh->GetPolygonVertexIndex(i);

		//3頂点分の処理をする
		for (int j = 0; j < 3; j++)
		{
			int index = pMesh->GetPolygonVertices()[startIndex + j];

			FbxVector4 Normal;
			//i番目のポリゴンの
			pMesh->GetPolygonVertexNormal(i, j, Normal);

			vertexList[index].normal = D3DXVECTOR3((float)Normal[0], (float)Normal[1], (float)Normal[2]);

			//UV座標を指定
			FbxVector2 uv = pMesh->GetLayer(0)->GetUVs()->GetDirectArray().GetAt(index);
			vertexList[index].uv = D3DXVECTOR2((float)uv.mData[0], (float)(1.0 - uv.mData[1]));
		}
	}
	//Quadクラスで使った方法と同じ、バーテックスバッファをロックするところまで行う
	Direct3D::pDevice->CreateVertexBuffer(sizeof(Vertex) *vertexCount_, 0,
		D3DFVF_XYZ | D3DFVF_NORMAL | D3DFVF_TEX1, D3DPOOL_MANAGED,
		&pVertexBuffer_, 0);
	assert(pVertexBuffer_ != nullptr);	//Createしたらassertを入れるようにしよう

	Vertex *vCopy;
	pVertexBuffer_->Lock(0, 0, (void**)&vCopy, 0);
	memcpy(vCopy, vertexList, sizeof(Vertex) *vertexCount_);
	pVertexBuffer_->Unlock();

	//deleteした後にしっかりnullptrを入れよう
	SAFE_DELETE_ARRAY(vertexList);

	//インデックスバッファを作成
	ppIndexBuffer_ = new IDirect3DIndexBuffer9*[materialCount_];
	polygonCountOfMaterial_ = new int[materialCount_];

	for (int i = 0; i < materialCount_; i++)
	{
		//インデックス情報を入れる配列
		int* indexList = new int[indexCount_];

		//indexlistにインデックス情報を入れる
		int count = 0;
		for (int polygon = 0; polygon < polygonCount_; polygon++)
		{
			//今見ているポリゴンの番号を確認し、それがi番目だったらIndexListに入れる
			int materialID = pMesh->GetLayer(0)->GetMaterials()->GetIndexArray().GetAt(polygon);
			if (materialID == i)
			{
				for (int vertex = 0; vertex < 3; vertex++)
				{
					//これでpolygon番目のvertex番目の頂点の番号を求められる
					indexList[count++] = pMesh->GetPolygonVertex(polygon, vertex);
				}
			}
		}
		//頂点数を3で割り、ポリゴン数とする
		polygonCountOfMaterial_[i] = count / 3;

		//Quadクラスで使った方法と同じ、バーテックスバッファをロックするところまで行う
		Direct3D::pDevice->CreateIndexBuffer(sizeof(int) * indexCount_, 0,
			D3DFMT_INDEX32, D3DPOOL_MANAGED, &ppIndexBuffer_[i], 0);
		assert(ppIndexBuffer_ != nullptr);	//Createしたらassertを入れるようにしよう

		DWORD *iCopy;
		ppIndexBuffer_[i]->Lock(0, 0, (void**)&iCopy, 0);
		memcpy(iCopy, indexList, sizeof(int) * indexCount_);
		ppIndexBuffer_[i]->Unlock();
		SAFE_DELETE_ARRAY(indexList);
	}
}

void Fbx::Draw(const D3DXMATRIX& matrix)
{
	//ワールド行列に受け取った行列を入れる
	Direct3D::pDevice->SetTransform(D3DTS_WORLD, &matrix);

	//表示したい頂点バッファとインデックスバッファを指定する
	Direct3D::pDevice->SetStreamSource(0, pVertexBuffer_, 0, sizeof(Vertex));

	//頂点データがどのような情報を持っているか指定する。
	//									↓これは論理和を表す(2進にしてどっちかが1なら1が立つアレ)
	Direct3D::pDevice->SetFVF(D3DFVF_XYZ | D3DFVF_NORMAL | D3DFVF_TEX1);

	for (int i = 0; i < materialCount_; i++)
	{
		Direct3D::pDevice->SetIndices(ppIndexBuffer_[i]);
		Direct3D::pDevice->SetMaterial(&pmaterial_[i]);

		//使いたいテクスチャを指定
		Direct3D::pDevice->SetTexture(0, pTexture_[i]);
		Direct3D::pDevice->DrawIndexedPrimitive(D3DPT_TRIANGLELIST, 0, 0, vertexCount_, 0, polygonCountOfMaterial_[i]);
	}
}