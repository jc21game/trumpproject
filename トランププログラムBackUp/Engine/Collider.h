#pragma once
#include "Global.h"

class Collider
{
	D3DXVECTOR3 center_;	//モデルの原点
	float		radius_;	//モデルの半径
	IGameObject* owner_;	//誰のコライダーなのか

public:
	Collider(D3DXVECTOR3 center, float radius,IGameObject* owner);
	~Collider();

	//当たっているか否か
	bool IsHit(Collider& target);
};