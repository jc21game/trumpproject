#include "Direct3D.h"

LPDIRECT3D9			Direct3D::pD3d = nullptr;	    //Direct3Dオブジェクト(ポインタ)
LPDIRECT3DDEVICE9	Direct3D::pDevice = nullptr;	//Direct3Dデバイスオブジェクト(ポインタ,画面に表示する時使う)

void Direct3D::Initilize(HWND hWnd)
{
	//Direct3Dオブジェクトの作成
	pD3d = Direct3DCreate9(D3D_SDK_VERSION);	// newでオブジェクトとして生成したいけど純粋仮想関数があるためnewできない
	assert(pD3d != nullptr);											// この場合は生成する用の関数がある(Create)


	//DIRECT3Dデバイスオブジェクトの作成
	D3DPRESENT_PARAMETERS d3dpp;	                //専用の構造体
	ZeroMemory(&d3dpp, sizeof(d3dpp));	          //中身を全部0にする
	d3dpp.BackBufferFormat = D3DFMT_UNKNOWN;
	d3dpp.BackBufferCount = 1;
	d3dpp.SwapEffect = D3DSWAPEFFECT_DISCARD;
	d3dpp.Windowed = TRUE;						//TRUE:ウィンドウに表示,FALSE:フルスクリーン
	d3dpp.EnableAutoDepthStencil = TRUE;
	d3dpp.AutoDepthStencilFormat = D3DFMT_D16;
	d3dpp.BackBufferWidth = g.screenWidth;
	d3dpp.BackBufferHeight = g.screenHeight;
	pD3d->CreateDevice(D3DADAPTER_DEFAULT, D3DDEVTYPE_HAL, hWnd,
		D3DCREATE_HARDWARE_VERTEXPROCESSING, &d3dpp, &pDevice);
	assert(pDevice != nullptr);

	//アルファブレンド(半透明表示の設定)
	pDevice->SetRenderState(D3DRS_ALPHABLENDENABLE, TRUE);
	pDevice->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_SRCALPHA);
	pDevice->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_INVSRCALPHA);

	//ライティング(Trueにしないと起動しないので注意)
	pDevice->SetRenderState(D3DRS_LIGHTING, TRUE);

	//ライトを設置
	D3DLIGHT9 lightState;
	ZeroMemory(&lightState, sizeof(lightState));	//zeromemoryは構造体の中身を全部0にするやつ
	lightState.Type = D3DLIGHT_DIRECTIONAL;			//ライトの種類(今回は平行光源)
	lightState.Direction = D3DXVECTOR3(1, -1, 1);	//光の向き

	//ライトの色(0 〜 1の値で選択する。普通は全部1)
	lightState.Diffuse.r = 1.0f;
	lightState.Diffuse.g = 1.0f;
	lightState.Diffuse.b = 1.0f;
	//lightState.Ambient.r = 1.0f;
	//lightState.Ambient.g = 1.0f;
	//lightState.Ambient.b = 1.0f;

	//ここでライトを作成
	//第一引数 ... ライトの番号
	pDevice->SetLight(0, &lightState);

	//ライトをONにする
	//第一引数 ... ライトの番号
	pDevice->LightEnable(0, TRUE);

	//カメラ
	D3DXMATRIX view;

	//D3DXMatrixLookAtLH ... ビュー行列を作成
	//第1引数 ... 行列
	//第2引数 ... カメラの視点位置
	//第3引数 ... カメラの焦点
	//第4引数 ... カメラの上方向ベクトル。(0, 1, 0)基本コレ
	D3DXMatrixLookAtLH(&view, &D3DXVECTOR3(0, 5, -10), &D3DXVECTOR3(0, 0, 0), &D3DXVECTOR3(0, 1, 0));

	//ビュー行列に変換
	pDevice->SetTransform(D3DTS_VIEW, &view);

	D3DXMATRIX proj;
	//D3DXMatrixPerspectiveFovLH ... プロジェクション行列を作成
	//第1引数 ... 行列
	//第2引数 ... 視野角(見える範囲)
	//第3引数 ... アスペクト比(矩形における長辺と短辺の比率。)
	//第4引数 ... 近クリッピング面。カメラの写す距離のはじめ(ある程度の距離はあったほうがいい)
	//第5引数 ... 遠クリッピング面。カメラの見える距離
	D3DXMatrixPerspectiveFovLH(&proj, D3DXToRadian(60), (float)g.screenWidth / g.screenHeight, 0.5f, 1000.0f);

	//プロジェクション行列に変換
	pDevice->SetTransform(D3DTS_PROJECTION, &proj);

}

void Direct3D::BeginDraw()
{
	//画面をクリア(毎フレーム)
	//D3DCOLOR_XRGB : RGBで色を変更
	pDevice->Clear(0, NULL, D3DCLEAR_TARGET | D3DCLEAR_ZBUFFER, D3DCOLOR_XRGB(0,0,128), 1.0f, 0);

	//描画開始
	//BeginScene : 描画処理開始時に必ず呼ぶ
	pDevice->BeginScene();
}

void Direct3D::EndDraw()
{
	//描画終了 : 描画処理終了時に必ず呼ぶ
	pDevice->EndScene();

	//スワップ
	pDevice->Present(NULL, NULL, NULL, NULL);
}

void Direct3D::Release()
{
	//開放処理
	//生成したのと逆順に開放
	//Release() : LP~型のオブジェクトを開放するとき専用の関数
	pDevice->Release();
	pD3d->Release();
}