#pragma once
#include "Global.h"

//ポリゴンを管理するクラス
class Quad
{
protected:

	//ここの順番はフラグの定義順なので注意
	struct Vertex
	{

		D3DXVECTOR3 pos;

		//法線の情報(法線って英語でnormalらしい)
		D3DXVECTOR3 normal;

		//UV座標の情報
		D3DXVECTOR2 uv;
	};

public:
	//バーテックスバッファ ... 用意した4つ分の頂点を入れるメモリを用意
	LPDIRECT3DVERTEXBUFFER9 pVertexBuffer_;

	//インデックスバッファ ... 
	LPDIRECT3DINDEXBUFFER9 pIndexBuffer_;

	//テクスチャ
	LPDIRECT3DTEXTURE9 pTexture_;

	//マテリアルを入れる変数を追加する
	//※マテリアル ... CG用語におけるポリゴンの質感のこと
	D3DMATERIAL9         material_;
	
	Quad();
	~Quad();

	virtual void Load( const char* pPicture );
	virtual void VertexBuffer(Vertex *vertexList);
	virtual void IndexBuffer(Vertex *indexList);
	virtual void Draw( const D3DXMATRIX& matrix );
	virtual void SetDraw(const D3DXMATRIX& matrix);
};