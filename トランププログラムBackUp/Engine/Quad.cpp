#include "Quad.h"
#include "Direct3D.h"

//動的メモリ確保
Quad::Quad() : pVertexBuffer_(nullptr),
pIndexBuffer_(nullptr),
pTexture_(nullptr),
material_({ 0 })			//構造体なのでこの方法で0クリアが可能
{
}

Quad::~Quad()
{
	//メモリ開放
	SAFE_RELEASE(pTexture_);
	SAFE_RELEASE(pIndexBuffer_);
	SAFE_RELEASE(pVertexBuffer_);
}

void Quad::Load(const char* pPicture)
{
	//4つ角分の情報
	Vertex vertexList[] = {
		//			x   y  z			法線を指定			UV座標を指定		      L    G    B   不透明度(0で透明 , 255で不透明)
		D3DXVECTOR3(-1, 1, 0),   D3DXVECTOR3(0, 0, -1), D3DXVECTOR2(0,0),//D3DXCOLOR(255, 255, 255,0),
	    D3DXVECTOR3(1, 1, 0),    D3DXVECTOR3(0, 0, -1), D3DXVECTOR2(1,0),//D3DXCOLOR(255, 0, 255, 255),
		D3DXVECTOR3(1, -1, 0),   D3DXVECTOR3(0, 0, -1), D3DXVECTOR2(1,1),//D3DXCOLOR(0, 255, 0, 255),
		D3DXVECTOR3(-1, -1, 0),  D3DXVECTOR3(0, 0, -1), D3DXVECTOR2(0,1),//D3DXCOLOR(255, 0, 0, 255),
	};

	//三角形の合わせ方をlistでつくる
	int indexList[] = { 0, 2, 3, 0, 1, 2 };

	//テクスチャ作成
	//画像サイズは2の乗数にする
	D3DXCreateTextureFromFileEx(Direct3D::pDevice, pPicture,
		0, 0, 0, 0, D3DFMT_UNKNOWN, D3DPOOL_DEFAULT, D3DX_FILTER_NONE,
		D3DX_DEFAULT, 0, 0, 0, &pTexture_);
	assert(pTexture_ != nullptr);

	//マテリアルの設定(取り合えず色の指定のみ行う)
	//数値はRGBそれぞれの色の光を何%反射するかを示している
	material_.Diffuse.r = 1.0f;
	material_.Diffuse.g = 1.0f;
	material_.Diffuse.b = 1.0f;
}

void Quad::VertexBuffer(Vertex *vertexList)
{
	//ここでバーテックスバッファを確保する
	Direct3D::pDevice->CreateVertexBuffer(sizeof(*vertexList), 0,
		//ここの順番はd3d9types.hのdefineの順番からき来ている
		D3DFVF_XYZ | D3DFVF_NORMAL | D3DFVF_TEX1 /*D3DFVF_DIFFUSE*/, D3DPOOL_MANAGED, &pVertexBuffer_, 0);
	assert(pVertexBuffer_ != NULL);	//assert処理

	//バーテックスバッファをロックする
	//これで*vCoptを使ってデータをコピーすることが可能になる
	Vertex *vCopy;
	pVertexBuffer_->Lock(0, 0, (void**)&vCopy, 0);

	//ここで作った頂点情報を入れる
	memcpy(vCopy, vertexList, sizeof(*vertexList));

	//コピーが終わり次第バッファのロックを外す
	pVertexBuffer_->Unlock();
}

void Quad::IndexBuffer(Vertex *indexList)
{
	//ここでインデックスバッファを確保する(行っていることはバーテックスバッファと同じ)
	Direct3D::pDevice->CreateIndexBuffer(sizeof(indexList), 0, D3DFMT_INDEX32,
		D3DPOOL_MANAGED, &pIndexBuffer_, 0);
	assert(pIndexBuffer_ != nullptr);
	DWORD *iCopy;
	pIndexBuffer_->Lock(0, 0, (void**)&iCopy, 0);
	memcpy(iCopy, indexList, sizeof(indexList));
	pIndexBuffer_->Unlock();
}

void Quad::Draw(const D3DXMATRIX& matrix)
{
	//描画
	Direct3D::pDevice->DrawIndexedPrimitive(D3DPT_TRIANGLELIST, 0, 0, 4, 0, 2);
}

void Quad::SetDraw(const D3DXMATRIX & matrix)
{
	//ワールド行列に受け取った行列を入れる
	Direct3D::pDevice->SetTransform(D3DTS_WORLD, &matrix);

	//使いたいテクスチャを指定
	Direct3D::pDevice->SetTexture(0, pTexture_);

	//使用するマテリアルを指定する
	Direct3D::pDevice->SetMaterial(&material_);

	//表示したい頂点バッファとインデックスバッファを指定する
	Direct3D::pDevice->SetStreamSource(0, pVertexBuffer_, 0, sizeof(Vertex));
	Direct3D::pDevice->SetIndices(pIndexBuffer_);

	//頂点データがどのような情報を持っているか指定する。
	//									↓これは論理和を表す(2進にしてどっちかが1なら1が立つアレ)
	Direct3D::pDevice->SetFVF(D3DFVF_XYZ | D3DFVF_NORMAL | D3DFVF_TEX1 /*D3DFVF_DIFFUSE*/);
}
