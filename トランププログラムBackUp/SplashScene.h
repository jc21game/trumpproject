#pragma once

#include "Engine/Global.h"

//テストシーンを管理するクラス
class SplashScene : public IGameObject
{
	int hPict_[3];
	float alpha_;
	bool turnFlg_;
	bool changeFlg_;
public:
	//コンストラクタ
	//引数：parent  親オブジェクト（SceneManager）
	SplashScene(IGameObject* parent);

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;
};