#pragma once
#include <d3dx9.h>
#include <list>
#include <string>

//クラスのプロトタイプ宣言
//使えるのは戻り値とかのみ、変数は作れない

class Collider;

//IGameObjectのIってInterferceのIだったらしい
class IGameObject
{

protected:
	//純粋仮想関数は実態を持てないため、ポインタにしないといけない
	IGameObject* pParent_;					//親は誰だか
	std::list<IGameObject*> pChildlist_;	//子供(何人いるか不明なのでlist型を使おう)
	std::string name_;						//とりあえず文字列が入る型

	D3DXVECTOR3 position_;					//位置
	D3DXVECTOR3 rotate_;					//回転
	D3DXVECTOR3 scale_;						//拡大

	D3DXMATRIX  localMatrix_;				//親から見た行列
	D3DXMATRIX	worldMatrix_;				//全体から見た行列

	bool dead_;
	void Transform();

	Collider* pCollider_;

private:

public:
	IGameObject();
	IGameObject(IGameObject* parent);

	//stringは文字列型
	IGameObject(IGameObject* parent, const std::string& name);
	virtual ~IGameObject();

	//コンストラクタ(純粋仮想関数)//
	//初期化
	virtual void Initialize() = 0;

	//更新
 	virtual void Update() = 0;

	//描画
	virtual void Draw() = 0;

	//開放
	virtual void Release() = 0;

	void UpdateSub();
	void DrawSub();
	void ReleaseSub();

	//自分を消す
	void KillMe();

	void Collision( IGameObject* targetobject );
	virtual void OnCollision(IGameObject* ptarget) {};

	//親オブジェクトを取得
	//戻値：親オブジェクトのアドレス
	IGameObject* GetParent();

	D3DXVECTOR3 Getposition();
	D3DXVECTOR3 Getrotate();
	D3DXVECTOR3 Getscale();

	//テンプレートを作成
	template <class T>

	//GameObjectを作成(自動的にもらったクラスに設定される)
	T* CreateGameObject(IGameObject* parent)
	{
		T* p = new T(parent);
		parent->PushBackChild(p);
		p->Initialize();
		return p;
	}

	//指定のオブジェクトを子として入れる
	void PushBackChild(IGameObject* pObj);

	//positionのセッター
	void SetPosition(D3DXVECTOR3 position)
	{
		position_ = position;
	}

	//scaleのセッター
	void SetScale(D3DXVECTOR3 scale)
	{
		scale_ = scale;
	}

	//Colliderのセッター
	void SetCollider(D3DXVECTOR3& center, float radius);
};