#include "Model.h"

namespace Model
{
	//ModelData型のvectorを作成
	std::vector<ModelData*> dataList;

	int Load(std::string fileName)
	{
		ModelData* pData = new ModelData;
		pData->fileName = fileName;

		//探しているファイルが見つかった際に立てるフラグ
		bool isExist = false;

		//構造体ModelListのfileNameに同一の名前のファイルを探す
		for (unsigned int i = 0; i < dataList.size(); i++)
		{
			//見つかった場合
			if (dataList[i]->fileName == fileName)
			{
				//アドレスをそのdataListに入れる
				//それを参照し、目的のファイルを探す
				pData->pFbx = dataList[i]->pFbx;
				isExist = true;
				break;
			}
		}

		//なかったらそのファイルを入れる
		if (isExist == false)
		{
			pData->pFbx = new Fbx;
			pData->pFbx->Load(fileName.c_str());
		}

		dataList.push_back(pData);

		//そのまま送ると配列なのに0からスタートしてしまうので-1しておこう
		return dataList.size() - 1;
	}

	void Draw(int handle)
	{
		//指定されたモデル番号の行列をDrawに送る
		dataList[handle]->pFbx->Draw(dataList[handle]->matrix);
	}

	void SetMatrix(int handle, D3DXMATRIX & matrix)
	{
		//dataList[handle]の行列を引数の行列に入れる
		dataList[handle]->matrix = matrix;

	}

	//開放処理 : 引数1.int dataList
	void Release(int handle)
	{
		//探しているファイルが見つかった際に立てるフラグ
		bool isExist = false;

		//今使っているモデルがほかので使われていないかを探す
		for (unsigned int i = 0; i < dataList.size(); i++)
		{
			//自分をループから外さないとダメ
			//見つかった場合
			if (i != handle && dataList[i] != nullptr &&
				dataList[i]->pFbx == dataList[handle]->pFbx)
			{
				isExist = true;
				break;
			}
		}

		//見つからなかった場合、dataListそのものを開放する
		if (isExist == false)
		{
			SAFE_DELETE(dataList[handle]->pFbx);
		}
		SAFE_DELETE(dataList[handle]);
	}

	void AllRelease()
	{
		for (unsigned int i = 0; i < dataList.size(); i++)
		{
			if (dataList[i] != nullptr)
			{
				Release(i);
			}
		}
		dataList.clear();
	}
};
