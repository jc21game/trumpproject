#pragma once
#include "Global.h"

class Sprite
{
	LPD3DXSPRITE		pSprite_;  //スプライト
	LPDIRECT3DTEXTURE9	pTexture_; //テクスチャ

public:
	Sprite();
	~Sprite();

	void Load(const char* fileName);
	void Draw(const D3DXMATRIX& matrix);
	void Draw(const D3DXMATRIX & matrix, const D3DXVECTOR3 position, float alpha);
};

