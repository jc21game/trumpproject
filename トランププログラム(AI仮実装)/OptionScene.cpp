#include "OptionScene.h"

//コンストラクタ
OptionScene::OptionScene(IGameObject * parent)
	: IGameObject(parent, "OptionScene")
{
}

//初期化
void OptionScene::Initialize()
{
}

//更新
void OptionScene::Update()
{
}

//描画
void OptionScene::Draw()
{
}

//開放
void OptionScene::Release()
{
}