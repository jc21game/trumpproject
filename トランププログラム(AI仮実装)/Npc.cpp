#include "Npc.h"
#include "Human.h"
#include "PlayScene.h"

#define rotateSpeed 3.0f

Npc::Npc(IGameObject* parent) :Player(parent), NPCCard_(HAND_MAX), NPCitr_(NPCCard_.begin()), tradeFlg_(true)
{
	//カードを配当
	//これではコンストラクタ呼ばれているから駄目だな...
	//Human *pHuman = new Human(parent);
	pHuman_ = ((Human*)((PlayScene*)parent)->GetHuman());
	bool isDuplication_ = false;
	int cnt = 0;

	//プレイシーン経由でシャッフル後の山札とそのイテレータをコピー
	shuffleCardSum_ = *((PlayScene*)parent)->GetShuffleCardSum_();
	shuffleitr_ = ((PlayScene*)parent)->GetShuffleitr_();

	//shufflehand_.erase(shufflehand_.begin(), shufflehand_.begin() + 5);
	//shuffleitr_ = shuffleCardSum_.begin();
	pHuman_->Humanitr_ = pHuman_->HumanCard_.begin();

	//Humanとカードが被らないようにカードを配布
	while (cnt != 5)
	{
		//山札から持ってきたカードの絵柄が相手も持っていたら弾く
		if (shuffleitr_->modelname == pHuman_->Humanitr_->modelname)
		{
			isDuplication_ = true;
			++shuffleitr_;
			//break;
		}

		//持ってなかったらvectorの前に入れる
		if (isDuplication_ != true)
		{
			NPCitr_->mark = shuffleitr_->mark;
			NPCitr_->num = shuffleitr_->num;
			NPCitr_->modelname = shuffleitr_->modelname;
			cnt++;
			++pHuman_->Humanitr_;
			++shuffleitr_;
			++NPCitr_;
			shuffleCardSum_.pop_back();
		}
		isDuplication_ = false;
	}
	//shuffleCardSum_.erase(shuffleCardSum_.begin(), shuffleCardSum_.begin() + 5);
	pParent_ = parent;
	pHuman_->Humanitr_ = pHuman_->HumanCard_.begin();
	//delete( pHuman_ );


	//直接いじってしまったカードの情報を元に戻す
	//pHuman->HumanCard_ = pHuman->currentHumanCard_;
	//pHuman->Humanitr_ = pHuman->currentHumanitr_;

	//テス
	pNormalAI_ = new NormalAI(this);
}


Npc::~Npc()
{
}

void Npc::ShowCard()
{
	//カードを表示
	shuffleitr_ = shuffleCardSum_.begin();
	NPCitr_ = NPCCard_.begin();
	std::cout << "NPCのカード : ";
	for (int i = 0; i < HAND_MAX; i++)
	{
		std::cout << "[" << NPCitr_->mark << ":" << NPCitr_->num << "] ";
		++shuffleitr_;
		++NPCitr_;
	}
	std::cout << std::endl;
}

std::vector<Hand>::iterator Npc::ChangeHand(std::vector<Hand>::iterator changecard)
{
	pHuman_ = ((Human*)((PlayScene*)pParent_)->GetHuman());
	pHuman_->Humanitr_ = pHuman_->HumanCard_.begin();
	NPCitr_ = NPCCard_.begin();
	bool isShufDup = false;
	bool isNPCDup = false;
	bool isHumanDup = false;

	//すべての重複がなかったら入れる
	while (isShufDup == false ||
		isNPCDup == false ||
		isHumanDup == false)
	{
		//--------------------------------------------------------
		//交換するカードと被っていたら弾く
		if (shuffleitr_->modelname != changecard->modelname)
			isShufDup = true;

		else isShufDup = false;

		//--------------------------------------------------------
		//山札から持ってきたカードの絵柄が相手も持っていたら弾く
		bool endHumanflg = false;
		while (endHumanflg == false)
		{
			if (shuffleitr_->modelname != pHuman_->Humanitr_->modelname)
				++pHuman_->Humanitr_;

			else
			{
				isShufDup = false;
				break;
			}

			//持ってなかったらvectorの前に入れる
			if (pHuman_->Humanitr_ == pHuman_->HumanCard_.end())
			{
				endHumanflg = true;
				isHumanDup = true;
				break;
			}
		}
		pHuman_->Humanitr_ = pHuman_->HumanCard_.begin();
		//--------------------------------------------------------
		//山札から持ってきたカードの絵柄が自分がすでに持ってたら弾く
		bool endNPCflg = false;
		while (endNPCflg == false)
		{
			if (shuffleitr_->modelname != NPCitr_->modelname)
				++NPCitr_;

			else
			{
				isNPCDup = false;
				break;
			}

			//同じなのが5回なかったらそいつを入れる
			if (NPCitr_ == NPCCard_.end())
			{
				endNPCflg = true;
				isNPCDup = true;
				break;
			}
		}
		NPCitr_ = NPCCard_.begin();
		//--------------------------------------------------------
		++shuffleitr_;
	}

	changecard->mark = shuffleitr_->mark;
	changecard->num = shuffleitr_->num;
	changecard->modelname = shuffleitr_->modelname;

	return changecard;
}

std::vector<Hand> Npc::GetHand()
{
	return NPCCard_;
}

void Npc::Initialize()
{
	int i = 0;

	//モデル配列にカードの絵柄を入れる
	for (auto Roopitr = NPCCard_.begin(); Roopitr != NPCCard_.end(); ++Roopitr )
	{
		hModel_[i] = Model::Load(Roopitr->modelname);
		i++;
	}

	//最後にPlayerのモデルを入れる
	hModel_[i] = Model::Load("Data/Player.fbx");
	assert(hModel_ >= 0);

	NPCitr_ = NPCCard_.begin();
}

void Npc::Update()
{

	if (turn_)
	{

		//srand(time(NULL));
		//どのカード交換するか
		if (tradeFlg_)
		{
			//カード交換するかどうかのフラグ
			//こいつを使ってそれぞれのカードのステータスをキメル（CHECKかSTOPか）
			bool changeFlg[5];
			//初期化
			for (int i = 0; i < 5; i++)
			{
				changeFlg[i] = true;
			}
			//ここでAIthinkを実行？
			pNormalAI_->Thinking(NPCCard_, changeFlg);

			int cnum = 0;
			for (auto RoopItr = NPCCard_.begin(); RoopItr != NPCCard_.end(); RoopItr++)
			{
				if (changeFlg[cnum])
				{
					RoopItr->status = CHECK;
				}
				cnum++;
			}
			//for (/*int i = 0; i < HAND_MAX; i++*/ auto Roopitr = NPCCard_.begin(); Roopitr != NPCCard_.end(); ++Roopitr)
			//{
			//	//50%の確率でそいつを選ぶ
			//	if (rand() % 2)
			//	{
			//		Roopitr->status = CHECK;
			//	}
			//}
			tradeFlg_ = false;
		}

		//カード交換開始
		for (/*int i = 0; i < HAND_MAX; i++*/ auto Roopitr = NPCCard_.begin(); Roopitr != NPCCard_.end(); ++Roopitr)
		{
			if (Roopitr->yPosition >= 1.5f)
			{
				if (Roopitr->status == CHECK)
				{
					Roopitr->status = MOVE_OUT;
				}
			}
		}

		int i = 0;

		//各カードのポジションの設定
		for (/*int i = 0; i < HAND_MAX; i++*/ auto Roopitr = NPCCard_.begin(); Roopitr != NPCCard_.end(); ++Roopitr)
		{
			switch (Roopitr->status)
			{
			case UNCHECK:
			case hairu:
				if (Roopitr->yPosition > 1.2f)
				{
					Roopitr->yPosition -= 0.05f;

					if (Roopitr->yPosition <= 1.2f)
					{
						Roopitr->status = STOP;
					}
				}
				break;
			case CHECK:
				Roopitr->yPosition += 0.05f;
				if (Roopitr->yPosition >= 1.5f)
				{
					Roopitr->yPosition = 1.5f;
				}
				break;
			case MOVE_OUT:
				if (Roopitr->xPosition < 1.5f)
				{
					Roopitr->xPosition += 0.05f;
					if (Roopitr->xPosition >= 1.5f)
					{
						//ここでカードを交換するみたい

						//カードのモデルを呼びなおす
						Roopitr = ChangeHand( Roopitr );
						hModel_[i] = Model::Load(Roopitr->modelname);
						Roopitr->status = MOVE_IN;
					}
				}
				break;
			case MOVE_IN:
				if (Roopitr->xPosition >= -0.6f)
				{
					Roopitr->xPosition -= 0.05f;
					if (Roopitr->xPosition <= -0.6f)
					{
						Roopitr->status = hairu;
					}
				}
				break;
			}
			i++;
		}

		for (auto Roopitr = NPCCard_.begin(); Roopitr != NPCCard_.end(); ++Roopitr)
		{
			if (Roopitr->status != STOP)
			{
				allStopFlg_ = false;
				//allStopFlg_ = true;
				break;
			}
			allStopFlg_ = true;
			//allStopFlg_ = false;
		}

		if (allStopFlg_)
		{
			for (auto Roopitr = NPCCard_.begin(); Roopitr != NPCCard_.end(); ++Roopitr)
			{
				if (0 < Roopitr->rotateY)
				{
					Roopitr->rotateY -= rotateSpeed;
				}
			}
		}
	}
}

void Npc::Draw()
{

	D3DXMATRIX cardMat[HAND_MAX];
	D3DXMATRIX tMat, rXMat, rYMat;

	int i = 0;

	for (auto Roopitr = NPCCard_.begin(); Roopitr != NPCCard_.end(); ++Roopitr)
	{
		if (allStopFlg_)
		{
			//回転
			/*if (i < 5)
			{*/
			//奥カード
			D3DXMatrixTranslation(&tMat, (-0.6f + (i * 0.3f)), 1.2f, 0.5f);
			D3DXMatrixRotationX(&rXMat, D3DXToRadian(-90));
			D3DXMatrixRotationY(&rYMat, D3DXToRadian(Roopitr->rotateY));
			cardMat[i] = rXMat * rYMat * tMat;
			//}
			i++;
		}
		//裏向き
		else
		{
			/*if (i < 5)
			{*/
			switch (Roopitr->status)
			{
				//縦移動
			case CHECK:
			case UNCHECK:
			case hairu:
				D3DXMatrixTranslation(&tMat, (Roopitr->xPosition + (i * 0.3f)), Roopitr->yPosition, 0.5f);
				D3DXMatrixRotationX(&rXMat, D3DXToRadian(-90));
				D3DXMatrixRotationY(&rYMat, D3DXToRadian(Roopitr->rotateY));
				cardMat[i] = rXMat * rYMat * tMat;
				break;
				//横移動
			case MOVE_OUT:
			case MOVE_IN:
				D3DXMatrixTranslation(&tMat, (Roopitr->xPosition + (i * 0.3f)), 1.55f, 0.5f);
				D3DXMatrixRotationX(&rXMat, D3DXToRadian(-90));
				D3DXMatrixRotationY(&rYMat, D3DXToRadian(Roopitr->rotateY));
				cardMat[i] = rXMat * rYMat * tMat;
				break;
				//停止状態
			default:
				D3DXMatrixTranslation(&tMat, (-0.6f + (i * 0.3f)), 1.2f, 0.5f);
				D3DXMatrixRotationX(&rXMat, D3DXToRadian(-90));
				D3DXMatrixRotationY(&rYMat, D3DXToRadian(Roopitr->rotateY));
				cardMat[i] = rXMat * rYMat * tMat;
				break;
				//}
				//	D3DXMATRIX tMat, rXMat, rYMat;
				//	//奥カード
				//	D3DXMatrixTranslation(&tMat, (-0.6f + (i * 0.3f)), 1.2f, 0.5f);
				//	D3DXMatrixRotationX(&rXMat, D3DXToRadian(-90));
				//	D3DXMatrixRotationY(&rYMat, D3DXToRadian(Roopitr->rotateY));
				//	cardMat[i] = rXMat * rYMat * tMat;
				//}
			}
			i++;
		}
	}

	for (int i = 0; i < 6; i++)
	{
		if (i < HAND_MAX)
		{
			Model::SetMatrix(hModel_[i], cardMat[i]);
		}
		else
		{
			Model::SetMatrix(hModel_[i], worldMatrix_);
		}
	}
	for (int i = 0; i < 6; i++)
	{
		Model::Draw(hModel_[i]);
	}
}

void Npc::Release()
{
	SAFE_DELETE(pNormalAI_);
}





/* 9/10作業内容

　　各AIを追加
  　AIの中のいらない部分(合わせた時にエラーはくところをコメントアウト)
    AIがIGameObjectを継承しないように変更
	関数名shinkingをThinkingに変更
	Thinkingに引数として  bool型の配列5つを渡す
	上に追加でstd::vector<Hand>も入れた
	AI.hにcheckとchangeを追加(宣言のみ)


	Npc内のカード交換処理の前にどこのカードを交換するのかを管理する配列を作成

*/

/*　9/11内容

　　カードの数値は1〜13
  　扱うときは0〜12
  　マークは1〜4
    扱うときは0〜3

	フラッシュ揃い確認の時の交換方法を少し変更して登録
	フラッシュ、ストレート確認の時の値、マーク登録時の方法を少し変更

	random交換の処理を登録
	うまく、交換するかどうかを戻せていないっぽい
	修正必須

	↑戻せていないものについて
	参照渡しを行い値を戻すようにした
	各AIクラスの行動に参照渡し的なのつけた

	交換方法、確認方法両方のマージに完了した
	ただしノーマルAIのみ(どれを狙うか)

	フラッシュの交換がおかしかったので修正
*/



/*　わからんとこ

　　AIがIGameObjectを継承することについて

  　そもそもIGameObjectを継承しなくてもよいのではないか
　　IgameObjectを継承すると子クラスとして登録したときに一緒にUpdate,Draw等の処理を一緒にしてくれるが
  　そもそもAIはそのような行動はなく、NPC側からの呼び出しに応じてしか動かない現状では特に継承する意味はないのではないか

　　ただ継承しないとなるとNPCのリリースのタイミングでしっかりとDELETEを呼ばないとメモリリークしてしまう
  　どのみちCreateGameObjectで作ってなかったからSAFEDELETEは呼ばないとリーク

*/