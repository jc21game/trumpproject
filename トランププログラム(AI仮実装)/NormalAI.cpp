#include "NormalAI.h"
#include "Engine/Model.h"


//int NormalAI::turn_ = HUMAN_TURN;

NormalAI::NormalAI(IGameObject* parent) :
	AI(parent),
	isDuplication_(false), allStopFlg_(true)
{
}

NormalAI::~NormalAI()
{
}

void NormalAI::Initialize()
{
}

void NormalAI::Update()
{
}

void NormalAI::Draw()
{
}

void NormalAI::Release()
{
}

void NormalAI::Thinking(std::vector<Hand> hand, bool(&changeFlg)[5])
{

	//////////////////　　堅実？？な考え方　　////////////////////
	//先にペアの有無を確認する
	//↑bool型でペア発見したらtrueを返す関数を作って使う
	//ペアがある場合にはペアを残した考え方を実行
	//↑上の関数でtrueの時

	//ペアがない場合には各マークの枚数を確認する
	//↑手書きしてたやつの考え方を使えば同一カードがどれくらいあるかは分かる(どのマークかまでは分からないが)
	//関数にするかな

	//最多マークの枚数が3枚以上あるなら
	//フラッシュを狙う考え方を実行
	//↑の関数?のスコアを見て

	//最多枚数のマークが2枚以下(2枚)なら
	//ペアを残した考え方(全交換)、あるいはランダムで交換を実行
	if (CheckPair(hand))
	{
		ChangePair(hand, changeFlg);
	}
	else
	{
		if (CheckStraight(hand))
		{
			ChangeStraight(hand, changeFlg);
		}
		else if (CheckMark(hand))
		{
			ChangeFlash(hand, changeFlg);
		}
		else
		{
			//テス
			std::cout << "ペア無し、マーク無し" << std::endl;
			srand((unsigned int)time(NULL));

			if (rand() % 2)
			{
				ChangePair(hand, changeFlg);
			}
			else
			{
				ChangeRand(hand, changeFlg);
			}
		}
	}
}
