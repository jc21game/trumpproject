#include "Judge.h"
#include <iostream>


Judge::Judge()
	:pairPoint_(0), playerPokerHand_(0), npcPokerHand_(0), playerMaxNum_(0), npcMaxNum_(0),
	straightFlg_(true), flashFlg_(true), rsfFlg_(true), reWrightFlg_(true)
{
}


Judge::~Judge()
{

}

void Judge::handCheck(std::vector<Hand> hand, int who)
{
	int stSum = 0;
	pairPoint_ = 0;
	reWrightFlg_ = true;
	straightFlg_ = true;
	flashFlg_ = true;
	rsfFlg_ = true;

	//手札の組み合わせを確認する
	for (auto baseItr = hand.begin(); baseItr != hand.end(); baseItr++)
	{
		int currentpairPoint = pairPoint_;
		//RSFに対応する数値かどうかの判定
		if (rsfFlg_)
		{
			if ((*baseItr).num > 1)
			{
				if ((*baseItr).num < 10)
				{
					rsfFlg_ = false;
				}
			}
		}

		//最大値の登録(ペア未発見の時)
		if (who)
		{
			if ((playerMaxNum_ < (*baseItr).num) && (pairPoint_ == 0))
			{
				playerMaxNum_ = (*baseItr).num;
			}
		}
		else
		{
			if ((npcMaxNum_ < (*baseItr).num) && (pairPoint_ == 0))
			{
				npcMaxNum_ = (*baseItr).num;
			}
		}

		//ベースカードとターゲットカードの数値、マークを確認する
		for (auto trgItr = baseItr + 1; trgItr != hand.end(); trgItr++)
		{
			if ((*baseItr).num == (*trgItr).num)
			{
				pairPoint_ += 1;
				if (reWrightFlg_)
				{
					if (who)
					{
						if (pairPoint_ == 1)
						{
							playerMaxNum_ = (*baseItr).num;
						}
						else if (playerMaxNum_ < (*baseItr).num)
						{
							playerMaxNum_ = (*baseItr).num;
						}
					}
					else
					{
						if (pairPoint_ == 1)
						{
							npcMaxNum_ = (*baseItr).num;
						}
						else if (npcMaxNum_ < (*baseItr).num)
						{
							npcMaxNum_ = (*baseItr).num;
						}
					}
				}
			}
			//ストレートが成立するか
			if (straightFlg_)
			{
				stSum += abs((*baseItr).num - (*trgItr).num);
				if (stSum > 20)
				{
					straightFlg_ = false;
				}
			}
			//フラッシュが成立するか
			if (flashFlg_)
			{
				if ((*baseItr).mark != (*trgItr).mark)
				{
					flashFlg_ = false;
				}
			}
		}
		//3カード,フルハウスが成立するときの最大値(三枚のカードの数値)登録
		if ((reWrightFlg_) && ((pairPoint_ - currentpairPoint) == 2))
		{
			if (who)
			{
				playerMaxNum_ = (*baseItr).num;
			}
			else
			{
				npcMaxNum_ = (*baseItr).num;
			}
			reWrightFlg_ = false;
		}
	}
	//確定した役を登録する
	Regist(who);
}

void Judge::Regist(int who)
{
	//役を登録する(プレイヤー)
	if (who)
	{
		switch (pairPoint_)
		{
		case 0:
			if (rsfFlg_ && flashFlg_)
			{
				playerPokerHand_ = ROYAL_STRAIGHT_FLASH;
			}
			else if (straightFlg_ && flashFlg_)
			{
				playerPokerHand_ = STRAIGHT_FLASH;
			}
			else if (flashFlg_)
			{
				playerPokerHand_ = FLASH;
			}
			else if (straightFlg_)
			{
				playerPokerHand_ = STRAIGHT;
			}
			else
			{
				playerPokerHand_ = NO_PAIR;
			}
			break;
		case 1:
			playerPokerHand_ = ONE_PAIR;
			break;
		case 2:
			playerPokerHand_ = TWO_PAIR;
			break;
		case 3:
			playerPokerHand_ = THREE_CARD;
			break;
		case 4:
			playerPokerHand_ = FULLHOUSE;
			break;
		case 6:
			playerPokerHand_ = FOUR_CARD;
			break;
		default:
			std::cout << "エラー？" << std::endl;
			break;
		}
		std::cout << "自分の役は" << playerPokerHand_ << std::endl;
	}
	//役の登録(npc)
	else
	{
		switch (pairPoint_)
		{
		case 0:
			if (rsfFlg_ && flashFlg_)
			{
				npcPokerHand_ = ROYAL_STRAIGHT_FLASH;
			}
			else if (straightFlg_ && flashFlg_)
			{
				npcPokerHand_ = STRAIGHT_FLASH;
			}
			else if (flashFlg_)
			{
				npcPokerHand_ = FLASH;
			}
			else if (straightFlg_)
			{
				npcPokerHand_ = STRAIGHT;
			}
			else
			{
				npcPokerHand_ = NO_PAIR;
			}
			break;
		case 1:
			npcPokerHand_ = ONE_PAIR;
			break;
		case 2:
			npcPokerHand_ = TWO_PAIR;
			break;
		case 3:
			npcPokerHand_ = THREE_CARD;
			break;
		case 4:
			npcPokerHand_ = FULLHOUSE;
			break;
		case 6:
			npcPokerHand_ = FOUR_CARD;
			break;
		default:
			std::cout << "エラー？" << std::endl;
			break;
		}
		std::cout << "相手の役は" << npcPokerHand_ << std::endl;
	}
}



void Judge::GameJudge()
{
	if ((playerPokerHand_ == 0) && (npcPokerHand_ == 0))
	{
		std::cout << "引き分けです" << std::endl;
	}
	else if (playerPokerHand_ < npcPokerHand_)
	{
		std::cout << "Npcの勝ちです" << std::endl;
	}
	else if (playerPokerHand_ == npcPokerHand_)
	{
		//（最大の）数値の比較をしてどっちが勝ったかを判定する
		if (playerMaxNum_ < npcMaxNum_)
		{
			std::cout << "NPCの勝ちです" << std::endl;
		}
		else if (playerMaxNum_ == npcMaxNum_)
		{
			std::cout << "引き分けです" << std::endl;
		}
		else
		{
			std::cout << "Playerの勝ちです" << std::endl;
		}
	}
	else
	{
		std::cout << "Playerの勝ちです" << std::endl;
	}
}