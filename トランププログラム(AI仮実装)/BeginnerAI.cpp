#include "BeginnerAI.h"
#include "Engine/Model.h"


//int BeginnerAI::turn_ = HUMAN_TURN;

BeginnerAI::BeginnerAI(IGameObject* parent) :
	AI(parent),
	isDuplication_(false), allStopFlg_(true)
{
}

BeginnerAI::~BeginnerAI()
{
}

void BeginnerAI::Initialize()
{
}

void BeginnerAI::Update()
{
}

void BeginnerAI::Draw()
{
}

void BeginnerAI::Release()
{
}

void BeginnerAI::Thinking(std::vector<Hand> hand, bool(&changeFlg)[5])
{
}

