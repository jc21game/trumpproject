#include "Player.h"
#include "Engine/Model.h"


int Player::turn_ = HUMAN_TURN;

Player::Player(IGameObject* parent) :
	IGameObject(parent, "Player"),
	isDuplication_(false), allStopFlg_(true)
{
}

Player::~Player()
{
}

void Player::Initialize()
{
}

void Player::Update()
{
}

void Player::Draw()
{
}

void Player::Release()
{
}

