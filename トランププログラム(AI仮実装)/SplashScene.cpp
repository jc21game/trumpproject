#include "SplashScene.h"
#include "Engine/Sprite.h"
#include "Image.h"

//コンストラクタ
SplashScene::SplashScene(IGameObject * parent)
	: IGameObject(parent, "SplashScene"), alpha_(0), turnFlg_(false),changeFlg_(false)
{

}

//初期化
void SplashScene::Initialize()
{

	//画像データの名前
	std::string name[3] =
	{
		"BackGroundSplash",
		"SchoolLogo",
		"TeamLogo"
	};

	for ( int i = 0; i < 3; i++ )
	{
		hPict_[i] = Image::Load("Data/" + name[i] + ".png");
		assert(hPict_[i] >= 0);
	}
	
}

//更新
void SplashScene::Update()
{

	//透明度の調整
	if (!turnFlg_)
	{
		alpha_ += 0.01f;

		//スプラッシュ画面スキップできるようにした
		if (Input::IsKeyDown(DIK_RETURN))
		{
			SceneManager::ChangeScene(SCENE_ID_TITLE);
		}

		//2秒間かけてフェードイン
		if (1.2f < alpha_)
		{
			turnFlg_ = true;
		}
	}
	else
	{
		//2秒間かけてフェードアウト
		alpha_ -= 0.01f;
	}

	//消えたら画面遷移
	//完全に消えて1秒後
	if (alpha_ <= -0.6)
	{
		if (changeFlg_)
		{
			SceneManager::ChangeScene(SCENE_ID_TITLE);
		}
		changeFlg_ = true;
		turnFlg_ = false;
	}
}

//描画
void SplashScene::Draw()
{

	//背景の描画
	Image::SetMatrix(hPict_[0], worldMatrix_);
	Image::Draw(hPict_[0]);

	//ロゴの描画
	if (changeFlg_)
	{
		Image::SetMatrix(hPict_[2], worldMatrix_);
		Image::Draw(hPict_[2], D3DXVECTOR3(448, 0, 0), alpha_);
	}
	else
	{
		Image::SetMatrix(hPict_[1], worldMatrix_);
		Image::Draw(hPict_[1], D3DXVECTOR3(448, 0, 0), alpha_);
	}
}

//開放
void SplashScene::Release()
{

}