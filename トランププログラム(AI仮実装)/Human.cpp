#include "Human.h"
#include "Npc.h"
#include "PlayScene.h"

Human::Human(IGameObject* parent) :Player(parent),  HumanCard_(HAND_MAX),preHandState_(HAND_MAX), Humanitr_(HumanCard_.begin()), changeEndFlg_(false)
{
	//プレイシーンの山札にアクセスして山札の情報を書き込む
	shuffleCardSum_ = *((PlayScene*)parent)->GetShuffleCardSum_();
	shuffleitr_ = ((PlayScene*)parent)->GetShuffleitr_();

	for (int i = 0; i < HAND_MAX; i++)
	{
 		Humanitr_->mark = shuffleitr_->mark;
		Humanitr_->num = shuffleitr_->num;
		Humanitr_->modelname = shuffleitr_->modelname;
		++shuffleitr_;
		++Humanitr_;
	}

	shuffleitr_ = shuffleCardSum_.begin();

	pParent_ = parent;
}


Human::~Human()
{
}

//交換用の関数
std::vector<Hand>::iterator Human::ChangeHand(std::vector<Hand>::iterator changecard )
{

	pNpc_ = ((Npc*)((PlayScene*)pParent_)->GetNpc());
	pNpc_->NPCitr_ = pNpc_->NPCCard_.begin();
	Humanitr_ = HumanCard_.begin();
	bool isShufDup = false;
	bool isNPCDup = false;
	bool isHumanDup = false;

	//すべての重複がなかったら入れる
	while ( isShufDup  == false ||
		    isNPCDup   == false ||
		    isHumanDup == false     )
	{
		//--------------------------------------------------------
		//交換するカードと被っていたら弾く
		if ( shuffleitr_->modelname != changecard->modelname )
			isShufDup = true;

		else isShufDup = false;

		//--------------------------------------------------------
		//山札から持ってきたカードの絵柄が相手も持っていたら弾く
		bool endNpcflg = false;
		while (endNpcflg == false )
		{
			if (shuffleitr_->modelname != pNpc_->NPCitr_->modelname)
				++pNpc_->NPCitr_;

			else
			{
				isNPCDup = false;
				break;
			}

			//持ってなかったらvectorの前に入れる
			if (pNpc_->NPCitr_ == pNpc_->NPCCard_.end())
			{
				endNpcflg = true;
				isNPCDup = true;
				break;
			}
		}
		pNpc_->NPCitr_ = pNpc_->NPCCard_.begin();
		//--------------------------------------------------------
		//山札から持ってきたカードの絵柄が自分がすでに持ってたら弾く
		bool endHumflg = false;
		while ( endHumflg == false )
		{
			if (shuffleitr_->modelname != Humanitr_->modelname)
				++Humanitr_;

			else
			{
				isHumanDup = false;
				break;
			}

			//同じなのが5回なかったらそいつを入れる
			if (Humanitr_ == HumanCard_.end())
			{
				endHumflg = true;
				isHumanDup = true;
				break;
			}
		}
		Humanitr_ = HumanCard_.begin();
		//--------------------------------------------------------
		++shuffleitr_;
	}

	changecard->mark = shuffleitr_->mark;
	changecard->num = shuffleitr_->num;
	changecard->modelname = shuffleitr_->modelname;

	return changecard;
}

std::vector<Hand> Human::GetHand()
{
	return HumanCard_;
}

void Human::Initialize()
{
	int i = 0;

	//モデル配列にカードの絵柄を入れる
	for (auto Roopitr = HumanCard_.begin(); Roopitr != HumanCard_.end(); ++Roopitr)
	{
		hModel_[i] = Model::Load(Roopitr->modelname);
		i++;
	}

	Humanitr_ = HumanCard_.begin();
}

void Human::Update()
{

	//プレイシーンで「リザルト画面を表示しているかどうか」の情報取得
	if (((PlayScene*)pParent_)->GetIsDrawRetire())
	{
		//表示中なら
		for (auto Roopitr = HumanCard_.begin(); Roopitr != HumanCard_.end(); ++Roopitr)
		{
			Roopitr->status = LOCK;
		}
	}

	//強制的に「STOP」にするから戻っちゃうみたい...
	else
	{
		auto PreRoopitr = preHandState_.begin();
		for (auto Roopitr = HumanCard_.begin(); Roopitr != HumanCard_.end(); ++Roopitr)
		{
			Roopitr->status = PreRoopitr->status;
			++PreRoopitr;
		}
	}

	if (Humanitr_->status != LOCK)
	{
		if (!turn_)
		{
			if (!changeEndFlg_)
			{
				//選択カード移動
				if (Input::IsKeyDown(DIK_RIGHT) && (handnum_ < 4) && (Humanitr_ != HumanCard_.end()))
				{
					handnum_++;
					++Humanitr_;
				}

				if (Input::IsKeyDown(DIK_LEFT) && (handnum_ > 0) && (Humanitr_ != HumanCard_.begin()))
				{
					handnum_--;
					--Humanitr_;
				}

				//カード選択
				if (Input::IsKeyDown(DIK_SPACE))
				{
					if (Humanitr_->status == CHECK)
					{
						Humanitr_->status = UNCHECK;
					}

					else
					{
						//これでカードが選ばれた状態になる
						Humanitr_->status = CHECK;
					}
				}

				if (Input::IsKeyDown(DIK_RETURN))
				{
					for (auto Roopitr = HumanCard_.begin(); Roopitr != HumanCard_.end(); ++Roopitr)
					{
						if (Roopitr->status == CHECK)
						{
							Roopitr->status = MOVE_OUT;
							changeEndFlg_ = true;
						}
					}
				}

				if (Input::IsKeyDown(DIK_F))
				{
					changeEndFlg_ = true;
				}
			}


			auto PreRoopitr = preHandState_.begin();
			int i = 0;

			//各カードのポジションの設定
			for (auto Roopitr = HumanCard_.begin(); Roopitr != HumanCard_.end(); ++Roopitr)
			{
				switch (Roopitr->status)
				{
				case UNCHECK:
				case hairu:
					if (Roopitr->yPosition > 1.2f)
					{
						Roopitr->yPosition -= 0.05f;
						if (Roopitr->yPosition <= 1.2f)
						{
							Roopitr->status = STOP;
						}
					}
					break;
				case CHECK:
					Roopitr->yPosition += 0.05f;
					if (Roopitr->yPosition >= 1.5f)
					{
						Roopitr->yPosition = 1.5f;
					}
					break;
				case MOVE_OUT:
					if (Roopitr->xPosition < 1.5f)
					{
						Roopitr->xPosition += 0.05f;
						if (Roopitr->xPosition >= 1.5f)
						{
							//ここでカードを交換するみたい

							//カードのモデルを呼びなおす
							Roopitr = ChangeHand( Roopitr );
							hModel_[i] = Model::Load(Roopitr->modelname);
							Roopitr->status = MOVE_IN;
						}
					}
					break;
				case MOVE_IN:
					if (Roopitr->xPosition >= -0.6f)
					{
						Roopitr->xPosition -= 0.05f;
						if (Roopitr->xPosition <= -0.6f)
						{
							Roopitr->status = hairu;
						}
					}
					break;
				}

				//更新前最後のカードの状態を記憶
				i++;
				if ( PreRoopitr != preHandState_.end() )
				{
					PreRoopitr->status = Roopitr->status;
					++PreRoopitr;
				}
			}
		}

		for (auto Roopitr = HumanCard_.begin(); Roopitr != HumanCard_.end(); ++Roopitr)
		{
			if (Roopitr->status != STOP)
			{
				allStopFlg_ = false;
				break;
			}
			allStopFlg_ = true;
		}

		if (changeEndFlg_ && allStopFlg_)
		{
			turn_ = NPC_TURN;
		}
	}
}

void Human::Draw()
{

	D3DXMATRIX cardMat[HAND_MAX];
	int i = 0;

	for (auto Roopitr = HumanCard_.begin(); Roopitr != HumanCard_.end(); ++Roopitr)
	{
		D3DXMATRIX tMat, rXMat, rYMat;
		switch (Roopitr->status)
		{
			//縦移動
		case CHECK:
		case UNCHECK:
		case hairu:
			D3DXMatrixTranslation(&tMat, (Roopitr->xPosition + (i * 0.3f)), Roopitr->yPosition + 0.05f, -1.0f);
			D3DXMatrixRotationX(&rXMat, D3DXToRadian(-90));
			cardMat[i] = rXMat * tMat;
			break;

			//横移動
		case MOVE_OUT:
		case MOVE_IN:
			D3DXMatrixTranslation(&tMat, (Roopitr->xPosition + (i * 0.3f)), 1.55f, -1.0f);
			D3DXMatrixRotationX(&rXMat, D3DXToRadian(-90));
			cardMat[i] = rXMat * tMat;
			break;
			//停止状態
		default:
			if (changeEndFlg_)
			{
				D3DXMatrixTranslation(&tMat, (-0.6f + (i * 0.3f)), 1.2f, -1.0f);
				D3DXMatrixRotationX(&rXMat, D3DXToRadian(-90));
				cardMat[i] = rXMat * tMat;
			}

			else if (handnum_ == i)
			{
				D3DXMatrixTranslation(&tMat, (-0.6f + (i * 0.3f)), 1.25f, -1.0f);
				D3DXMatrixRotationX(&rXMat, D3DXToRadian(-90));
				cardMat[i] = rXMat * tMat;
			}
			else
			{
				D3DXMatrixTranslation(&tMat, (-0.6f + (i * 0.3f)), 1.2f, -1.0f);
				D3DXMatrixRotationX(&rXMat, D3DXToRadian(-90));
				cardMat[i] = rXMat * tMat;
			}
			break;
		}
		i++;
	}

	for (int i = 0; i < HAND_MAX; i++)
	{
		Model::SetMatrix(hModel_[i], cardMat[i]);
	}
	for (int i = 0; i < HAND_MAX; i++)
	{
		Model::Draw(hModel_[i]);
	}
}

void Human::Release()
{
}
