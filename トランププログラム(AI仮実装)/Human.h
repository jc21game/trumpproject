#pragma once
#include "Player.h"

class Npc;

class Human :public Player
{
private:

	std::vector<Hand> GetHand();


	//Update処理前の状態を記憶しておくための変数
	std::vector<Hand> preHandState_;

	//交換終了か
	bool changeEndFlg_;

	Npc *pNpc_;
	IGameObject *pParent_;	//メンバとして親の情報を入れておく

public:
	Human(IGameObject* parent);
	~Human();

	std::vector<Hand> HumanCard_;
	std::vector<Hand>::iterator Humanitr_;

	std::vector<Hand> currentHumanCard_;	//NPCクラスでイテレータいじるから前の情報保存用(手札の情報)
	std::vector<Hand>::iterator currentHumanitr_;		//NPCクラスでイテレータいじるから前の情報保存用(どの手札見てるのか)

	void RegistHand();

	void ShowCard();

	std::vector<Hand>::iterator ChangeHand(std::vector<Hand>::iterator changecard );

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;
};