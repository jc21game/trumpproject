#pragma once
#include "Engine/global.h"
#include <vector>

//プレイシーンを管理するクラス

class Human;
class Npc;

struct Hand
{
	int num = -1;			//カードのナンバー
	int mark = -1;			//カードのマーク
	std::string modelname;	//モデルの名前
	int status;
	float yPosition = 1.5f;
	//float yPosition = 1.2f;
	float xPosition = -0.6f;
	float rotateY = 180.0f;
};

class PlayScene : public IGameObject
{
private:


	enum
	{
		RETIRE,			//はい( リタイアする )
		NOT_RETIRE,		//いいえ( リタイアしない )
		RESULT,			//リザルト画面
		RETIRE_PICT_MAX,
	};

	enum
	{
		TURN,
		HUMAN_BET_CHIP_TOTAL,
		NPC_BET_CHIP_TOTAL,
		PICT_MAX
	};

	int mPict_[PICT_MAX];	//画像番号
	int battlecnt_;			//勝負数はこれで管理
	int hPict_[RETIRE_PICT_MAX];		//画像データ
	int hModel_;			//テーブル用モデル番号
	bool isDrawRetire_;		//リタイア画面を表示するかどうか
	bool isRetire_;			//リタイア「はい」or「いいえ」
	bool isDrawResult_;		//リザルト画面を表示するかどうか

	//↓実験用
	float posX_ = 0.0f;
	D3DXMATRIX mat;

	//山札関係
	std::vector<Hand> cardSum_;
	std::vector<Hand>::iterator cardSumItr_;

	std::vector<Hand> shuffleCardSum_;
	std::vector<Hand>::iterator shuffleitr_;

	Human* pHuman_;
	Npc* pNpc_;

	bool isDuplication_;

public:
	//コンストラクタ
	//引数：parent  親オブジェクト（SceneManager）
	PlayScene(IGameObject* parent);

	//初期化
	void Initialize() override;

	//カードモデルをロードして初期化
	void CardModelLoad();

	//カードをシャッフル
	void CardShuffle();

	//モデル、画像のデータロード
	void LoadData();

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;

	//リザルト画面表示中かどうかのフラグのゲッター
	bool GetIsDrawRetire();

	//プレイヤーオブジェクトのゲッター
	Human *GetHuman() { return pHuman_;}
	Npc *GetNpc() { return pNpc_; }

	//山札のゲッター関係
	std::vector<Hand> *GetShuffleCardSum_() { return &shuffleCardSum_; }
	std::vector<Hand>::iterator GetShuffleitr_() { return shuffleitr_; }
};