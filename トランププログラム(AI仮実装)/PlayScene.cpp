#include <string>
#include "PlayScene.h"
#include "Player.h"
#include "Judge.h"
#include "Human.h"
#include "Npc.h"
//#include "Player.h"
#include "Image.h"
#include "Engine/Model.h"
#include "Engine/Camera.h"

#define NPC 0
#define HUMAN 1

//コンストラクタ
PlayScene::PlayScene(IGameObject * parent)
	: IGameObject(parent, "PlayScene"), battlecnt_(0),isDrawRetire_(false), isRetire_(false),
	isDrawResult_(false), cardSum_(CARD_SUM), cardSumItr_(cardSum_.begin()),
	shuffleCardSum_(CARD_SUM), shuffleitr_(shuffleCardSum_.begin())
{
}

//初期化
void PlayScene::Initialize()
{
	srand((unsigned)time(NULL));

	LoadData();

	CardModelLoad();

	CardShuffle();

	pHuman_ = CreateGameObject<Human>(this);
	pNpc_ = CreateGameObject<Npc>(this);
	//CreateGameObject<Judge>(this);
 	/*Player* pPlayer = CreateGameObject<Player>(this);*/

}

void PlayScene::CardModelLoad()
{
	//画像データの名前
	std::string name[CARD_MARK_MAX] =
	{
		"Club",
		"Diamond",
		"Spade",
		"Heart"
	};

	//カードのナンバー
	std::string number[CARD_NUMBER_MAX] =
	{
		"1","2","3","4","5","6","7","8","9","10","11","12","13"
	};

	for (int i = 0; i < CARD_MARK_MAX; i++)
	{
		//カードを初期化
		for (int j = 0; j < CARD_NUMBER_MAX; j++)
		{
			cardSumItr_->mark = i;
			cardSumItr_->num = j;
			cardSumItr_->modelname = ("Data/CardModels/" + name[i] + number[j] + ".fbx");
			++cardSumItr_;
		}
	}
}

void PlayScene::CardShuffle()
{
	//シャッフルの処理
	for (cardSumItr_ = cardSum_.begin(); cardSumItr_ != cardSum_.end(); ++cardSumItr_)
	{
		//全シャッフル用のカードから無作為に番地を取り、そこに山札のカードを仕込む
		while (isDuplication_ == false)
		{
			shuffleitr_ = shuffleCardSum_.begin() + rand() % CARD_SUM;

			//ランダムにとった番地にすでに値が入っていないときにのみ
			if (shuffleitr_->mark == -1)
			{
				shuffleitr_->mark = cardSumItr_->mark;
				shuffleitr_->num = cardSumItr_->num;
				shuffleitr_->modelname = cardSumItr_->modelname;
				isDuplication_ = true;
			}
		}
		isDuplication_ = false;
	}
	shuffleitr_ = shuffleCardSum_.begin();
}

void PlayScene::LoadData()
{
	std::string pictName[RETIRE_PICT_MAX]
	{
		"Retire",
		"NotRetire",
		"Result",
	};

	//画像データの名前
	std::string name[PICT_MAX] =
	{
		"TurnSample2FS",
		"HumanBetChipTotalSampleFS",
		"NPCBetChipTotalSampleFS"
	};


	hModel_ = Model::Load("data/table.fbx");
	assert(hModel_ >= 0);


	Camera* pCamera = CreateGameObject<Camera>(this);
	pCamera->SetPosition(D3DXVECTOR3(0, 1.15f, -1.0f));

	for (int i = 0; i < RETIRE_PICT_MAX; i++)
	{
		hPict_[i] = Image::Load("Data/" + pictName[i] + ".png");
		assert(hPict_[i] >= 0);
	}

	//画像データを読み込み
	for (int i = 0; i < PICT_MAX; i++)
	{
		mPict_[i] = Image::Load("Data/" + name[i] + ".png");
		assert(mPict_[i] >= 0);
	}
}

//更新
void PlayScene::Update()
{
	//クラスの生成
	//Human human;
	//Npc npc;
	//Player player;

	//手札の開示(テスト用)
	//human_->ShowCard();
	//npc_->ShowCard();

	////判定
	//judge.handCheck(human.GetHand(), HUMAN);
	//judge.handCheck(npc.GetHand(), NPC);
	//judge.GameJudge();

	std::cout << "--------------------------------------------" << std::endl;

	battlecnt_++;

	//とりあえず5回勝負したら終わり
	if ( battlecnt_ == 5 )
	{
		//リザルト画面を表示
	}

	///////////////////////////
	////リタイア関係の処理////
	//////////////////////////

	if (Input::IsKeyDown(DIK_ESCAPE))
	{
		//リタイアメッセージ表示用
		isDrawRetire_ = true;
	}

	if (isDrawRetire_)
	{
		//「いいえ」ボタン選択中左キー入力で
		if (Input::IsKeyDown(DIK_LEFT) && !(isRetire_))
		{
			//「はい」に移動
			isRetire_ = true;
		}

		//「はい」ボタン選択中右キー入力で
		else if(Input::IsKeyDown(DIK_RIGHT) && isRetire_)
		{
			//「いいえ」に移動
			isRetire_ = false;
		}

		//「いいえ」ボタン選択中エンターキー入力で
		if (Input::IsKeyDown(DIK_RETURN) && !(isRetire_))
		{
			isDrawRetire_ = false;
		}

		//「はい」ボタン選択中エンターキー入力で
		else if (Input::IsKeyDown(DIK_RETURN) && isRetire_)
		{
			//リザルト画面表示
			isDrawResult_ = true;
		}
	}

	//リザルト画面表示中にスペースキー入力で
	if (isDrawResult_ && Input::IsKeyDown(DIK_SPACE))
	{
		//難易度選択に戻る
		SceneManager::ChangeScene(SCENE_ID_SELECT_DIFFICULT);
	}

}

//描画
void PlayScene::Draw()
{
	//描画の処理//
	//テーブル
	Model::Draw(hModel_);
	for (int i = 0; i < PICT_MAX; i++)
	{
		//UI関係の描画
		Image::SetMatrix(mPict_[i], worldMatrix_);
		Image::Draw(mPict_[i]);
	}

	//リタイア画面の描画
	if (isDrawRetire_)
	{
		//はい
		if (isRetire_)
		{
			Image::SetMatrix(hPict_[RETIRE], worldMatrix_);
			Image::Draw(hPict_[RETIRE]);
		}

		//いいえ
		else
		{
			Image::SetMatrix(hPict_[NOT_RETIRE], worldMatrix_);
			Image::Draw(hPict_[NOT_RETIRE]);
		}
	}

	//リザルト画面の描画
	if (isDrawResult_)
	{
		Image::SetMatrix(hPict_[RESULT], worldMatrix_);
		Image::Draw(hPict_[RESULT]);
	}

}

//開放
void PlayScene::Release()
{
}

bool PlayScene::GetIsDrawRetire()
{
	return isDrawRetire_;
}
