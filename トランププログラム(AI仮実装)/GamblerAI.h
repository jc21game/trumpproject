#include "PlayScene.h"
#include "AI.h"
#include <iostream>
#include <vector>
#include <time.h>
//テス
#include "Engine/Model.h"
#include "Image.h"

#pragma once
//手札配列の最大値
#define HAND_MAX 5
#define CARD_MARK_MAX 4
#define CARD_NUMBER_MAX 13
#define CARD_SUM 52

//モデル番号の最大値
//const int MODEL_MAX = 6;

////構造体カードの型
//struct Hand
//{
//	int num = -1;			//カードのナンバー
//	int mark = -1;			//カードのマーク
//	std::string modelname;	//モデルの名前
//	int status;
//	float yPosition = 1.2f;
//	float xPosition = -0.6f;
//	float rotateY = 180.0f;
//};

//テス
//enum STATUS
//{
//	STOP,		//基本状態
//	UNCHECK,	//カードのy座標を下げる状態
//	CHECK,		//カードのy座標を上げる状態
//	MOVE_OUT,	//カードのx座標を右に移動する状態
//	MOVE_IN,	//カードのx座標を左に移動する状態
//	hairu,		//UNCHECKと同じことをしているのでいらない？
//	LOCK,		//リタイア画像出してる時にカードは動かさない
//};

//enum TURN
//{
//	HUMAN_TURN,
//	NPC_TURN
//};

//struct vhand
//{
//	int status;
//	float yPosition = 1.2f;
//	float xPosition = -0.6f;
//	float rotateY = 180.0f;
//};

class GamblerAI : public AI
{
private:

protected:

	bool isDuplication_;

	//テス
	//手札のモデル番号登録用
	//int hModel_[MODEL_MAX];

	//状態管理用仮想手札
	//vhand virtualhand_[HAND_MAX];

	//何番目の手札を指しているのかを保持する
	int handnum_ = 0;

	//どっちのターンか
	//static int turn_;
	//全てのカードがSTOPか
	bool allStopFlg_;

	//継承先でプレイシーンの値をコピーして使う
	std::vector<Hand> shuffleCardSum_;
	std::vector<Hand>::iterator shuffleitr_;

public:
	GamblerAI(IGameObject * parent);
	~GamblerAI();
	//初期化
	virtual void Initialize() override;

	//更新
	virtual void Update() override;

	//描画
	virtual void Draw() override;

	//開放
	virtual void Release() override;

	//いくらベットするか、何を交換するかの純粋仮想関数
	void Thinking(std::vector<Hand> hand, bool(&changeFlg)[5]) override;
};